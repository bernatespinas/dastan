use std::{collections::HashMap, fs::File, io::BufReader, path::PathBuf};

use crate::generator::Generator;
use crate::map_generator::construction::{ConstructionLayout, ConstructionMapping};

#[derive(Default)]
pub struct ConstructionGen {
	hashmap: HashMap<String, ConstructionLayout>,
}

impl Generator<ConstructionLayout> for ConstructionGen {
	type AuxiliaryGenerator = ();

	fn load(_: &Self::AuxiliaryGenerator) -> Result<Self, String> {
		let parent_dir = PathBuf::from("resources").join("data").join("construction");

		let hashmap = std::fs::read_dir(&parent_dir)
			.unwrap()
			.into_iter()
			.map(|construction_dir_entry| {
				let construction_name = construction_dir_entry
					.unwrap()
					.file_name()
					.into_string()
					.unwrap();

				let construction_dir = parent_dir.join(&construction_name);

				let layout = std::fs::read_to_string(construction_dir.join("layout.txt")).unwrap();

				let mapping: ConstructionMapping = ron::de::from_reader(BufReader::new(
					File::open(construction_dir.join("mapping.ron")).unwrap(),
				))
				.unwrap();

				(
					construction_name.to_uppercase(),
					ConstructionLayout::new(layout, mapping).unwrap(),
				)
			})
			.collect();

		Ok(Self { hashmap })
	}

	fn get(&self, key: &str) -> &ConstructionLayout {
		self.hashmap.get(key).expect(&format!(
			"Unknown key `{}` in Generator {}. Available keys: {:#?}",
			key,
			stringify!($generator),
			self.list()
		))
	}

	fn generate(&self, key: &str) -> ConstructionLayout {
		self.get(key).clone()
	}

	fn can_generate(&self, key: &str) -> bool {
		self.hashmap.contains_key(key)
	}

	fn list(&self) -> Vec<&String> {
		let mut vec: Vec<&String> = self.hashmap.keys().collect();
		vec.sort();

		vec
	}
}
