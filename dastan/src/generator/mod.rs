mod generator_bundle;
pub use generator_bundle::GeneratorBundle;

pub mod construction_gen;
pub use construction_gen::ConstructionGen;

mod create_generators;

// mod raw_representations;

pub trait Generator<T>: Sized {
	type AuxiliaryGenerator;

	/// Load all the `.ron` files in a directory, which are expected to be deserializable to `T`.
	fn load(auxiliary_generator: &Self::AuxiliaryGenerator) -> Result<Self, String>;

	fn get(&self, key: &str) -> &T;

	/// Generates and returns the object specified by `key`.
	fn generate(&self, key: &str) -> T;

	/// Returns `true` if the key is present in this `Generator`; `false` otherwise.
	fn can_generate(&self, key: &str) -> bool;

	/// Lists the keys present in the current `Generator`.
	fn list(&self) -> Vec<&String>;
}
