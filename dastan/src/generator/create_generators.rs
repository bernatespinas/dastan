use std::collections::HashMap;
use std::path::PathBuf;

use serde::de::DeserializeOwned;

use dastan_macro_rules::create_generator;

use crate::biome::Biome;
use crate::ecs::prelude::{CreatureWithComponents, ItemWithComponents, PropWithComponents};
use crate::ecs_components::{Blueprint, Recipe};
use crate::file_utils;
use crate::generator::Generator;

create_generator!(Generator CreatureGen loads CreatureWithComponents from "resources/data/creature");

create_generator!(Generator PropGen loads PropWithComponents from "resources/data/props");

create_generator!(Generator ItemGen loads ItemWithComponents from "resources/data/items");

create_generator!(
	Generator RecipeGen loads Recipe from "resources/data/recipe");

create_generator!(
	Generator BlueprintGen loads Blueprint from "resources/data/blueprint");

create_generator!(
	Generator BiomeGen loads Biome from "resources/data/biome");

fn deserialize_ron_definitions<T: std::fmt::Debug + DeserializeOwned>(
	dir_path: &str,
) -> Result<HashMap<String, T>, String> {
	let filenames = file_utils::get_filenames_in_path(&PathBuf::from(dir_path))?;

	// filenames.into_iter().map(|filename| -> Result<String, String> {
	// 	let string_content = dastan_core::file_utils::read(&format!("{dir_path}/{filename}"), &filename)?;
	// 	dastan_core::file_utils::deserialize_owned(&string_content, &filename)?
	// })?.collect();

	let mut hashmap = HashMap::new();

	for filename in filenames {
		let file_content = file_utils::read(format!("{dir_path}/{filename}"), &filename)?;
		let element: T = file_utils::deserialize_owned(&file_content, &filename)?;

		hashmap.insert(filename.to_string(), element);
	}

	Ok(hashmap)
}
