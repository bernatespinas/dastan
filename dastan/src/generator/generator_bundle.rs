use uuid::Uuid;

use crate::biome::Biome;
use crate::ecs::prelude::{
	CreatureWithComponents, IdentifyMut, ItemWithComponents, NonIdentify, PropWithComponents,
};
use crate::ecs_components::{Blueprint, Recipe};
use crate::generator::{ConstructionGen, Generator};
use crate::map_generator::construction::ConstructionLayout;
use crate::terrain::{Terrain, TerrainGenerator, TerrainType};

use super::create_generators::{BiomeGen, BlueprintGen, CreatureGen, ItemGen, PropGen, RecipeGen};

/// A struct that "bundles" all the generators together, so that passing them separately is not necessary.
#[derive(Default)]
pub struct GeneratorBundle {
	pub terrain_gen: TerrainGenerator,
	creature_gen: CreatureGen,
	prop_gen: PropGen,
	item_gen: ItemGen,
	recipe_gen: RecipeGen,
	blueprint_gen: BlueprintGen,
	biome_gen: BiomeGen,
	construction_gen: ConstructionGen,
}

pub trait Generate<T: Clone> {
	fn _generate(&self, key: &str) -> T;
	fn _get(&self, key: &str) -> &T;
}

impl Generate<CreatureWithComponents> for GeneratorBundle {
	fn _generate(&self, key: &str) -> CreatureWithComponents {
		self.creature_gen.generate(key)
	}

	fn _get(&self, key: &str) -> &CreatureWithComponents {
		self.creature_gen.get(key)
	}
}

impl Generate<PropWithComponents> for GeneratorBundle {
	fn _generate(&self, key: &str) -> PropWithComponents {
		self.prop_gen.generate(key)
	}

	fn _get(&self, key: &str) -> &PropWithComponents {
		self.prop_gen.get(key)
	}
}

impl Generate<ItemWithComponents> for GeneratorBundle {
	fn _generate(&self, key: &str) -> ItemWithComponents {
		self.item_gen.generate(key)
	}

	fn _get(&self, key: &str) -> &ItemWithComponents {
		self.item_gen.get(key)
	}
}

impl Generate<Recipe> for GeneratorBundle {
	fn _generate(&self, key: &str) -> Recipe {
		self.recipe_gen.generate(key)
	}

	fn _get(&self, key: &str) -> &Recipe {
		self.recipe_gen.get(key)
	}
}

impl Generate<Blueprint> for GeneratorBundle {
	fn _generate(&self, key: &str) -> Blueprint {
		self.blueprint_gen.generate(key)
	}

	fn _get(&self, key: &str) -> &Blueprint {
		self.blueprint_gen.get(key)
	}
}

impl Generate<Biome> for GeneratorBundle {
	fn _generate(&self, key: &str) -> Biome {
		self.biome_gen.generate(key)
	}

	fn _get(&self, key: &str) -> &Biome {
		self.biome_gen.get(key)
	}
}

impl Generate<ConstructionLayout> for GeneratorBundle {
	fn _generate(&self, key: &str) -> ConstructionLayout {
		self.construction_gen.generate(key)
	}

	fn _get(&self, key: &str) -> &ConstructionLayout {
		self.construction_gen.get(key)
	}
}

impl GeneratorBundle {
	pub fn load() -> Self {
		GeneratorBundle {
			creature_gen: CreatureGen::load(&()).unwrap(),
			prop_gen: PropGen::load(&()).unwrap(),
			item_gen: ItemGen::load(&()).unwrap(),
			construction_gen: ConstructionGen::load(&()).unwrap(),
			terrain_gen: TerrainGenerator::default(),
			recipe_gen: RecipeGen::load(&()).unwrap(),
			blueprint_gen: BlueprintGen::load(&()).unwrap(),
			biome_gen: BiomeGen::load(&()).unwrap(),
		}
	}

	pub fn generate<T>(&self, key: &str) -> T
	where
		GeneratorBundle: Generate<T>,
		T: Clone + NonIdentify,
	{
		self._generate(key)
	}

	pub fn get<T>(&self, key: &str) -> &T
	where
		GeneratorBundle: Generate<T>,
		T: Clone + NonIdentify,
	{
		self._get(key)
	}

	pub fn generate_with_random_id<T>(&self, key: &str) -> T
	where
		GeneratorBundle: Generate<T>,
		T: Clone + IdentifyMut,
	{
		self._generate(key).with_random_id()
	}

	pub fn generate_with_id<T>(&self, key: &str, id: Uuid) -> T
	where
		GeneratorBundle: Generate<T>,
		T: Clone + IdentifyMut,
	{
		self._generate(key).with_id(id)
	}

	pub fn terrain(&self, key: &TerrainType) -> &Terrain {
		self.terrain_gen.terrain(key)
	}

	pub fn biome(&self, key: &str) -> &Biome {
		self.biome_gen.get(key)
	}
}
