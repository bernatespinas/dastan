use rand::Rng;

use crate::{
	ecs::prelude::PropWithComponents,
	game::UniverseBuilderData,
	generator::GeneratorBundle,
	map::{Position, ZoneBuilder, ZoneFeature},
};

use super::Deploy;

#[derive(Clone, Copy)]
pub struct Forest {}

impl Deploy for Forest {
	fn deploy(
		&self,
		zone_builder: &mut ZoneBuilder,
		_universe_builder_data: &mut UniverseBuilderData,
		gen_bundle: &GeneratorBundle,
	) -> Result<(), String> {
		log::debug!(
			"### Zone {:?}: Forest deployment begins.",
			zone_builder.zone_id()
		);

		let mut rng = rand::thread_rng();

		let number_of_trees = rng.gen_range(500..800);

		for _ in 0..number_of_trees {
			let y = rng.gen_range(0..zone_builder.zone().height());
			let x = rng.gen_range(0..zone_builder.zone().width());

			let position = Position::new(y, x);

			if !zone_builder.zone().tile(&position).has_prop() {
				let tree: PropWithComponents = gen_bundle.generate_with_random_id("pine.ron");

				zone_builder.put(&position, tree);
			}
		}

		zone_builder.set_overview_feature(ZoneFeature::Forest);

		log::debug!(
			"### Zone {:?}: Forest deployment ends.",
			zone_builder.zone_id()
		);

		Ok(())
	}
}
