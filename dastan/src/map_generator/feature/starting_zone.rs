use crate::{
	ecs::prelude::{CreatureWithComponents, ItemWithComponents},
	game::UniverseBuilderData,
	generator::GeneratorBundle,
	map::{Position, ZoneBuilder, ZonePosition},
	map_generator::{spaceship_generator::generate_spaceship, MySporeCreation1},
};

use super::Deploy;

#[derive(Clone)]
pub struct StartingZone {
	pub universe_name: String,
	pub player_pos: Position,
}

impl Deploy for StartingZone {
	fn deploy(
		&self,
		zone_builder: &mut ZoneBuilder,
		universe_builder_data: &mut UniverseBuilderData,
		gen_bundle: &GeneratorBundle,
	) -> Result<(), String> {
		let helmet: ItemWithComponents = gen_bundle.generate_with_random_id("leather_helmet.ron");

		zone_builder.clear_tile_and_put(&Position::new(3, 3), helmet);

		let elf: CreatureWithComponents = gen_bundle.generate_with_random_id("elf.ron");

		zone_builder.clear_tile_and_put(&Position::new(4, 4), elf);

		let spaceship = generate_spaceship(
			&self.universe_name,
			universe_builder_data,
			gen_bundle,
			"my_spore_creation_1.ron",
			&MySporeCreation1 {},
		)
		.unwrap();

		zone_builder.clear_tile_and_put(&Position::new(5, 5), spaceship);

		zone_builder.clear_tile(&self.player_pos);

		let player_zone_pos = ZonePosition::new(*zone_builder.zone_id(), self.player_pos);

		universe_builder_data
			.player_zone_pos
			.replace(player_zone_pos);

		Ok(())
	}
}
