use crate::{game::UniverseBuilderData, generator::GeneratorBundle, map::ZoneBuilder};

mod forest;
pub use forest::Forest;

mod lake;
pub use lake::Lake;

mod starting_zone;
pub use starting_zone::StartingZone;

mod space_starting_zone;
pub use space_starting_zone::SpaceStartingZone;

mod village;
pub use village::Village;

// TODO Rename to `DeployFeature`? Something which has "feature" in its name.
pub trait Deploy: Sync + Send {
	fn deploy(
		&self,
		zone_builder: &mut ZoneBuilder,
		universe_builder_data: &mut UniverseBuilderData,
		gen_bundle: &GeneratorBundle,
	) -> Result<(), String>;
}
