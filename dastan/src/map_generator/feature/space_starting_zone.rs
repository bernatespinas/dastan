use crate::{
	ecs::prelude::{CreatureWithComponents, ItemWithComponents, PropWithComponents},
	ecs_components::TeleportComponent,
	game::UniverseBuilderData,
	generator::GeneratorBundle,
	map::{MapInfo, Position, ZoneBuilder, ZonePosition},
	map_generator::{
		spaceship_generator::generate_spaceship, Earth, MapGenerator, MySporeCreation1,
	},
};

use super::Deploy;

#[derive(Clone)]
pub struct SpaceStartingZone {
	pub map_info_to_copy: MapInfo,
}

impl Deploy for SpaceStartingZone {
	fn deploy(
		&self,
		zone_builder: &mut ZoneBuilder,
		universe_builder_data: &mut UniverseBuilderData,
		gen_bundle: &GeneratorBundle,
	) -> Result<(), String> {
		let earth01_info = self.map_info_to_copy.copy(
			"Earth01".to_string(),
			self.map_info_to_copy.height,
			self.map_info_to_copy.width,
		);

		let earth = Earth {}.generate(&earth01_info, gen_bundle, universe_builder_data)?;
		earth.save()?;

		let mut lalaland_teleporter: PropWithComponents =
			gen_bundle.generate_with_random_id("planet_teleporter.ron");

		lalaland_teleporter
			.components
			.teleport
			.replace(TeleportComponent::AtMap(*earth.id()));

		zone_builder.put(&Position::new(1, 1), lalaland_teleporter);

		let helmet: ItemWithComponents = gen_bundle.generate_with_random_id("leather_helmet.ron");

		zone_builder.put(&Position::new(3, 3), helmet);

		let elf: CreatureWithComponents = gen_bundle.generate_with_random_id("elf.ron");

		zone_builder.put(&Position::new(4, 4), elf);

		let spaceship = generate_spaceship(
			&self.map_info_to_copy.universe_name,
			universe_builder_data,
			gen_bundle,
			"my_spore_creation_1.ron",
			&MySporeCreation1 {},
		)?;

		zone_builder.put(&Position::new(5, 5), spaceship);

		universe_builder_data
			.player_zone_pos
			.replace(ZonePosition::new(
				*zone_builder.zone_id(),
				Position::new(0, 0),
			));

		Ok(())
	}
}
