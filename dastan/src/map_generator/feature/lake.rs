use crate::{
	game::UniverseBuilderData,
	generator::GeneratorBundle,
	map::{Position, ZoneBuilder},
	map_generator::shapes,
	terrain::{Depth, TerrainType},
};

use super::Deploy;

#[derive(Clone, Copy)]
pub struct Lake {}

impl Deploy for Lake {
	fn deploy(
		&self,
		zone_builder: &mut ZoneBuilder,
		_universe_builder_data: &mut UniverseBuilderData,
		_gen_bundle: &GeneratorBundle,
	) -> Result<(), String> {
		// I know the lake will fit in the `Zone`.
		let y = 30;
		let x = 8;
		let rectangle = shapes::Rectangle::new(5, 5, 20, 40);

		let mut top_left = Position::new(y, x);

		for _ in rectangle.range_y() {
			for _ in rectangle.range_x() {
				zone_builder.clear_tile(&top_left);

				zone_builder.terrain_put(&top_left, TerrainType::FreshWater(Depth::Shallow));

				top_left.x += 1;
			}

			top_left.x = x;
			top_left.y += 1;
		}

		Ok(())
	}
}
