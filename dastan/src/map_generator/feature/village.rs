use rand::Rng;

use crate::{
	game::UniverseBuilderData,
	generator::GeneratorBundle,
	map::{ZoneBuilder, ZoneFeature},
	map_generator::zone_layout::ZoneLayout,
};

use super::Deploy;

#[derive(Clone, Copy)]
pub struct Village {}

impl Deploy for Village {
	fn deploy(
		&self,
		zone_builder: &mut ZoneBuilder,
		universe_builder_data: &mut UniverseBuilderData,
		gen_bundle: &GeneratorBundle,
	) -> Result<(), String> {
		// TODO Hardcoded.
		let mut zone_layout = ZoneLayout::new(8);

		for _ in 0..rand::thread_rng().gen_range(7..15) {
			let coords = zone_layout.random_empty_chunk();

			zone_layout.set_construction(coords.0, coords.1, "COZY_COTTAGE");
		}

		{
			let hospital_coords = zone_layout.random_empty_chunk();

			zone_layout.set_construction(hospital_coords.0, hospital_coords.1, "HOSPITAL");
		}

		zone_layout.deploy(zone_builder, universe_builder_data, gen_bundle)?;

		zone_builder.set_overview_feature(ZoneFeature::Settlement);

		Ok(())
	}
}
