use rand::Rng;

use crate::{
	map::{MapInfo, ZoneId},
	map_generator::{feature::Deploy, MapLayout},
	terrain::TerrainType,
};

pub struct DrunkPathSpawner<F: Deploy + Clone> {
	pub surface: TerrainType,
	pub spawn: (TerrainType, Option<F>),

	pub life: u16,
	pub starting_zone_id: ZoneId,
}

impl<F: 'static + Deploy + Clone> DrunkPathSpawner<F> {
	pub fn work(self, map_info: &MapInfo, map_layout: &mut MapLayout) -> u16 {
		let mut cursor = self.starting_zone_id;
		let mut spawn_count = 0;

		for _ in 0..self.life {
			log::error!("\tdrunk_life: {}", self.life);

			if map_layout.zone_base_terrain(cursor.0 as usize, cursor.1 as usize) == self.surface {
				map_layout.set_base_terrain(cursor.0 as usize, cursor.1 as usize, self.spawn.0);

				if let Some(feature) = self.spawn.1.as_ref() {
					map_layout.add_feature(
						cursor.0 as usize,
						cursor.1 as usize,
						Box::new(feature.clone()),
					);
				}

				spawn_count += 1;
			}

			let mut rng = rand::thread_rng();

			let direction_roll = rng.gen_range(0..=3);

			match direction_roll {
				0 => {
					if cursor.1 > 2 {
						cursor.1 -= 1;
					}
				}

				1 => {
					if cursor.1 < map_info.width - 2 {
						cursor.1 += 1;
					}
				}

				2 => {
					if cursor.0 > 2 {
						cursor.0 -= 1;
					}
				}

				_ => {
					if cursor.0 < map_info.height - 2 {
						cursor.0 += 1;
					}
				}
			}
		}

		spawn_count
	}
}
