use crate::{
	biome::Biome, game::UniverseBuilderData, generator::GeneratorBundle,
	map_generator::spaceship_generator::generate_spaceship, map_generator::MapGenerator,
};

use crate::map::{Map, MapBuilder, MapInfo, Position, ZonePosition};

use super::super::MySporeCreation1;

pub struct SeaDepths {}

impl SeaDepths {
	fn gen_and_put_spaceship(
		&self,
		map_builder: &mut MapBuilder,
		universe_builder_data: &mut UniverseBuilderData,
		gen_bundle: &GeneratorBundle,
		map_info: &MapInfo,
	) -> Result<(), String> {
		let spaceship_zone_pos = ZonePosition {
			zone_id: (0, 0),
			pos: Position::new(4, 4),
		};

		let spaceship = generate_spaceship(
			&map_info.universe_name,
			universe_builder_data,
			gen_bundle,
			"my_spore_creation_1.ron",
			&MySporeCreation1 {},
		)?;

		map_builder.map_mut().put(&spaceship_zone_pos, spaceship);

		Ok(())
	}
}

impl MapGenerator for SeaDepths {
	fn generate(
		&self,
		map_info: &MapInfo,
		gen_bundle: &GeneratorBundle,
		universe_builder_data: &mut UniverseBuilderData,
	) -> Result<Map, String> {
		let mut map_builder = MapBuilder::new(map_info)?;

		let sea_depths_biome: Biome = gen_bundle.generate("SEA_DEPTHS");

		for y in 0..map_info.height {
			for x in 0..map_info.width {
				log::debug!(
					"y: {y}/{}    ,    x: {x}/{}",
					map_info.height,
					map_info.width
				);

				let zone_builder = map_builder.zone_builder((y, x), sea_depths_biome.terrain);

				map_builder.insert_zone(zone_builder);
			}
		}

		self.gen_and_put_spaceship(
			&mut map_builder,
			universe_builder_data,
			gen_bundle,
			map_info,
		)?;

		map_builder.build(universe_builder_data)
	}
}
