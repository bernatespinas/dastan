// use rand::Rng;

// use crate::{
// 	map::{
// 		Map, GridMap, Tile, Rectangle,
// 	},
// 	dastan_base::{
// 		Terrain, TerrainBundle,
// 		// Generator,
// 		GeneratorBundle, CreatureBundle, ItemBundle,
// 		utils::Position,
// 		// ListEnum,
// 	},
// 	interface::create_game::WorldInfo,
// };

// use super::MapGenerator;

// const MAX_ROOMS: u16 = 60;
// const ROOM_MAX_SIZE: u16 = 10;
// const ROOM_MIN_SIZE: u16 = 6;
// const MAX_ROOM_MONSTERS: u16 = 10;
// const MAX_ROOM_ITEMS: u16 = 20;

// pub struct Dungeon {
// 	rooms: Vec<Rectangle>,
// }

// impl Dungeon {
// 	fn create_tunnel_h(&self, map: &mut Map, x1: usize, x2: usize, y: usize, terrain: &Terrain) {
// 		for x in std::cmp::min(x1, x2)..=std::cmp::max(x1, x2) {
// 			map.tile_mut_yx(y, x).terrain_put(terrain.clone());
// 		}
// 	}

// 	fn create_tunnel_v(&self, map: &mut Map, y1: usize, y2: usize, x: usize, terrain: &Terrain) {
// 		for y in std::cmp::min(y1, y2)..=std::cmp::max(y1, y2) {
// 			map.tile_mut_yx(y, x).terrain_put(terrain.clone());
// 		}
// 	}

// 	fn create_room(&self, map: &mut Map, room: &Rectangle, terrain: &Terrain) {
// 		for y in room.y1+1..room.y2 {
// 			for x in room.x1+1..room.x2 {
// 				map.tile_mut_yx(y, x).terrain_put(terrain.clone());
// 			}
// 		}
// 	}

// 	fn place_terrain(&mut self, map: &mut Map, terrain_bundle: &TerrainBundle) {
// 		let height = map.height_total() as usize;
// 		let width = map.width_total() as usize;

// 		// Biome regions.
// 		let cold_north = std::ops::Range {
// 			start: 0,
// 			end: height/10,
// 		};
// 		let warm_north = std::ops::Range {
// 			start: height/10,
// 			end: 4*height/10,
// 		};
// 		let hot_region = std::ops::Range {
// 			start: 4*height/10,
// 			end: 6*height/10,
// 		};
// 		let warm_south = std::ops::Range {
// 			start: 6*height/10,
// 			end: 9*height/10,
// 		};
// 		let cold_south = std::ops::Range {
// 			start: 9*height/10,
// 			end: height,
// 		};

// 		for y in cold_north {
// 			map.grid.push(Vec::with_capacity(width));

// 			for _x in 0..width {
// 				map.grid[y].push(
// 					Tile {
// 						creature: None,
// 						item: None,
// 						terrain: terrain_bundle.gen_terrain("WALL_ICE"),
// 					}
// 				);
// 			}
// 		}

// 		for y in warm_north {
// 			map.grid.push(Vec::with_capacity(width));

// 			for _x in 0..width {
// 				map.grid[y].push(
// 					Tile {
// 						creature: None,
// 						item: None,
// 						terrain: terrain_bundle.gen_terrain("WALL_BUSH"),
// 					}
// 				);
// 			}
// 		}

// 		for y in hot_region {
// 			map.grid.push(Vec::with_capacity(width));

// 			for _x in 0..width {
// 				map.grid[y].push(
// 					Tile {
// 						creature: None,
// 						item: None,
// 						terrain: terrain_bundle.gen_terrain("WALL_DIRT"),
// 					}
// 				);
// 			}
// 		}

// 		for y in warm_south {
// 			map.grid.push(Vec::with_capacity(width));

// 			for _x in 0..width {
// 				map.grid[y].push(
// 					Tile {
// 						creature: None,
// 						item: None,
// 						terrain: terrain_bundle.gen_terrain("WALL_BUSH"),
// 					}
// 				);
// 			}
// 		}

// 		for y in cold_south {
// 			map.grid.push(Vec::with_capacity(width));

// 			for _x in 0..width {
// 				map.grid[y].push(
// 					Tile {
// 						creature: None,
// 						item: None,
// 						terrain: terrain_bundle.gen_terrain("WALL_ICE"),
// 					}
// 				);
// 			}
// 		}

// 		for _ in 0..MAX_ROOMS {
// 			let mut rng = rand::thread_rng();
// 			// Random width and height.
// 			let h = rng.gen_range(ROOM_MIN_SIZE as usize, ROOM_MAX_SIZE as usize);
// 			let w = rng.gen_range(ROOM_MIN_SIZE as usize, ROOM_MAX_SIZE as usize);

// 			// Random valid position.
// 			let y = rng.gen_range(0, height - h - 1);
// 			let x = rng.gen_range(0, width - w - 1);

// 			let new_room = Rectangle::new(y, x, h, w);

// 			// See if the other rooms intersect with this one.
// 			let mut failed = false;
// 			for other_room in self.rooms.iter() {
// 				if new_room.intersects_with(other_room) {
// 					failed = true;
// 					break;
// 				}
// 			}

// 			// If there weren't any intersections...
// 			if !failed {
// 				// "Paint" it to the map's tiles.
// 				// TODO: Always FLOOR_DIRT? FLOOR_ICE, FLOOR_GRASS...
// 				self.create_room(map, &new_room, &terrain_bundle.gen_terrain("FLOOR_DIRT"));
// 			}

// 			// Connect all rooms after the first with the previous one.
// 			// NOTE: > 0 and not > 1 because I append rooms at the end! Even thought the len is 0, I've got a generated room that isn't in the Vec yet.
// 			if !self.rooms.is_empty() {
// 				// Center posinates of the previous room.
// 				let (prev_y, prev_x) = self.rooms.last().unwrap().center();
// 				let (new_y, new_x) = new_room.center();

// 				// TODO: Use the gen_bool thingy.
// 				if rand::thread_rng().gen_range(0, 1) == 1 {
// 					// First move vertically, then horizontally.
// 					self.create_tunnel_h(map,
// 						prev_x, new_x, prev_y,
// 						&terrain_bundle.gen_terrain("FLOOR_DIRT"));
// 					self.create_tunnel_v(map,
// 						prev_y, new_y, new_x,
// 						&terrain_bundle.gen_terrain("FLOOR_DIRT"));
// 				}
// 				else {
// 					// First move horizontally, then vertically.
// 					self.create_tunnel_h(map,
// 						prev_x, new_x, new_y,
// 						&terrain_bundle.gen_terrain("FLOOR_DIRT"));
// 					self.create_tunnel_v(map,
// 						prev_y, new_y, prev_x,
// 						&terrain_bundle.gen_terrain("FLOOR_DIRT"));
// 				}
// 			}

// 			// Finally, append the room to the list.
// 			self.rooms.push(new_room);
// 		}
// 	}

// 	fn place_creatures(
// 		&self,
// 		map: &mut Map,
// 		room: &Rectangle,
// 		creature_bundle: &CreatureBundle
// 	) {
// 		let mut rng = rand::thread_rng();
// 		let num_monsters = rng.gen_range(0, MAX_ROOM_MONSTERS);

// 		for _i in 0..num_monsters {
// 			let y: usize = rng.gen_range(
// 				room.y1 +1,
// 				room.y2 -1,
// 			);
// 			let x: usize = rng.gen_range(
// 				room.x1 +1,
// 				room.x2 -1,
// 			);

// 			if !map.tile_yx(y, x).blocks_passage() {
// 				let n: u8 = rng.gen_range(0, 100);
// 				let creature =
// 					if n < 70 {creature_bundle.gen_creature("ORC")}
// 					else if n < 90 {creature_bundle.gen_creature("TROLL")}
// 					else {
// 						map.ais.push(Position {y, x});
// 						creature_bundle.gen_creature("COW")
// 					}
// 				;
// 				// self.ais.push(Position{
// 				// 	y: y as usize,
// 				// 	x: x as usize,
// 				// });
// 				map.tile_mut_yx(y, x).creature_put(creature);
// 			}
// 		}
// 	}

// 	// fn place_vegetation(&self, _map: &mut Map, _room: &Rectangle) {

// 	// }

// 	fn place_items(&self, map: &mut Map, room: &Rectangle, item_bundle: &ItemBundle) {
// 		let mut rng = rand::thread_rng();
// 		let num_items: u16 = rng.gen_range(0, MAX_ROOM_ITEMS);

// 		for _i in 0..num_items {
// 			// let pos = Position::new(
// 			// 	rng.gen_range(room.y1+1, room.y2-1),
// 			// 	rng.gen_range(room.x1+1, room.x2-1)
// 			// ):
// 			let y: usize = rng.gen_range(
// 				room.y1 +1,
// 				room.y2 -1,
// 			);
// 			let x: usize = rng.gen_range(
// 				room.x1 +1,
// 				room.x2 -1,
// 			);

// 			if !map.tile_yx(y, x).blocks_passage() && !map.tile_yx(y, x).item_there() {
// 				let n: u8 = rng.gen_range(0, 100);
// 				// TODO What's better, to call .to_item() or do it _____ -_-
// 				// let item =
// 				// 	if n < 20 {self.food_gen.get("POTION_HEALING").to_item()}
// 				// 	else if n < 90 {self.equipment_gen.get("HELMET_LEATHER").to_item()}
// 				// 	else {self.equipment_gen.get("SWORD_IRON").to_item()}
// 				// ;
// 				// map.put_item(y, x, item);
// 				if n < 20 {
// 					map.tile_mut_yx(y, x).item_put(
// 						item_bundle.gen_item("POTION_HEALING")
// 					);
// 				}
// 				else if n < 90 {
// 					map.tile_mut_yx(y, x).item_put(
// 						item_bundle.gen_item("HELMET_LEATHER")
// 					);
// 				}
// 				else {
// 					map.tile_mut_yx(y, x).item_put(
// 						item_bundle.gen_item("SWORD_IRON")
// 					);
// 				}
// 			}
// 		}
// 	}

// 	fn place_buildings(&self, map: &mut Map, gen_bundle: &GeneratorBundle) {
// 		map.deploy_building(0, 0, &gen_bundle.gen_building("COZY_COTTAGE"), gen_bundle);
// 		map.deploy_building(30, 0, &gen_bundle.gen_building("HOSPITAL"), gen_bundle);
// 	}

// 	fn place_player(&self, map: &mut Map, room: &Rectangle) {
// 		let (y, x) = room.center();
// 		map.player_pos = Position{y: y as usize, x: x as usize};
// 	}
// }

// impl MapGenerator for Dungeon {
// 	fn new() -> Dungeon {
// 		Dungeon {
// 			rooms: Vec::new(),
// 		}
// 	}

// 	fn generate(&mut self, world: &WorldInfo, gen_bundle: &GeneratorBundle) -> Map {
// 		let height = world.height;
// 		let width = world.width;

// 		let mut map = Map::new(height as u16, width as u16);

// 		self.place_terrain(&mut map, &gen_bundle.terrain_bundle);
// 		self.place_buildings(&mut map, gen_bundle);

// 		// Clone them to be able to iterate through them AND also modify generator.
// 		let the_rooms = self.rooms.clone();

// 		self.place_player(&mut map, &the_rooms.last().unwrap());

// 		for room in &the_rooms {
// 			self.place_creatures(&mut map, room, &gen_bundle.creature_bundle);
// 			self.place_items(&mut map, room, &gen_bundle.item_bundle);
// 		}

// 		map
// 	}
// }