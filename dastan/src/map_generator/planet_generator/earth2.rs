use rand::Rng;

use crate::{
	game::UniverseBuilderData,
	generator::GeneratorBundle,
	map::{Map, MapBuilder, MapInfo, Position},
	map_generator::{
		algorithms::DrunkPathSpawner,
		feature::{Forest, StartingZone, Village},
		MapGenerator, MapLayout,
	},
	terrain::{Depth, Material, TerrainType},
};

use super::random_zone_id;

pub struct Earth2 {}

impl MapGenerator for Earth2 {
	fn generate(
		&self,
		map_info: &MapInfo,
		gen_bundle: &GeneratorBundle,
		universe_builder_data: &mut UniverseBuilderData,
	) -> Result<Map, String> {
		let mut map_layout = MapLayout::with_base_terrain(
			map_info.height as usize,
			map_info.width as usize,
			TerrainType::SaltWater(Depth::Deep),
		);

		let center = (map_info.height / 2, map_info.width / 2);

		map_layout.set_base_terrain(
			center.0 as usize,
			center.1 as usize,
			TerrainType::Floor(Material::Dirt),
		);

		map_layout.add_feature(
			center.0 as usize,
			center.1 as usize,
			Box::new(StartingZone {
				universe_name: map_info.universe_name.clone(),
				player_pos: Position::new(0, 0),
			}),
		);

		let zones_count = map_info.height * map_info.width;
		// TODO Tweak me.
		let desired_floor_zones = zones_count as usize / 3;

		let mut floor_tile_count = 0;
		let mut digger_count = 0;

		while floor_tile_count < desired_floor_zones {
			log::error!("digger_count: {digger_count}");
			log::error!("floor_tile_count: {floor_tile_count}");

			let spawn_count = DrunkPathSpawner {
				surface: TerrainType::SaltWater(Depth::Deep),
				spawn: (TerrainType::Floor(Material::Dirt), None::<Forest>),

				life: 200,
				starting_zone_id: center,
			}
			.work(map_info, &mut map_layout);

			floor_tile_count += spawn_count as usize;

			digger_count += 1;
		}

		deploy_grass_with_potential_forest(&mut map_layout, map_info);
		deploy_villages(&mut map_layout, map_info, 25);

		let map_builder = MapBuilder::new(map_info)?.with_layout(
			&map_layout,
			universe_builder_data,
			gen_bundle,
		)?;

		universe_builder_data
			.player_map_id
			.replace(*map_builder.map().id());

		map_builder.build(universe_builder_data)
	}
}

fn deploy_grass_with_potential_forest(map_layout: &mut MapLayout, map_info: &MapInfo) {
	for _ in 0..60 {
		let mut zone_id = random_zone_id(map_info.height, map_info.width);

		while map_layout
			.zone_base_terrain(zone_id.0 as usize, zone_id.1 as usize)
			.is_liquid()
		{
			zone_id = random_zone_id(map_info.height, map_info.width)
		}

		let forest_chance = rand::thread_rng().gen_range(0..=100);

		let forest_feature = if forest_chance > 60 {
			Some(Forest {})
		} else {
			None
		};

		DrunkPathSpawner {
			surface: TerrainType::Floor(Material::Dirt),
			spawn: (TerrainType::Floor(Material::Grass), forest_feature),
			life: 25,
			starting_zone_id: zone_id,
		}
		.work(map_info, map_layout);
	}
}

fn deploy_villages(map_layout: &mut MapLayout, map_info: &MapInfo, mut amount_of_villages: u16) {
	let mut rng = rand::thread_rng();

	while amount_of_villages > 0 {
		let random_zone_id_y = rng.gen_range(0..map_info.height) as usize;
		let random_zone_id_x = rng.gen_range(0..map_info.width) as usize;

		if map_layout
			.zone_features(random_zone_id_y, random_zone_id_x)
			.is_empty() && map_layout
			.zone_base_terrain(random_zone_id_y, random_zone_id_x)
			.is_floor()
		{
			map_layout.add_feature(random_zone_id_y, random_zone_id_x, Box::new(Village {}));
			amount_of_villages -= 1;
		}
	}
}
