use crate::{
	game::UniverseBuilderData,
	generator::GeneratorBundle,
	map::{Map, MapBuilder, MapInfo, Position},
	map_generator::{
		feature::{Deploy, Forest, Lake, StartingZone},
		MapGenerator, MapLayout,
	},
	terrain::{Depth, Material, TerrainType},
};

pub struct Earth {}

impl MapGenerator for Earth {
	fn generate(
		&self,
		map_info: &MapInfo,
		gen_bundle: &GeneratorBundle,
		universe_builder_data: &mut UniverseBuilderData,
	) -> Result<Map, String> {
		let mut map_layout = MapLayout::new(map_info.height as usize, map_info.width as usize);

		let biome_height = map_info.height / 8;

		let mut y = 0;

		set_map_layout_rows(
			&mut map_layout,
			y,
			biome_height,
			map_info.width,
			TerrainType::Floor(Material::Ice),
		);

		y += biome_height;

		set_map_layout_rows(
			&mut map_layout,
			y,
			biome_height,
			map_info.width,
			TerrainType::SaltWater(Depth::Deep),
		);

		y += biome_height;

		set_map_layout_rows(
			&mut map_layout,
			y,
			biome_height * 2,
			map_info.width,
			TerrainType::Floor(Material::Grass),
		);

		set_map_layout_rows_feature(
			&mut map_layout,
			y,
			biome_height * 2,
			map_info.width,
			Forest {},
		);

		y += biome_height * 2;

		set_map_layout_rows(
			&mut map_layout,
			y,
			biome_height,
			map_info.width,
			TerrainType::Floor(Material::Dirt),
		);

		y += biome_height;

		set_map_layout_rows(
			&mut map_layout,
			y,
			biome_height,
			map_info.width,
			TerrainType::Floor(Material::Grass),
		);

		set_map_layout_rows_feature(&mut map_layout, y, biome_height, map_info.width, Forest {});

		y += biome_height;

		set_map_layout_rows(
			&mut map_layout,
			y,
			biome_height,
			map_info.width,
			TerrainType::SaltWater(Depth::Deep),
		);

		y += biome_height;

		set_map_layout_rows(
			&mut map_layout,
			y,
			biome_height,
			map_info.width,
			TerrainType::Floor(Material::Ice),
		);

		// let mut rng = rand::thread_rng();
		// let y = rng.gen_range(1..map_info.height - 1) as usize;
		// let x = rng.gen_range(1..map_info.width - 1) as usize;

		map_layout.add_feature(
			13,
			0,
			Box::new(StartingZone {
				universe_name: map_info.universe_name.clone(),
				player_pos: Position::new(0, 0),
			}),
		);

		map_layout.add_feature(14, 0, Box::new(Lake {}));

		let map_builder = MapBuilder::new(map_info)?.with_layout(
			&map_layout,
			universe_builder_data,
			gen_bundle,
		)?;

		universe_builder_data
			.player_map_id
			.replace(*map_builder.map().id());

		map_builder.build(universe_builder_data)
	}
}

fn set_map_layout_rows(
	map_layout: &mut MapLayout,
	first_row: u16,
	rows: u16,
	columns: u16,
	base_terrain: TerrainType,
) {
	for y in first_row..first_row + rows {
		for x in 0..columns {
			map_layout.set_base_terrain(y as usize, x as usize, base_terrain);
		}
	}
}

fn set_map_layout_rows_feature(
	map_layout: &mut MapLayout,
	first_row: u16,
	rows: u16,
	columns: u16,
	feature: impl Deploy + Clone + 'static,
) {
	for y in first_row..first_row + rows {
		for x in 0..columns {
			map_layout.add_feature(y as usize, x as usize, Box::new(feature.clone()));
		}
	}
}
