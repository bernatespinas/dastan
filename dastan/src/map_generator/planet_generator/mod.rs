use rand::Rng;

use crate::map::ZoneId;

mod sea_depths;
pub use sea_depths::SeaDepths;

mod earth;
pub use earth::Earth;

mod earth2;
pub use earth2::Earth2;

fn random_zone_id(height: u16, width: u16) -> ZoneId {
	let mut rng = rand::thread_rng();

	let y = rng.gen_range(0..height);
	let x = rng.gen_range(0..width);

	(y, x)
}
