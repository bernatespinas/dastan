use crate::terrain::TerrainType;

use super::feature::Deploy;

// TODO Rename to MapSurfaceLayout? Will caves need a different thing?
pub struct MapLayout {
	pub base_terrains: Vec<Vec<Option<TerrainType>>>,
	// TODO The order in which features are applied might be important.
	features: Vec<Vec<Vec<Box<dyn Deploy>>>>,
}

impl MapLayout {
	pub fn new(height: usize, width: usize) -> Self {
		// In order to be able to use `vec![vec![Vec::new(); width]; height]`,
		// `DeployFeature` must require `Clone`. Since I don't actually need
		// that, I create `features` like this instead.
		let features = (0..height)
			.map(|_| (0..width).map(|_| Vec::new()).collect())
			.collect();

		Self {
			base_terrains: vec![vec![None; width]; height],
			features,
		}
	}

	pub fn with_base_terrain(height: usize, width: usize, base_terrain: TerrainType) -> Self {
		// In order to be able to use `vec![vec![Vec::new(); width]; height]`,
		// `DeployFeature` must require `Clone`. Since I don't actually need
		// that, I create `features` like this instead.
		let features = (0..height)
			.map(|_| (0..width).map(|_| Vec::new()).collect())
			.collect();

		Self {
			base_terrains: vec![vec![Some(base_terrain); width]; height],
			features,
		}
	}

	pub fn set_base_terrain(&mut self, y: usize, x: usize, base_terrain: TerrainType) {
		self.base_terrains
			.get_mut(y)
			.unwrap()
			.get_mut(x)
			.unwrap()
			.replace(base_terrain);
	}

	pub fn add_feature(&mut self, y: usize, x: usize, feature: Box<dyn Deploy>) {
		self.features
			.get_mut(y)
			.unwrap()
			.get_mut(x)
			.unwrap()
			.push(feature);
	}

	pub fn remove_features(&mut self, y: usize, x: usize) {
		self.features
			.get_mut(y)
			.unwrap()
			.get_mut(x)
			.unwrap()
			.clear();
	}

	pub fn zone_base_terrain(&self, y: usize, x: usize) -> TerrainType {
		self.base_terrains
			.get(y)
			.unwrap()
			.get(x)
			.unwrap()
			.unwrap_or_else(|| panic!("Zone ({y}, {x}) has not been given a TerrainType!"))
	}

	pub fn zone_features(&self, y: usize, x: usize) -> &Vec<Box<dyn Deploy>> {
		self.features.get(y).unwrap().get(x).unwrap()
	}

	pub fn count_tiles_with_base_terrain(&self, base_terrain: TerrainType) -> usize {
		self.base_terrains
			.iter()
			.flat_map(|row| {
				row.iter().map(|zone| match zone {
					None => 0,
					Some(bt) => {
						if *bt == base_terrain {
							1
						} else {
							0
						}
					}
				})
			})
			.sum()
	}
}
