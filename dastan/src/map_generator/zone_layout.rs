use rand::{seq::SliceRandom, Rng};

use crate::{
	game::UniverseBuilderData,
	generator::GeneratorBundle,
	map::{Position, ZoneBuilder},
};

use super::{
	construction::{ConstructionDeployer, ConstructionLayout},
	feature::Deploy,
};

pub struct ZoneLayout {
	/// The amount of chunks that will be in each row. Since `Zone`s are squares,
	/// this number will match the amount of chunks in each column too.
	chunks_per_row: u16,

	construction_layouts: Vec<Vec<Option<&'static str>>>,
}

impl ZoneLayout {
	pub fn new(chunks_per_row: u16) -> Self {
		Self {
			chunks_per_row,
			construction_layouts: vec![
				vec![None; chunks_per_row as usize];
				chunks_per_row as usize
			],
		}
	}

	pub fn set_construction(&mut self, y: usize, x: usize, construction_layout: &'static str) {
		self.construction_layouts
			.get_mut(y)
			.unwrap()
			.get_mut(x)
			.unwrap()
			.replace(construction_layout);
	}

	/// Returns
	pub fn random_empty_chunk(&self) -> (usize, usize) {
		let empty_chunks: Vec<(usize, usize)> = self
			.construction_layouts
			.iter()
			.enumerate()
			.flat_map(|(row, row_chunks)| {
				row_chunks
					.iter()
					.enumerate()
					.filter_map(move |(column, chunk)| {
						if chunk.is_none() {
							Some((row, column))
						} else {
							None
						}
					})
			})
			.collect();

		let mut rng = rand::thread_rng();

		*empty_chunks
			.choose(&mut rng)
			.expect("There are no remaining empty chunks.")
	}
}

impl Deploy for ZoneLayout {
	fn deploy(
		&self,
		zone_builder: &mut ZoneBuilder,
		_universe_builder_data: &mut UniverseBuilderData,
		gen_bundle: &GeneratorBundle,
	) -> Result<(), String> {
		let mut rng = rand::thread_rng();

		for chunk_y in 0..self.chunks_per_row as usize {
			for chunk_x in 0..self.chunks_per_row as usize {
				if let Some(construction_layout_code) = self
					.construction_layouts
					.get(chunk_y)
					.unwrap()
					.get(chunk_x)
					.unwrap()
				{
					let construction_layout: &ConstructionLayout =
						gen_bundle.get(construction_layout_code);

					// Shift the y and x coordinates to avoid having all constructions aligned
					// between themselves.
					let y_shift = rng.gen_range(
						0..zone_builder.zone().height() / self.chunks_per_row
							- construction_layout.height() as u16,
					);

					let x_shift = rng.gen_range(
						0..zone_builder.zone().width() / self.chunks_per_row
							- construction_layout.width() as u16,
					);

					let y = chunk_y as u16 * zone_builder.zone().height() / self.chunks_per_row
						+ y_shift;

					let x = chunk_x as u16 * zone_builder.zone().width() / self.chunks_per_row
						+ x_shift;

					ConstructionDeployer::new(Position::new(y, x), &construction_layout)
						.deploy(zone_builder, gen_bundle)?;
				}
			}
		}

		Ok(())
	}
}
