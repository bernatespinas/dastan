use crate::{
	biome::Biome,
	game::UniverseBuilderData,
	map::{Map, MapBuilder, MapInfo},
	map_generator::{feature::SpaceStartingZone, MapLayout},
};

use super::{GeneratorBundle, MapGenerator};

pub struct Space {}

impl MapGenerator for Space {
	fn generate(
		&self,
		map_info: &MapInfo,
		gen_bundle: &GeneratorBundle,
		universe_builder_data: &mut UniverseBuilderData,
	) -> Result<Map, String> {
		let mut map_layout = MapLayout::new(map_info.height as usize, map_info.width as usize);

		let space_biome: Biome = gen_bundle.generate("space.ron");

		for y in 0..map_info.height {
			for x in 0..map_info.width {
				log::debug!(
					"y: {y}/{}    ,    x: {x}/{}",
					map_info.height,
					map_info.width
				);

				map_layout.set_base_terrain(y as usize, x as usize, space_biome.terrain);
			}
		}

		map_layout.add_feature(
			0,
			0,
			Box::new(SpaceStartingZone {
				map_info_to_copy: map_info.clone(),
			}),
		);

		let map_builder = MapBuilder::new(map_info)?.with_layout(
			&map_layout,
			universe_builder_data,
			gen_bundle,
		)?;

		universe_builder_data
			.player_map_id
			.replace(*map_builder.map().id());

		map_builder.build(universe_builder_data)
	}
}
