use crate::ecs::prelude::NonIdentify;

use super::ConstructionMapping;

/// A struct that contains a "semi-visual" representation of a room
/// using characters chosen by the programmer/developer and
/// a HashMap that maps these to actual objects.
#[derive(Clone, Serialize, Deserialize)]
pub struct ConstructionLayout {
	pub matrix: Vec<Vec<char>>,
	pub mapping: ConstructionMapping,
}

impl ConstructionLayout {
	/// Creation fails when a character that hasn't been defined is used.
	/// Spaces are ignored because they can help align characters.
	pub fn new(mut layout: String, mapping: ConstructionMapping) -> Result<Self, String> {
		layout.retain(|x| x != ' ' && x != '\t');

		let mut matrix: Vec<Vec<char>> = Vec::with_capacity(layout.lines().count());

		for line in layout.lines() {
			let mut row: Vec<char> = Vec::with_capacity(line.len());

			for icon in line.chars() {
				if !mapping.contains_key(&icon) {
					return Err(format!("Missing key '{icon}' (there might be more)."));
				}
				row.push(icon);
			}

			matrix.push(row);
		}

		Ok(ConstructionLayout { matrix, mapping })
	}

	pub fn height(&self) -> usize {
		self.matrix.len()
	}

	pub fn width(&self) -> usize {
		self.matrix[0].len()
	}
}

impl NonIdentify for ConstructionLayout {}
