use crate::terrain::TerrainType;

#[derive(Clone, Serialize, Deserialize)]
pub struct ConstructionTile {
	pub terrain_type: TerrainType,
	pub creature: Option<String>,
	pub prop: Option<String>,
	pub item: Option<String>,
}

// impl ConstructionTile {
// 	pub fn new(terrain_type: TerrainType) -> Self {
// 		ConstructionTile {
// 			terrain_type,
// 			creature: None,
// 			prop: None,
// 			item: None,
// 		}
// 	}

// 	pub fn with_creature(mut self, creature: &'static str) -> Self {
// 		self.creature = Some(creature.to_string());

// 		self
// 	}

// 	pub fn with_prop(mut self, prop: &str) -> Self {
// 		self.prop.replace(prop.to_string());

// 		self
// 	}

// 	pub fn with_item(mut self, item: &'static str) -> Self {
// 		self.item = Some(item.to_string());

// 		self
// 	}
// }
