use crate::{
	ecs::prelude::{CreatureWithComponents, ItemWithComponents, PropWithComponents},
	generator::GeneratorBundle,
	map::{Position, ZoneBuilder},
};

use super::ConstructionLayout;

/// Deploys a `ConstructionLayout` in a `Zone`.
pub struct ConstructionDeployer<'a> {
	/// The position in the map where the top left corner of the construction will
	/// be.
	top_left_position: Position,

	/// The `ConstructionLayout` that will be deployed.
	construction_layout: &'a ConstructionLayout,
}

impl<'a> ConstructionDeployer<'a> {
	/// `top_left_position`: the position in the map where the top left corner of
	/// the construction will be.
	pub fn new(top_left_position: Position, construction_layout: &'a ConstructionLayout) -> Self {
		Self {
			top_left_position,
			construction_layout,
		}
	}

	pub fn deploy(
		&self,
		zone_builder: &mut ZoneBuilder,
		gen_bundle: &GeneratorBundle,
	) -> Result<(), String> {
		let mut pos = self.top_left_position;

		for row in &self.construction_layout.matrix {
			for icon in row {
				zone_builder.clear_tile(&pos);

				let construction_tile = self.construction_layout.mapping.get(icon).unwrap();

				zone_builder.terrain_put(&pos, construction_tile.terrain_type);

				if let Some(creature_code) = &construction_tile.creature {
					let creature: CreatureWithComponents =
						gen_bundle.generate_with_random_id(creature_code);

					zone_builder.put(&pos, creature);
				}

				if let Some(prop_code) = &construction_tile.prop {
					let prop: PropWithComponents = gen_bundle.generate_with_random_id(prop_code);

					zone_builder.put(&pos, prop);
				}

				if let Some(item_code) = &construction_tile.item {
					let item: ItemWithComponents = gen_bundle.generate_with_random_id(item_code);

					zone_builder.put(&pos, item);
				}

				pos.x += 1;
			}

			pos.y += 1;

			pos.x = self.top_left_position.x;
		}

		Ok(())
	}
}
