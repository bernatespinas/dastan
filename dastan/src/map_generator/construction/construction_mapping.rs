use std::collections::HashMap;

use super::ConstructionTile;

pub type ConstructionMapping = HashMap<char, ConstructionTile>;
