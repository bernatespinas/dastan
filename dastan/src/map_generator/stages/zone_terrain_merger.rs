use std::collections::HashMap;

use rand::Rng;

use direction_simple::Direction;

use crate::{
	map::{Map, Position, ZoneId, ZonePosition},
	map_generator::MapLayout,
};

pub struct ZoneTerrainMerger {}

impl ZoneTerrainMerger {
	pub fn work(map: &mut Map, map_layout: &MapLayout) -> Result<(), String> {
		let mut zones_to_merge: HashMap<ZoneId, Vec<Direction>> = HashMap::new();

		for (y, row) in map_layout.base_terrains.iter().enumerate() {
			for (x, column) in row.iter().enumerate() {
				let terrain_right = map_layout
					.base_terrains
					.get(y)
					.unwrap()
					.get(if x + 1 >= map.map_width as usize {
						0
					} else {
						x + 1
					})
					.unwrap()
					.unwrap();

				let mut directions = Vec::new();

				if column.unwrap() != terrain_right {
					directions.push(Direction::Right);
				}

				let terrain_down = map_layout
					.base_terrains
					.get(if y + 1 >= map.map_height as usize {
						0
					} else {
						y + 1
					})
					.unwrap()
					.get(x)
					.unwrap()
					.unwrap();

				if column.unwrap() != terrain_down {
					directions.push(Direction::Down);
				}

				if !directions.is_empty() {
					zones_to_merge.insert((y as u16, x as u16), directions);
				}
			}
		}

		let mut rng = rand::thread_rng();

		for (zone_id, directions) in zones_to_merge {
			map.load_zone(&zone_id)?;
			for direction in directions {
				match direction {
					Direction::Right => {
						let zone_id_right = (
							zone_id.0,
							if zone_id.1 + 1 >= map.map_width {
								0
							} else {
								zone_id.1 + 1
							},
						);

						log::error!("la zone_id_right: {zone_id_right:?}");

						map.load_zone(&zone_id_right)?;

						// TODO Tweak me.
						let mut x: i32 = rng.gen_range(-5..=5);

						for y in 0..map.zone_height {
							if x <= 0 {
								let terrain_to_put = map
									.zone_tile(&ZonePosition::new(
										zone_id_right,
										Position::new(0, 0),
									))
									.terrain_type;

								for xx in x..0 {
									let zone_pos = ZonePosition::new(
										zone_id,
										Position::new(y, map.zone_width - xx.unsigned_abs() as u16),
									);

									map.zone_tile_mut(&zone_pos).terrain_put(terrain_to_put);
								}

								// x += rng.ge gen_range(-1..=1);
								if rng.gen_bool(0.5) {
									x += 1;
								} else {
									x -= 1;
								}
							} else {
								let terrain_to_put = map
									.zone_tile(&ZonePosition::new(zone_id, Position::new(0, 0)))
									.terrain_type;

								for xx in 0..x {
									let zone_pos = ZonePosition::new(
										zone_id_right,
										Position::new(y, xx as u16),
									);

									map.zone_tile_mut(&zone_pos).terrain_put(terrain_to_put);
								}

								// x += rng.gen_range(-1..=1);
								if rng.gen_bool(0.5) {
									x += 1;
								} else {
									x -= 1;
								}
							};
						}
					}
					Direction::Down => {}
					_ => unreachable!(),
				};
			}
		}

		Ok(())
	}
}
