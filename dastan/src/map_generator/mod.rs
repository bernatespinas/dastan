use crate::{
	game::UniverseBuilderData,
	generator::GeneratorBundle,
	map::{Map, MapInfo},
};

mod map_layout;
pub use map_layout::MapLayout;

mod zone_layout;
pub use zone_layout::ZoneLayout;

mod feature;

mod planet_generator;
pub use planet_generator::{Earth, Earth2, SeaDepths};

mod spaceship_generator;
pub use spaceship_generator::MySporeCreation1;

mod space_generator;
pub use space_generator::Space;

mod shapes;

pub mod construction;

mod algorithms;

mod stages;

pub trait MapGenerator {
	fn generate(
		&self,
		map_info: &MapInfo,
		gen_bundle: &GeneratorBundle,
		universe_builder_data: &mut UniverseBuilderData,
	) -> Result<Map, String>;
}
