use std::ops::Range;

#[derive(Clone)]
pub struct Rectangle {
	y1: usize,
	x1: usize,
	y2: usize,
	x2: usize,
}

impl Rectangle {
	pub fn new(y: usize, x: usize, h: usize, w: usize) -> Rectangle {
		Rectangle {
			y1: y,
			x1: x,
			y2: y + h,
			x2: x + w,
		}
	}

	pub fn center(&self) -> (usize, usize) {
		let center_y = (self.y1 + self.y2) / 2;
		let center_x = (self.x1 + self.x2) / 2;
		(center_y, center_x)
	}

	pub fn intersects_with(&self, other: &Rectangle) -> bool {
		self.y1 <= other.y2 && self.y2 >= other.y1 && self.x1 <= other.x2 && self.x2 >= other.x1
	}

	pub fn range_y(&self) -> Range<usize> {
		self.y1..self.y2
	}

	pub fn range_x(&self) -> Range<usize> {
		self.x1..self.x2
	}
}
