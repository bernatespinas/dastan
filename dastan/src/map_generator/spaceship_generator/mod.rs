use std::collections::HashMap;

use crate::{
	ecs::prelude::{CreatureWithComponents, Entity, EntityId, Identify},
	ecs_components::TeleportComponent,
	game::UniverseBuilderData,
	map::MapMode,
};

use super::{GeneratorBundle, MapGenerator, MapInfo};

mod my_spore_creation_1;
pub use my_spore_creation_1::MySporeCreation1;

pub fn generate_spaceship<M: MapGenerator>(
	universe_name: &str,
	universe_builder_data: &mut UniverseBuilderData,
	gen_bundle: &GeneratorBundle,
	spaceship_gen_key: &str,
	spaceship_map_generator: &M,
) -> Result<CreatureWithComponents, String> {
	let mut spaceship: CreatureWithComponents =
		gen_bundle.generate_with_random_id(spaceship_gen_key);

	let spaceship_teleporter_id = EntityId::new_v4();

	let map_info = MapInfo {
		universe_name: universe_name.to_string(),
		name: spaceship.entity.name().clone(),
		map_mode: MapMode::Simple,

		height: 1,
		width: 1,

		zone_height: 10,
		zone_width: 10,

		entry_points: HashMap::new(),
	}
	.with_entry_point("spaceship", *spaceship.id(), None)
	.with_entry_point("spaceship_teleporter", spaceship_teleporter_id, None);

	let spaceship_map =
		spaceship_map_generator.generate(&map_info, gen_bundle, universe_builder_data)?;

	spaceship_map.save()?;

	spaceship
		.components
		.teleport
		.replace(TeleportComponent::AtEntity(spaceship_teleporter_id));

	Ok(spaceship)
}
