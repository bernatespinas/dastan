use crate::{
	ecs::prelude::*,
	ecs_components::TeleportComponent,
	game::UniverseBuilderData,
	map::{Map, MapBuilder, Position, ZonePosition},
	terrain::{Material, TerrainType},
};

use super::{GeneratorBundle, MapGenerator, MapInfo};

pub struct MySporeCreation1 {}

impl MapGenerator for MySporeCreation1 {
	fn generate(
		&self,
		map_info: &MapInfo,
		gen_bundle: &GeneratorBundle,
		universe_builder_data: &mut UniverseBuilderData,
	) -> Result<Map, String> {
		let mut map_builder = MapBuilder::new(map_info)?;

		map_builder
			.insert_zone(map_builder.zone_builder((0, 0), TerrainType::Floor(Material::Dirt)));

		let spaceship_id = map_info.entry_points.get("spaceship").unwrap().entity_id;

		let mut computer: PropWithComponents = gen_bundle.generate_with_random_id("computer.ron");

		computer
			.components
			.computer
			.as_mut()
			.unwrap()
			.linked_tracked_entities
			.insert(spaceship_id);

		let computer_zone_pos = ZonePosition {
			zone_id: (0, 0),
			pos: Position::new(6, 6),
		};

		map_builder.map_mut().put(&computer_zone_pos, computer);

		let teleporter_id = map_info
			.entry_points
			.get("spaceship_teleporter")
			.unwrap()
			.entity_id;

		let mut teleporter: PropWithComponents =
			gen_bundle.generate_with_id("planet_teleporter.ron", teleporter_id);

		let teleporter_zone_pos = ZonePosition {
			zone_id: (0, 0),
			pos: Position::new(3, 3),
		};

		teleporter.components.teleport = Some(TeleportComponent::NextToEntity(spaceship_id));

		universe_builder_data
			.entity_map_tracker
			.set(*teleporter.id(), *map_builder.map().id());

		map_builder.map_mut().put(&teleporter_zone_pos, teleporter);

		map_builder.build(universe_builder_data)
	}
}
