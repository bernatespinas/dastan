use crate::event::Message;

#[derive(Default)]
pub struct MessageLog {
	pub messages: Vec<Message>,
}

impl MessageLog {
	pub fn add(&mut self, message: Message) {
		self.messages.push(message);
	}
}
