use std::fmt::Debug;

pub trait ListEnum: Debug + Sized {
	fn list() -> Vec<Self>;

	fn list_string() -> Vec<String> {
		Self::list()
			.into_iter()
			.map(|debug_enum| format!("{debug_enum:?}"))
			.collect()
	}
}
