use crate::ecs::prelude::EntityId;

use super::Severity;

/// A message that describes an `Event`.
#[derive(Debug)]
pub struct Message {
	/// The text that will be shown to describe the `Event`.
	pub text: String,

	/// The `Entity` the `Message` is directed to. `None` if it is a global `Message`.
	pub receiver: Option<EntityId>,

	/// The severity of the message for its receiver.
	pub severity_for_receiver: Severity,
}
