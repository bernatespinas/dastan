use crate::{ecs::prelude::EntityId, map::ZonePosition};

mod message;
pub use message::Message;

mod severity;
pub use severity::Severity;

#[derive(Default)]
pub struct Event {
	/// The `Entity` that sent the `Event`. `None` if it was the `Map`/`Game`
	/// itself.
	pub event_sender: Option<EntityId>,

	/// A message that will be shown when the `Event` takes place to describe
	/// it.
	pub message: Option<Message>,

	/// The `ZonePosition`s of the `Tile`s which have been updated and might
	/// need to be redrawn.
	pub updated_tiles: Vec<ZonePosition>,
}

impl Event {
	pub fn sent_by_entity(mut self, entity_id: EntityId) -> Self {
		self.event_sender = Some(entity_id);

		self
	}

	pub fn with_message(
		mut self,
		text: String,
		receiver: EntityId,
		severity_for_receiver: Severity,
	) -> Self {
		self.message = Some(Message {
			text,
			receiver: Some(receiver),
			severity_for_receiver,
		});

		self
	}

	pub fn with_global_message(mut self, text: String, severity_for_receiver: Severity) -> Self {
		self.message = Some(Message {
			text,
			receiver: None,
			severity_for_receiver,
		});

		self
	}

	pub fn with_updated_tiles(mut self, updated_tiles: Vec<ZonePosition>) -> Self {
		self.updated_tiles = updated_tiles;

		self
	}
}
