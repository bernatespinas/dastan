#[derive(Debug)]
pub enum Severity {
	Good,
	Neutral,
	Bad,
}
