use std::cmp::Ordering;

use crate::list_enum::ListEnum;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum Size {
	Tiny,
	Small,
	Medium,
	Big,
	Huge,
}

impl Size {
	fn num_value(&self) -> u8 {
		match self {
			Size::Tiny => 0,
			Size::Small => 1,
			Size::Medium => 2,
			Size::Big => 3,
			Size::Huge => 4,
		}
	}
}

impl std::fmt::Display for Size {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{self:?}")
	}
}

impl PartialOrd for Size {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		Some(self.num_value().cmp(&other.num_value()))
	}
}

impl ListEnum for Size {
	fn list() -> Vec<Size> {
		vec![Size::Tiny, Size::Small, Size::Medium, Size::Big, Size::Huge]
	}
}
