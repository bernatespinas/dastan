use std::collections::HashMap;

use crate::ecs::prelude::NonIdentify;
use crate::terrain::TerrainType;
use crate::time::Season;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Biome {
	name: String,

	// fauna: SeasonChance,
	pub flora: SeasonChance,
	pub terrain: TerrainType,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SeasonChance {
	seasons: HashMap<Season, HashMap<(u8, u8), String>>,
}

impl Default for SeasonChance {
	fn default() -> SeasonChance {
		let mut seasons = HashMap::new();
		seasons.insert(Season::Winter, HashMap::new());
		seasons.insert(Season::Spring, HashMap::new());
		seasons.insert(Season::Summer, HashMap::new());
		seasons.insert(Season::Autumn, HashMap::new());

		SeasonChance { seasons }
	}
}

impl SeasonChance {
	pub fn get_species(&self, season: Season, chance_num: u8) -> Option<&String> {
		if let Some(season_chances) = self.seasons.get(&season) {
			for (chance, what) in season_chances {
				if chance.0 <= chance_num && chance_num <= chance.1 {
					return Some(what);
				}
			}
		}

		None
	}
}

impl NonIdentify for Biome {}
