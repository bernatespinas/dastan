use crate::{ecs::prelude::EntityId, map::MapZonePos};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum PlayerMapChange {
	TeleportToPosition(MapZonePos),
	TeleportToEntity(EntityId),
	ControlCreature(EntityId),
	StopControllingEntity,
	PickAdjacentPosition(EntityId),
}
