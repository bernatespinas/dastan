use std::collections::VecDeque;

use uuid::Uuid;

use crate::ecs::prelude::{Creature, CreatureWithComponents, EntityId, Identify};
use crate::ecs_components::TargetComponent;
use crate::file_utils;
use crate::generator::GeneratorBundle;
use crate::map::{EntityMapTracker, Map, MapManager, MapZonePos, ZonePosition};
use crate::message_log::MessageLog;
use crate::save_manager;

mod player_map_change;
pub use player_map_change::PlayerMapChange;

mod generate_universe;
pub use generate_universe::{generate_universe, UniverseBuilderData};

pub mod player_info;

#[derive(Serialize, Deserialize)]
pub struct Game {
	name: String,

	/// Whether that Game hasn't been played yet for the first time.
	pub new_game: bool,

	#[serde(skip)]
	pub map_manager: MapManager,

	/// The ID of the Map file the player is currently in.
	pub current_map_id: Uuid,

	/// The ID of the player.
	pub player_id: EntityId,

	/// The `EntityId`s of previous `Entity`-s which represented the player.
	pub previous_player_ids: VecDeque<EntityId>,

	pub player_map_change: Option<PlayerMapChange>,

	#[serde(skip)]
	pub message_log: MessageLog,

	// I generate everything (just Items for now) when loading any Game... I
	// still don't know if I should let each game keep the generators it had
	// when it was created. TODO
	// Hmm, I guess I should always use the current generators. If someone
	// wants to keep playing a save created with an older version of the
	// game, the best idea would be to keep the older version for that!
	#[serde(skip)]
	pub gen_bundle: GeneratorBundle,

	entity_map_tracker: EntityMapTracker,
}

impl Game {
	pub fn load(name: &str) -> Result<Game, String> {
		let game_file = file_utils::read(save_manager::universe_file(name), "universe")?;

		let mut game: Game = file_utils::deserialize(&game_file, "game (version mismatch?)")?;

		game.map_manager
			.load_map(&game.name, &game.current_map_id)?;

		let player_zone_id = game.map().entity_zone_pos(&game.player_id).unwrap().zone_id;
		game.map_mut().load_initial_zones(&player_zone_id)?;

		Ok(game)
	}

	pub fn name(&self) -> &str {
		&self.name
	}

	pub fn save_universe(&self) -> Result<(), String> {
		file_utils::serialize_and_write(self, save_manager::universe_file(&self.name), "universe")?;

		// Save all the Zones that have been loaded, which are skipped
		// by #[serde(skip)] to keep them in separate files.
		// The currently loadded Zones are in Map, while the Zones that
		// have been unloaded are in .new files.
		self.map_manager.save_maps()
	}

	pub fn map(&self) -> &Map {
		self.map_manager.map(&self.current_map_id)
	}

	pub fn map_mut(&mut self) -> &mut Map {
		self.map_manager.map_mut(&self.current_map_id)
	}

	pub fn get_entity_map(&self, entity_id: &EntityId) -> Option<&Uuid> {
		self.entity_map_tracker.get(entity_id)
	}

	pub fn set_entity_map(&mut self, entity_id: &EntityId, map_id: Uuid) {
		self.entity_map_tracker.set(*entity_id, map_id);
	}

	pub fn change_map(&mut self, map_zone_pos: &MapZonePos) -> Result<(), String> {
		self.map_manager.load_map_and_required_zones(
			&self.name,
			&map_zone_pos.map_id,
			&map_zone_pos.zone_pos.zone_id,
		)?;

		self.current_map_id = map_zone_pos.map_id;

		Ok(())
	}

	pub fn player_target_id(&self) -> Option<&EntityId> {
		self.map()
			.entity_component::<TargetComponent>(self.player().id())
			.map(|tc| &tc.entity_id)
	}

	pub fn player_target(&self) -> Option<&Creature> {
		self.player_target_id()
			.map(|target_id| self.map().get_by_id(target_id))
	}

	pub fn player(&self) -> &Creature {
		self.map().get_by_id::<Creature>(&self.player_id)
	}

	pub fn player_take(&mut self) -> CreatureWithComponents {
		let player_zone_pos = self
			.map()
			.entity_zone_pos(&self.player_id)
			.copied()
			.unwrap();

		self.map_mut().take_by_zone_pos(&player_zone_pos)
	}

	pub fn player_zone_pos(&self) -> &ZonePosition {
		self.map().entity_zone_pos(&self.player_id).unwrap()
	}

	pub fn is_current_map(&self, map: &Map) -> bool {
		self.current_map_id == *map.id()
	}
}
