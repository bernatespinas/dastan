use std::collections::VecDeque;

use uuid::Uuid;

use crate::{
	ecs::prelude::{CreatureWithComponents, EntityId, Identify},
	generator::GeneratorBundle,
	map::{EntityMapTracker, Map, MapInfo, MapManager, ZonePosition},
	map_generator::MapGenerator,
	message_log::MessageLog,
};

use super::{player_info::PlayerInfo, Game};

// TODO Rename it to something different.
#[derive(Default)]
pub struct UniverseBuilderData {
	/// The ID of the `Map` in which the player will start the game.
	pub player_map_id: Option<Uuid>,

	/// The `ZonePosition` of the `Map` in which the player will start the game.
	pub player_zone_pos: Option<ZonePosition>,

	/// Tracks `Entity`s across `Map`s.
	pub entity_map_tracker: EntityMapTracker,
}

/// Generates and returns a universe.
pub fn generate_universe(
	universe_info: &MapInfo,
	player_info: &PlayerInfo,
	universe_builder: &impl MapGenerator,
	gen_bundle: &GeneratorBundle,
) -> Result<Game, String> {
	let mut data = UniverseBuilderData::default();

	let mut map = universe_builder.generate(universe_info, gen_bundle, &mut data)?;

	let player_zone_pos = data.player_zone_pos.expect("Missing player_zone_pos");

	let player_id = generate_and_place_player(player_info, &player_zone_pos, gen_bundle, &mut map)?;

	map.save()?;

	let game = Game {
		name: universe_info.universe_name.clone(),
		new_game: true,

		map_manager: MapManager::default(),
		current_map_id: data.player_map_id.expect("Missing player_map_id"),
		player_id,

		previous_player_ids: VecDeque::new(),

		player_map_change: None,

		message_log: MessageLog::default(),

		gen_bundle: GeneratorBundle::load(),

		entity_map_tracker: data.entity_map_tracker,
	};

	Ok(game)
}

/// Generates the player, puts it in the `Map` and returns its `EntityId`.
fn generate_and_place_player(
	player_info: &PlayerInfo,
	player_zone_pos: &ZonePosition,
	gen_bundle: &GeneratorBundle,
	map: &mut Map,
) -> Result<EntityId, String> {
	let mut player: CreatureWithComponents = gen_bundle.generate_with_random_id("elf.ron");
	player.entity.rename(player_info.name.clone());

	let player_id = *player.id();

	map.load_zone(&player_zone_pos.zone_id)?;
	map.put(player_zone_pos, player);

	Ok(player_id)
}
