use crate::race::Race;

pub struct PlayerInfo {
	pub name: String,
	pub race: Race,
}
