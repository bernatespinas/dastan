use std::collections::HashSet;

use crate::ecs::prelude::{Entity, Item, ItemWithComponents};
use crate::size::Size;
use crate::weight::Weight;

use super::EcsComponent;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Inventory {
	slots: usize,
	items: Vec<ItemWithComponents>,
}

impl EcsComponent for Inventory {}

impl Inventory {
	pub fn new(slots: usize, items: Vec<ItemWithComponents>) -> Inventory {
		assert!(items.len() <= slots, "slots > items.len()");

		Inventory { slots, items }
	}

	pub fn with_slots(slots: usize) -> Inventory {
		Inventory {
			slots,
			items: Vec::new(),
		}
	}

	pub fn items(&self) -> &Vec<ItemWithComponents> {
		&self.items
	}

	pub fn is_full(&self) -> bool {
		self.items.len() >= self.slots
	}

	pub fn has_space_left(&self, amount: usize) -> bool {
		self.items.len() + amount <= self.slots
	}

	pub fn add_slots(&mut self, amount: usize) {
		self.slots += amount;
	}

	pub fn remove_slots(&mut self, amount: usize) {
		if self.slots > self.items.len() {
			self.slots -= amount;
		} else {
			// TODO Return `Result<(), String>`.
			panic!("Tried to remove too many slots from an Inventory.");
		}
	}

	pub fn add_item(&mut self, item: ItemWithComponents) {
		self.items.push(item);
	}

	pub fn remove_item(&mut self, index: usize) -> ItemWithComponents {
		self.items.remove(index)
	}

	pub fn can_pick_up(&self, item: &Item) -> Result<(), String> {
		if self.is_full() {
			Err("The inventory is full!".to_string())
		} else if item.size() > Size::Small {
			Err("The item is too big!".to_string())
		} else if item.weight() > Weight::Light {
			Err("The item is too heavy!".to_string())
		} else {
			Ok(())
		}
	}

	// TODO: Hmm, I'd like to check here if there's space left for the item. But if there isn't and I return the item, I'd have to put it back in the map (because I'd have had extracted it from there), which is a bit weird.
	// TODO: Is pick_up okay? Which name to choose?
	// TODO: Should I just push the thing? Making the check manually before.
	pub fn pick_up(&mut self, item: ItemWithComponents) -> Result<(), ItemWithComponents> {
		if self.items.len() >= self.slots {
			Err(item)
		} else {
			self.items.push(item);

			Ok(())
		}
	}

	// TODO Is this working as intended? `indexes` is randomly sorted, so maybe I'm not removing items in the order I should...
	pub fn remove_multiple(&mut self, indexes: &HashSet<usize>) -> Vec<ItemWithComponents> {
		// Since HashSet doesn't/can't have .iter().rev(), I guess I'll have to use a Vec...
		let indexes_vec: Vec<&usize> = Vec::from_iter(indexes.iter());

		// I reverse the iterator because I have to remove Items from the Vec<Item> starting from the end in order to preserve their..., ¿order? The way they are sorted.
		indexes_vec
			.iter()
			.rev()
			.map(|i| self.remove_item(**i))
			.collect()
	}

	/// Checks if/that all the items' names ___ are present in self.items. Returns Ok with a Vec containing the indexes of the items that are present ___; Err otherwise. Compares by name.
	// If __ can be crafted, returns Ok(Vec<usize>) with the indexes of the items that will get removed.
	// TODO Changed from &Vec<&str> to &Vec<String> because Recipe.input is now Vec<String>. Use Cow or something?
	// TODO Remove this - it shouldn't be here.
	// TODO Return an `Option` and rename the method? `Err` is `()`, and clippy doesn't like that.
	pub fn check_all_present(&self, names: &[String]) -> Result<HashSet<usize>, ()> {
		let mut items_indexes: HashSet<usize> = HashSet::with_capacity(names.len());

		for name in names {
			let mut found = false;
			'search: for (index, item) in self.items.iter().enumerate() {
				if name == item.entity.name() {
					if items_indexes.contains(&index) {
						continue 'search;
					} else {
						items_indexes.insert(index);
						// continue 'recipe;
						found = true;
					}
				}
			}
			// *sigh* This /name/ is not in inventory! -_-
			if !found {
				return Err(());
			}
		}

		Ok(items_indexes)
	}
}
