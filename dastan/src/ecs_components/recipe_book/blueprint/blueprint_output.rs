use crate::terrain::TerrainType;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum BlueprintOutput {
	Terrain(TerrainType),
	Prop(String),
}

impl std::fmt::Display for BlueprintOutput {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			BlueprintOutput::Prop(key) => {
				write!(f, "{key}")
			}
			BlueprintOutput::Terrain(key) => {
				write!(f, "{key}")
			}
		}
	}
}

impl From<String> for BlueprintOutput {
	fn from(string: String) -> Self {
		BlueprintOutput::Prop(string)
	}
}

impl From<TerrainType> for BlueprintOutput {
	fn from(terrain_type: TerrainType) -> Self {
		BlueprintOutput::Terrain(terrain_type)
	}
}
