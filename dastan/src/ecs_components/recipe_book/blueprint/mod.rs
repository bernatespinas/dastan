use crate::ecs::prelude::NonIdentify;

use super::Profession;

mod blueprint_output;
pub use blueprint_output::BlueprintOutput;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Blueprint {
	pub profession: Profession,
	pub input: Vec<String>,
	pub output: BlueprintOutput,
}

impl NonIdentify for Blueprint {}
