use crate::list_enum::ListEnum;

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone, Serialize, Deserialize)]
pub enum Profession {
	Butchering,
	Fishing,
	Herbalism,
	Farming,
	Mining,

	Cooking,
	Alchemy,
	FirstAid,

	Tailoring,
	Leatherworking,
	Metalworking,

	Carpentry,
	Masonry,
	// Flooring,
	// Walling,
}

impl std::fmt::Display for Profession {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
		write!(f, "{self:?}")
	}
}

impl ListEnum for Profession {
	fn list() -> Vec<Profession> {
		vec![
			Profession::Butchering,
			Profession::Fishing,
			Profession::Herbalism,
			Profession::Farming,
			Profession::Mining,
			Profession::Cooking,
			Profession::Alchemy,
			Profession::FirstAid,
			Profession::Tailoring,
			Profession::Leatherworking,
			Profession::Metalworking,
			Profession::Carpentry,
			Profession::Masonry,
			// Profession::Flooring,
			// Profession::Walling,
		]
	}
}

impl Profession {
	/// Lists crafting professions, that is, ___.
	pub fn list_crafting() -> Vec<Profession> {
		vec![
			Profession::Butchering,
			Profession::Fishing,
			Profession::Herbalism,
			Profession::Farming,
			Profession::Mining,
			Profession::Cooking,
			Profession::Alchemy,
			Profession::FirstAid,
			Profession::Tailoring,
			Profession::Leatherworking,
			Profession::Metalworking,
		]
	}

	pub fn list_construction() -> Vec<Profession> {
		vec![
			Profession::Carpentry,
			Profession::Masonry,
			// Profession::Flooring,
			// Profession::Walling,
		]
	}
}
