use std::collections::HashMap;

use crate::list_enum::ListEnum;

mod profession;
pub use profession::Profession;

mod recipe;
pub use recipe::Recipe;

mod blueprint;
pub use blueprint::{Blueprint, BlueprintOutput};

use super::EcsComponent;

// const RECIPE: char = 'ý';
// const BLUEPRINT: char = 'y';

// TODO HashSet<Recipe> instead of a Vec?
// + No duplicates, fast access.
// - I have to see if I'd need to collect() it into a Vec for Scrollable or other things to work. If I have to, maybe it's more worth ¿it? / worthier to add a HashSet<&'a str> containing the names of every added recipe, to easily prevent duplicates.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RecipeBook {
	recipes: HashMap<Profession, Vec<Recipe>>,
	blueprints: HashMap<Profession, Vec<Blueprint>>,
}

impl EcsComponent for RecipeBook {}

impl Default for RecipeBook {
	fn default() -> RecipeBook {
		let mut recipes: HashMap<Profession, Vec<Recipe>> = HashMap::new();
		let mut blueprints = HashMap::new();

		for profession in Profession::list() {
			recipes.insert(profession, Vec::new());
			blueprints.insert(profession, Vec::new());
		}

		RecipeBook {
			recipes,
			blueprints,
		}
	}
}

// TODO Trait with methods that apply for Recipe and Blueprint.
impl RecipeBook {
	pub fn with_recipes(mut self, recipes: Vec<Recipe>) -> RecipeBook {
		for recipe in recipes {
			self.recipes
				.get_mut(&recipe.profession)
				.unwrap()
				.push(recipe);
		}

		self
	}

	pub fn with_blueprints(mut self, blueprints: Vec<Blueprint>) -> RecipeBook {
		for blueprint in blueprints {
			self.blueprints
				.get_mut(&blueprint.profession)
				.unwrap()
				.push(blueprint);
		}

		self
	}

	pub fn recipes_by_profession(&self, profession: Profession) -> &Vec<Recipe> {
		self.recipes.get(&profession).unwrap()
	}

	pub fn blueprints_by_profession(&self, profession: Profession) -> &Vec<Blueprint> {
		self.blueprints.get(&profession).unwrap()
	}

	pub fn recipe_remove(&mut self, profession: Profession, i: usize) -> Recipe {
		self.recipes.get_mut(&profession).unwrap().remove(i)
	}

	pub fn recipe_already_known(&self, recipe: &Recipe) -> bool {
		self.recipes
			.get(&recipe.profession)
			.unwrap()
			.iter()
			.any(|r| r.name == recipe.name)
	}

	pub fn recipe_add(&mut self, recipe: Recipe) -> Result<(), String> {
		if self.recipe_already_known(&recipe) {
			Err("This recipe is already known!".to_string())
		} else {
			self.recipes
				.get_mut(&recipe.profession)
				.unwrap()
				.push(recipe);
			Ok(())
		}
	}
}
