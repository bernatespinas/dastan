use crate::ecs::prelude::NonIdentify;

use super::Profession;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Recipe {
	pub name: String,

	/// The names of the ingredients.
	pub input: Vec<String>,

	/// The name of the output.
	pub output: String,

	pub profession: Profession,
	// workshop: Option<Workshop>,
}

impl NonIdentify for Recipe {}
