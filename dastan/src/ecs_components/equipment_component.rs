use serde::{de::DeserializeOwned, Serialize};

use super::{body::HumanoidSlot, EcsComponent};

pub type HumanoidEquipment = EquipmentComponent<HumanoidSlot>;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EquipmentComponent<Slot> {
	pub vitality: u16,
	pub strength: u16,
	pub defense: u16,

	pub slot: Slot,
}

impl<Slot: Clone + Serialize + DeserializeOwned> EcsComponent for EquipmentComponent<Slot> {}
