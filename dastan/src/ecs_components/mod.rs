mod recipe_book;
pub use recipe_book::{Blueprint, BlueprintOutput, Profession, Recipe, RecipeBook};

mod inventory;
pub use inventory::Inventory;

mod computer_component;
pub use computer_component::ComputerComponent;

mod teleport_component;
pub use teleport_component::TeleportComponent;

pub mod body;

mod passage_component;
pub use passage_component::PassageComponent;

mod target_component;
pub use target_component::TargetComponent;

mod food_component;
pub use food_component::{Effect, FoodComponent};

mod equipment_component;
pub use equipment_component::{EquipmentComponent, HumanoidEquipment};

mod corpse_component;
pub use corpse_component::CorpseComponent;

/// A marker trait which must be implemented by any struct
/// which can be a component in `dastan`'s own ECS implementation.
pub trait EcsComponent: Clone + serde::Serialize + serde::de::DeserializeOwned {}
