use std::collections::HashMap;
use std::hash::Hash;

use crate::ecs::prelude::ItemWithComponents;

mod humanoid_slot;
pub use humanoid_slot::HumanoidSlot;

use super::EcsComponent;

pub type HumanoidBody = Body<HumanoidSlot>;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Body<Slot: Eq + Hash> {
	pieces: HashMap<Slot, Option<ItemWithComponents>>,
}

impl<Slot: Clone + Eq + Hash + serde::Serialize + serde::de::DeserializeOwned> EcsComponent
	for Body<Slot>
{
}

impl<Slot: Copy + Eq + Hash> Body<Slot> {
	pub fn piece(&self, slot: &Slot) -> Option<&ItemWithComponents> {
		self.pieces
			.get(slot)
			.unwrap_or(&None::<ItemWithComponents>)
			.as_ref()
	}

	pub fn equip(&mut self, slot: &Slot, piece: ItemWithComponents) -> Option<ItemWithComponents> {
		self.pieces.get_mut(slot).unwrap().replace(piece)
	}

	pub fn dequip(&mut self, slot: &Slot) -> Option<ItemWithComponents> {
		self.pieces.get_mut(slot).and_then(|piece| piece.take())
	}

	pub fn has_slot(&self, slot: &Slot) -> bool {
		self.pieces.contains_key(slot)
	}

	pub fn slot_status(&self, slot: &Slot) -> SlotStatus {
		if self.piece(slot).is_some() {
			SlotStatus::Used
		} else if self.has_slot(slot) {
			SlotStatus::Free
		} else {
			SlotStatus::Missing
		}
	}

	pub fn can_equip_in(&self, slot: &Slot) -> bool {
		matches!(self.slot_status(slot), SlotStatus::Free)
	}
}

pub trait BodyPiece {
	type Slot;

	fn slot(&self) -> Self::Slot;
}

pub enum SlotStatus {
	Free,
	Used,
	Missing,
}
