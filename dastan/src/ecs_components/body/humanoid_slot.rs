use std::fmt;

use crate::list_enum::ListEnum;

#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Serialize, Deserialize)]
pub enum HumanoidSlot {
	Head,
	Torso,
	Hands,
	Legs,
	Feet,

	Fingers,
	Neck,
	Wrists,
	Waist,

	Weapon,
}

impl fmt::Display for HumanoidSlot {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), fmt::Error> {
		write!(f, "{self:?}")
	}
}

impl ListEnum for HumanoidSlot {
	fn list() -> Vec<HumanoidSlot> {
		vec![
			HumanoidSlot::Head,
			HumanoidSlot::Torso,
			HumanoidSlot::Hands,
			HumanoidSlot::Legs,
			HumanoidSlot::Feet,
			HumanoidSlot::Fingers,
			HumanoidSlot::Neck,
			HumanoidSlot::Wrists,
			HumanoidSlot::Waist,
			HumanoidSlot::Weapon,
		]
	}
}
