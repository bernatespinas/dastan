use crate::ecs::prelude::EntityId;

use super::EcsComponent;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TargetComponent {
	pub entity_id: EntityId,
}

impl EcsComponent for TargetComponent {}
