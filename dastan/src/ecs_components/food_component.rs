use super::EcsComponent;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FoodComponent {
	pub effect: Effect,
}

impl EcsComponent for FoodComponent {}

#[derive(Debug, PartialEq, Copy, Clone, Serialize, Deserialize)]
pub enum Effect {
	Heal(u16),
	// Feed(u16),
	// Poison(u16),
	// Kill,
}

impl std::fmt::Display for Effect {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
		write!(f, "{self:?}")
	}
}
