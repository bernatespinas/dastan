use uuid::Uuid;

use crate::ecs::prelude::EntityId;

use super::EcsComponent;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum TeleportComponent {
	AtMap(Uuid),
	AtEntity(EntityId),
	NextToEntity(EntityId),
}

impl EcsComponent for TeleportComponent {}
