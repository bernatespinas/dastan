use super::EcsComponent;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PassageComponent {
	pub blocks_passage: bool,

	pub opened_icon: char,
	pub closed_icon: char,
}

impl EcsComponent for PassageComponent {}
