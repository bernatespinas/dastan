use crate::ecs::prelude::CreatureWithComponents;

use super::EcsComponent;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct CorpseComponent {
	pub creature: Box<CreatureWithComponents>,
}

impl EcsComponent for CorpseComponent {}
