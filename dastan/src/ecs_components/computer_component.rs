use std::collections::HashSet;

use crate::ecs::prelude::EntityId;

use super::EcsComponent;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ComputerComponent {
	// internet: bool,
	// cd_rom: Option<CdRom>,
	pub linked_tracked_entities: HashSet<EntityId>,
}

impl EcsComponent for ComputerComponent {}
