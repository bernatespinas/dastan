#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum WeekDay {
	Monday,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Saturday,
	Sunday,
}

impl WeekDay {
	pub fn next(week_day: &WeekDay) -> WeekDay {
		match week_day {
			WeekDay::Monday => WeekDay::Tuesday,
			WeekDay::Tuesday => WeekDay::Wednesday,
			WeekDay::Wednesday => WeekDay::Thursday,
			WeekDay::Thursday => WeekDay::Friday,
			WeekDay::Friday => WeekDay::Saturday,
			WeekDay::Saturday => WeekDay::Sunday,
			WeekDay::Sunday => WeekDay::Monday,
		}
	}
}
