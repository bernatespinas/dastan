use super::{Month, PartOfDay};

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Season {
	Winter,
	Spring,
	Summer,
	Autumn,
}

impl Season {
	pub fn next(season: Season) -> Season {
		match season {
			Season::Winter => Season::Spring,
			Season::Spring => Season::Summer,
			Season::Summer => Season::Autumn,
			Season::Autumn => Season::Winter,
		}
	}

	pub fn from(month: Month) -> Season {
		match month {
			Month::December => Season::Winter,
			Month::January => Season::Winter,
			Month::February => Season::Winter,
			Month::March => Season::Spring,
			Month::April => Season::Spring,
			Month::May => Season::Spring,
			Month::June => Season::Summer,
			Month::July => Season::Summer,
			Month::August => Season::Summer,
			Month::September => Season::Autumn,
			Month::October => Season::Autumn,
			Month::November => Season::Autumn,
		}
	}

	pub fn part_of_day(self, hour: u8) -> PartOfDay {
		match self {
			Season::Winter => {
				if (5..=11).contains(&hour) {
					PartOfDay::Morning
				} else if (12..=16).contains(&hour) {
					PartOfDay::Afternoon
				} else if (17..=20).contains(&hour) {
					PartOfDay::Evening
				} else {
					PartOfDay::Night
				}
			}
			Season::Spring => {
				if (5..=11).contains(&hour) {
					PartOfDay::Morning
				} else if (12..=16).contains(&hour) {
					PartOfDay::Afternoon
				} else if (17..=20).contains(&hour) {
					PartOfDay::Evening
				} else {
					PartOfDay::Night
				}
			}
			Season::Summer => {
				if (5..=11).contains(&hour) {
					PartOfDay::Morning
				} else if (12..=16).contains(&hour) {
					PartOfDay::Afternoon
				} else if (17..=20).contains(&hour) {
					PartOfDay::Evening
				} else {
					PartOfDay::Night
				}
			}
			Season::Autumn => {
				if (5..=11).contains(&hour) {
					PartOfDay::Morning
				} else if (12..=16).contains(&hour) {
					PartOfDay::Afternoon
				} else if (17..=20).contains(&hour) {
					PartOfDay::Evening
				} else {
					PartOfDay::Night
				}
			}
		}
	}
}
