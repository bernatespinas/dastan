use super::WeekDay;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Day {
	week_day: WeekDay,
	number: u8,
}

impl Default for Day {
	fn default() -> Day {
		Day {
			week_day: WeekDay::Monday,
			number: 1,
		}
	}
}

impl Day {
	pub fn week_day(&self) -> WeekDay {
		self.week_day
	}

	pub fn number(&self) -> u8 {
		self.number
	}

	pub fn next(&mut self) {
		self.week_day = WeekDay::next(&self.week_day);

		self.number = if self.number == 28 {
			1
		} else {
			self.number + 1
		};
	}
}
