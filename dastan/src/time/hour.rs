#[derive(Debug, Default, Copy, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Hour {
	hour: u8,
	minute: u8,
}

impl std::fmt::Display for Hour {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
		if self.minute < 10 {
			write!(f, "{}:0{}", self.hour, self.minute)
		} else {
			write!(f, "{}:{}", self.hour, self.minute)
		}
	}
}

impl Hour {
	pub fn hour(&self) -> u8 {
		self.hour
	}

	pub fn minute(&self) -> u8 {
		self.minute
	}

	pub fn tick_minute(&mut self) {
		if self.minute < 59 {
			self.minute += 1;
		} else {
			self.minute = 0;
			self.tick_hour();
		}
	}

	pub fn tick_hour(&mut self) {
		if self.hour < 23 {
			self.hour += 1;
		} else {
			self.hour = 0;
		}
	}
}
