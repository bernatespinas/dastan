use super::Season;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Month {
	January,
	February,
	March,
	April,
	May,
	June,
	July,
	August,
	September,
	October,
	November,
	December,
}

impl Month {
	pub fn next(month: Month) -> Month {
		match month {
			Month::January => Month::February,
			Month::February => Month::March,
			Month::March => Month::April,
			Month::April => Month::May,
			Month::May => Month::June,
			Month::June => Month::July,
			Month::July => Month::August,
			Month::August => Month::September,
			Month::September => Month::October,
			Month::October => Month::November,
			Month::November => Month::December,
			Month::December => Month::January,
		}
	}

	pub fn get_season(self) -> Season {
		match self {
			Month::December | Month::January | Month::February => Season::Winter,

			Month::March | Month::April | Month::May => Season::Spring,

			Month::June | Month::July | Month::August => Season::Summer,

			Month::September | Month::October | Month::November => Season::Autumn,
		}
	}
}
