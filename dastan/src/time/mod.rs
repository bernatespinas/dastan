mod hour;
pub use hour::Hour;

mod part_of_day;
pub use part_of_day::PartOfDay;

mod day;
pub use day::Day;

mod week_day;
pub use week_day::WeekDay;

mod month;
pub use month::Month;

mod season;
pub use season::Season;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Time {
	pub hour: Hour,
	pub day: Day,
	pub month: Month,
	pub year: u16,
}

impl Default for Time {
	fn default() -> Time {
		Time {
			hour: Hour::default(),
			day: Day::default(),
			month: Month::January,
			year: 0,
		}
	}
}

impl Time {
	// TODO Don't return a `bool` - create a new method to obtain it: `is_at_beginning_of_hour`?
	/// Tickes a minute, which might tick an hour, which might tick a day. ___
	pub fn tick_minute(&mut self) -> bool {
		self.hour.tick_minute();

		// If minute == 0, it's been an hour!
		if self.hour.minute() == 0 {
			// If hour == 0, it's also been a day!
			if self.hour.hour() == 0 {
				self.next_day();
			}

			return true;
		}

		false
	}

	fn next_day(&mut self) {
		self.day.next();
		if self.day.number() == 1 {
			self.next_month();
		}
	}

	fn next_month(&mut self) {
		self.month = Month::next(self.month);
		if let Month::January = self.month {
			self.next_year();
		}
	}

	fn next_year(&mut self) {
		self.year += 1;
	}

	fn season(&self) -> Season {
		Season::from(self.month)
	}

	pub fn part_of_day(&self) -> PartOfDay {
		self.season().part_of_day(self.hour.hour())
	}

	pub fn day_number(&self) -> u8 {
		self.day.number()
	}
}
