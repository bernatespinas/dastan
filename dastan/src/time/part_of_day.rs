#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum PartOfDay {
	Morning,
	Afternoon,
	Evening,
	Night,
}
