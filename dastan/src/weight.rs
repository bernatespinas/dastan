use std::cmp::Ordering;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub enum Weight {
	Light,
	Medium,
	Heavy,
	SuperHeavy,
}

impl Weight {
	fn num_value(&self) -> u8 {
		match self {
			Weight::Light => 0,
			Weight::Medium => 1,
			Weight::Heavy => 2,
			Weight::SuperHeavy => 3,
		}
	}
}

impl std::fmt::Display for Weight {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{self:?}")
	}
}

impl PartialOrd for Weight {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		Some(self.num_value().cmp(&other.num_value()))
	}
}
