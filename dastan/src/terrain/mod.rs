mod terrain_type;
pub use terrain_type::{Depth, Material, TerrainType};

mod terrain_gen;
pub use terrain_gen::TerrainGenerator;

pub struct Terrain {
	name: String,
	sprite: &'static str,
}

impl Terrain {
	pub fn name(&self) -> &String {
		&self.name
	}

	pub fn sprite(&self) -> &str {
		self.sprite
	}
}
