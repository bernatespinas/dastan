use std::collections::HashMap;

use super::{Depth, Material, Terrain, TerrainType};

pub struct TerrainGenerator {
	terrains: HashMap<TerrainType, Terrain>,
}

impl Default for TerrainGenerator {
	fn default() -> Self {
		let mut terrains = HashMap::new();

		terrains.insert(
			TerrainType::Floor(Material::Dirt),
			Terrain {
				name: "Dirt floor".to_string(),
				sprite: "FLOOR_DIRT",
			},
		);

		terrains.insert(
			TerrainType::Wall(Material::Dirt),
			Terrain {
				name: "Dirt wall".to_string(),
				sprite: "WALL_DIRT",
			},
		);

		terrains.insert(
			TerrainType::Floor(Material::Grass),
			Terrain {
				name: "Grass floor".to_string(),
				sprite: "FLOOR_GRASS",
			},
		);

		terrains.insert(
			TerrainType::Wall(Material::Grass),
			Terrain {
				name: "Bush wall".to_string(),
				sprite: "WALL_BUSH",
			},
		);

		terrains.insert(
			TerrainType::Floor(Material::Ice),
			Terrain {
				name: "Ice floor".to_string(),
				sprite: "FLOOR_ICE",
			},
		);

		terrains.insert(
			TerrainType::Wall(Material::Ice),
			Terrain {
				name: "Ice wall".to_string(),
				sprite: "WALL_ICE",
			},
		);

		terrains.insert(
			TerrainType::Floor(Material::Stone),
			Terrain {
				name: "Stone floor".to_string(),
				sprite: "FLOOR_STONE",
			},
		);

		terrains.insert(
			TerrainType::Wall(Material::Stone),
			Terrain {
				name: "Stone wall".to_string(),
				sprite: "WALL_STONE",
			},
		);

		terrains.insert(
			TerrainType::Floor(Material::Wood),
			Terrain {
				name: "Wood floor".to_string(),
				sprite: "FLOOR_WOOD",
			},
		);

		terrains.insert(
			TerrainType::Wall(Material::Wood),
			Terrain {
				name: "Wood wall".to_string(),
				sprite: "WALL_WOOD",
			},
		);

		terrains.insert(
			TerrainType::Void,
			Terrain {
				name: "Void".to_string(),
				sprite: "VOID",
			},
		);

		terrains.insert(
			TerrainType::Planet,
			Terrain {
				name: "Planet tile".to_string(),
				sprite: "PLANET_TILE",
			},
		);

		terrains.insert(
			TerrainType::SaltWater(Depth::Shallow),
			Terrain {
				name: "Shallow salt water".to_string(),
				sprite: "WATER_SALT_SHALLOW",
			},
		);

		terrains.insert(
			TerrainType::SaltWater(Depth::Deep),
			Terrain {
				name: "Deep salt water".to_string(),
				sprite: "WATER_SALT_DEEP",
			},
		);

		terrains.insert(
			TerrainType::FreshWater(Depth::Shallow),
			Terrain {
				name: "Shallow fresh water".to_string(),
				sprite: "WATER_FRESH_SHALLOW",
			},
		);

		Self { terrains }
	}
}

impl TerrainGenerator {
	pub fn validate_terrain(&self, terrain_type: TerrainType) -> TerrainType {
		assert!(self.terrains.contains_key(&terrain_type));

		terrain_type
	}

	pub fn terrain(&self, terrain_type: &TerrainType) -> &Terrain {
		self.terrains
			.get(terrain_type)
			.as_ref()
			.unwrap_or_else(|| panic!("TerrainType {terrain_type:?} is not valid!"))
	}
}
