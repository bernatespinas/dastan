#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum TerrainType {
	Floor(Material),
	Wall(Material),
	Void,
	Planet,
	SaltWater(Depth),
	FreshWater(Depth),
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum Depth {
	Shallow,
	Deep,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum Material {
	Wood,
	Stone,
	Ice,
	Dirt,
	Grass,
}

impl std::fmt::Display for TerrainType {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			TerrainType::Floor(material) => write!(f, "{material:?} floor"),
			TerrainType::Wall(material) => write!(f, "{material:?} wall"),
			TerrainType::Void => write!(f, "Void"),
			TerrainType::Planet => write!(f, "Planet"),

			TerrainType::SaltWater(depth) => write!(f, "{depth:?} salt water"),
			TerrainType::FreshWater(depth) => write!(f, "{depth:?} fresh water"),
		}
	}
}

impl TerrainType {
	pub fn blocks_passage(&self) -> bool {
		self.is_wall()
	}

	pub fn is_floor(&self) -> bool {
		matches!(
			self,
			TerrainType::Floor(_) | TerrainType::Void | TerrainType::Planet
		)
	}

	pub fn is_wall(&self) -> bool {
		matches!(self, TerrainType::Wall(_))
	}

	pub fn is_liquid(&self) -> bool {
		matches!(self, TerrainType::SaltWater(_) | TerrainType::FreshWater(_))
	}
}
