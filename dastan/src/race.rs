use crate::list_enum::ListEnum;

#[derive(Debug, PartialEq, Copy, Clone, Serialize, Deserialize)]
pub enum Race {
	Human,
	Elf,
	Dwarf,
	Gnome,
	Undead,
	Orc,
	Troll,
	Goblin,
}

impl std::fmt::Display for Race {
	fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
		write!(f, "{self:?}")
	}
}

impl ListEnum for Race {
	fn list() -> Vec<Race> {
		vec![
			Race::Human,
			Race::Elf,
			Race::Dwarf,
			Race::Gnome,
			Race::Undead,
			Race::Orc,
			Race::Troll,
			Race::Goblin,
		]
	}
}
