use uuid::Uuid;

pub trait Identify {
	fn id(&self) -> &Uuid;
}
