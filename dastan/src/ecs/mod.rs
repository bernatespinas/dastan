use std::collections::HashMap;

use dastan_macro_rules::create_entity_components_structs;

use crate::ecs_components::{
	body::HumanoidBody, ComputerComponent, CorpseComponent, EcsComponent, FoodComponent,
	HumanoidEquipment, Inventory, PassageComponent, RecipeBook, TargetComponent, TeleportComponent,
};

mod entity;
use entity::EntityId;

mod entity_with_components;

mod identify;

mod identify_mut;

mod non_identify;

pub mod prelude {
	pub use super::entity::{
		generate_corpse, Creature, CreatureWithComponents, Entity, EntityId, Item,
		ItemWithComponents, Prop, PropWithComponents,
	};

	pub use super::entity_with_components::EntityWithComponents;

	pub use super::identify::Identify;

	pub(crate) use super::identify_mut::IdentifyMut;

	pub(crate) use super::non_identify::NonIdentify;

	pub use super::{
		EntityComponents, EntityComponentsMask, EntityComponentsRef, SpecializedEntityTracker,
		ZoneComponentManager, ZoneComponents,
	};
}

create_entity_components_structs!(
	inventory -> Inventory,
	recipe_book -> RecipeBook,
	computer -> ComputerComponent,
	passage -> PassageComponent,
	teleport -> TeleportComponent,
	// lock -> LockComponent,
	humanoid_body -> HumanoidBody,
	target -> TargetComponent,
	humanoid_equipment -> HumanoidEquipment,
	food -> FoodComponent,
	corpse -> CorpseComponent,
);

pub trait ZoneComponentManager<C: EcsComponent> {
	fn component(&self, entity_id: &EntityId) -> Option<&C>;

	fn component_mut(&mut self, entity_id: &EntityId) -> Option<&mut C>;

	fn add_component(&mut self, entity_id: EntityId, component: C);

	fn remove_component(&mut self, entity_id: &EntityId) -> Option<C>;
}
