/// Marker trait use to mark structs which don't implement `Identify`.
pub trait NonIdentify {}
