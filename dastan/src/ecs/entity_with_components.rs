use super::prelude::{Entity, EntityComponents, EntityId, Identify, IdentifyMut};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct EntityWithComponents<E: Entity> {
	pub entity: E,

	pub components: EntityComponents,
}

impl<E: Entity> Identify for EntityWithComponents<E> {
	fn id(&self) -> &uuid::Uuid {
		self.entity.id()
	}
}

impl<E: Entity> IdentifyMut for EntityWithComponents<E> {
	fn with_random_id(self) -> Self {
		Self {
			entity: self.entity.with_random_id(),
			components: self.components,
		}
	}

	fn with_id(self, id: EntityId) -> Self {
		Self {
			entity: self.entity.with_id(id),
			components: self.components,
		}
	}
}
