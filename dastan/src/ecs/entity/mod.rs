use uuid::Uuid;

use crate::{
	ecs::prelude::{Identify, IdentifyMut},
	size::Size,
	weight::Weight,
};

mod creature;
pub use creature::{Creature, CreatureWithComponents};

mod prop;
pub use prop::{Prop, PropWithComponents};

mod item;
pub use item::{generate_corpse, Item, ItemWithComponents};

pub type EntityId = Uuid;

pub trait Entity: Clone + Identify + IdentifyMut {
	fn name(&self) -> &String;

	fn sprite(&self) -> &String;

	fn size(&self) -> Size;

	fn weight(&self) -> Weight;

	fn change_sprite(&mut self, sprite: String);
}
