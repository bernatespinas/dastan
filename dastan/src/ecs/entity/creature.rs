use uuid::Uuid;

use crate::{ecs::prelude::*, size::Size, weight::Weight};

pub type CreatureWithComponents = EntityWithComponents<Creature>;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Creature {
	id: Option<EntityId>,

	name: String,
	sprite: String,

	size: Size,
	weight: Weight,

	current_vitality: u16,
	max_vitality: u16,
	strength: u16,
	defense: u16,
}

impl Entity for Creature {
	fn name(&self) -> &String {
		&self.name
	}

	fn sprite(&self) -> &String {
		&self.sprite
	}

	fn size(&self) -> Size {
		self.size
	}

	fn weight(&self) -> Weight {
		self.weight
	}

	fn change_sprite(&mut self, sprite: String) {
		self.sprite = sprite;
	}
}

impl Identify for Creature {
	fn id(&self) -> &EntityId {
		self.id
			.as_ref()
			.unwrap_or_else(|| panic!("Creature with name {} does not have an id.", self.name))
	}
}

impl IdentifyMut for Creature {
	fn with_random_id(mut self) -> Self {
		self.id = Some(Uuid::new_v4());

		self
	}

	fn with_id(mut self, id: EntityId) -> Self {
		self.id = Some(id);

		self
	}
}

impl Creature {
	pub fn rename(&mut self, name: String) {
		self.name = name;
	}

	pub fn current_vitality(&self) -> u16 {
		self.current_vitality
	}

	pub fn max_vitality(&self) -> u16 {
		self.max_vitality
	}

	pub fn strength(&self) -> u16 {
		self.strength
	}

	pub fn defense(&self) -> u16 {
		self.defense
	}

	pub fn is_alive(&self) -> bool {
		self.current_vitality > 0
	}

	/// Applies an amount of damage to the `Creature`.
	/// `damage` is the final amount of damage that will be dealt. This value
	/// will not be altered by the defense attribute's value.
	pub fn take_damage(&mut self, damage: u16) {
		if damage < self.current_vitality {
			self.current_vitality -= damage;
		} else {
			self.current_vitality = 0;
		}
	}

	/// Returns the amount of damage the current `Creature` will deal to
	/// another one.
	pub fn attack_damage(&self, enemy: &Self) -> u16 {
		if self.strength > enemy.defense {
			self.strength - enemy.defense
		} else {
			enemy.defense - self.strength
		}
	}

	pub fn heal(&mut self, heal_amount: u16) {
		self.current_vitality = self.max_vitality.min(self.current_vitality + heal_amount);
	}
}
