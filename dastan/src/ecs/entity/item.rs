use uuid::Uuid;

use crate::{
	ecs::prelude::{EntityComponents, EntityWithComponents, Identify, IdentifyMut},
	ecs_components::CorpseComponent,
	size::Size,
	weight::Weight,
};

use super::{CreatureWithComponents, Entity};

pub type ItemWithComponents = EntityWithComponents<Item>;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Item {
	id: Option<Uuid>,

	name: String,
	sprite: String,

	size: Size,
	weight: Weight,
}

impl Entity for Item {
	fn name(&self) -> &String {
		&self.name
	}

	fn sprite(&self) -> &String {
		&self.sprite
	}

	fn size(&self) -> Size {
		self.size
	}

	fn weight(&self) -> Weight {
		self.weight
	}

	fn change_sprite(&mut self, sprite: String) {
		self.sprite = sprite;
	}
}

impl Identify for Item {
	fn id(&self) -> &Uuid {
		self.id
			.as_ref()
			.unwrap_or_else(|| panic!("Item with name {} does not have an id.", self.name))
	}
}

impl IdentifyMut for Item {
	fn with_random_id(mut self) -> Self {
		self.id = Some(Uuid::new_v4());

		self
	}

	fn with_id(mut self, id: Uuid) -> Self {
		self.id = Some(id);

		self
	}
}

/// Creates and returns an `Item` `Entity` with a `CorpseComponent` which
/// contains the `CreatureWithComponents` which has died.
pub fn generate_corpse(creature: CreatureWithComponents) -> ItemWithComponents {
	ItemWithComponents {
		entity: Item {
			id: Some(Uuid::new_v4()),

			name: format!("{} corpse", creature.entity.name()),
			icon: creature.entity.icon(),
			color: Color::Red,

			size: creature.entity.size(),
			weight: creature.entity.weight(),
		},
		components: EntityComponents {
			inventory: None,
			recipe_book: None,
			computer: None,
			passage: None,
			teleport: None,
			humanoid_body: None,
			target: None,
			humanoid_equipment: None,
			food: None,
			corpse: Some(CorpseComponent {
				creature: Box::new(creature),
			}),
		},
	}
}
