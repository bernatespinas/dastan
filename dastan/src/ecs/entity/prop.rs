use uuid::Uuid;

use crate::ecs::prelude::*;

use super::{Entity, Size, Weight};

pub type PropWithComponents = EntityWithComponents<Prop>;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Prop {
	id: Option<EntityId>,

	name: String,
	sprite: String,

	size: Size,
	weight: Weight,
}

impl Entity for Prop {
	fn name(&self) -> &String {
		&self.name
	}

	fn sprite(&self) -> &String {
		&self.sprite
	}

	fn size(&self) -> Size {
		self.size
	}

	fn weight(&self) -> Weight {
		self.weight
	}

	fn change_sprite(&mut self, sprite: String) {
		self.sprite = sprite;
	}
}

impl Identify for Prop {
	fn id(&self) -> &EntityId {
		self.id
			.as_ref()
			.unwrap_or_else(|| panic!("Prop with name {} does not have an id.", self.name))
	}
}

impl IdentifyMut for Prop {
	fn with_random_id(mut self) -> Self {
		self.id = Some(Uuid::new_v4());

		self
	}

	fn with_id(mut self, id: EntityId) -> Self {
		self.id = Some(id);

		self
	}
}
