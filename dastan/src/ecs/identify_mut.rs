use uuid::Uuid;

use crate::ecs::prelude::Identify;

pub trait IdentifyMut: Identify {
	fn with_random_id(self) -> Self;
	fn with_id(self, id: Uuid) -> Self;
}
