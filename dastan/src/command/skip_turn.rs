use super::prelude::*;

pub struct SkipTurn {}

impl Command for SkipTurn {
	fn execute(&self, _game: &mut Game) -> Event {
		Event::default()
	}
}
