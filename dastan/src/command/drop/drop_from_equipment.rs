use super::{super::prelude::*, CANNOT_DROP_ITEM_THERE};

#[derive(Clone)]
pub struct DropFromEquipment {
	pub creature_id: EntityId,
	pub slot: HumanoidSlot,
}

impl Command for DropFromEquipment {
	fn execute(&self, game: &mut Game) -> Event {
		let body = game
			.map_mut()
			.entity_component_mut::<HumanoidBody>(&self.creature_id)
			.unwrap();

		let item = body.dequip(&self.slot).unwrap();

		let drop_zone_pos = *game.map().entity_zone_pos(&self.creature_id).unwrap();

		let creature_name = creature_name(game, &self.creature_id);

		let message = format!("{creature_name} dequips and drops {}.", item.entity.name());

		game.map_mut().put(&drop_zone_pos, item);

		Event::default()
			.sent_by_entity(self.creature_id)
			.with_message(message, self.creature_id, Severity::Neutral)
	}

	fn player_validate(&self, game: &Game) -> Result<(), String> {
		let drop_zone_pos = game.map().entity_zone_pos(&self.creature_id).unwrap();

		if game.map().zone_tile(drop_zone_pos).has_item() {
			Err(CANNOT_DROP_ITEM_THERE.to_string())
		} else {
			Ok(())
		}
	}
}
