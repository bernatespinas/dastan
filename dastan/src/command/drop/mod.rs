mod drop_from_equipment;
pub use drop_from_equipment::DropFromEquipment;

mod drop_from_inventory;
pub use drop_from_inventory::DropFromInventory;

const CANNOT_DROP_ITEM_THERE: &str = "Can't drop it, there already is an item here!";
