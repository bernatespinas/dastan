use super::prelude::*;

pub struct LootInventory {
	pub creature_id: EntityId,
	pub looted_id: EntityId,
	pub loot_indexes: Vec<usize>,
	pub asd_indexes: Vec<usize>,
}

impl Command for LootInventory {
	fn execute(&self, game: &mut Game) -> Event {
		let looted_items =
			Self::remove_items_from_inventory(game, &self.creature_id, &self.loot_indexes);

		let looter_items =
			Self::remove_items_from_inventory(game, &self.looted_id, &self.asd_indexes);

		Self::add_items_to_inventory(game, &self.creature_id, looted_items);

		Self::add_items_to_inventory(game, &self.looted_id, looter_items);

		let looter_name = creature_name(game, &self.creature_id);
		let looted_name = creature_name(game, &self.looted_id);

		Event::default()
			.sent_by_entity(self.creature_id)
			.with_message(
				format!("{looter_name} loots {looted_name}."),
				self.creature_id,
				Severity::Neutral,
			)
	}
}

impl LootInventory {
	fn remove_items_from_inventory(
		game: &mut Game,
		creature_id: &EntityId,
		item_indexes: &[usize],
	) -> Vec<ItemWithComponents> {
		let inventory = game
			.map_mut()
			.entity_component_mut::<Inventory>(creature_id)
			.unwrap();

		let mut items = Vec::new();

		for i in item_indexes.iter().rev() {
			items.push(inventory.remove_item(*i));
		}

		items
	}

	fn add_items_to_inventory(
		game: &mut Game,
		entity_id: &EntityId,
		items: Vec<ItemWithComponents>,
	) {
		let looted_inventory = game
			.map_mut()
			.entity_component_mut::<Inventory>(entity_id)
			.unwrap();

		for item in items {
			looted_inventory.add_item(item);
		}
	}
}
