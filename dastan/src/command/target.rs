use super::prelude::*;

#[derive(Clone, Copy)]
pub struct Target {
	pub creature_id: EntityId,
	pub target_id: EntityId,
}

impl Command for Target {
	fn execute(&self, game: &mut Game) -> Event {
		game.map_mut().entity_component_add(
			&self.creature_id,
			TargetComponent {
				entity_id: self.target_id,
			},
		);

		Event::default()
	}
}
