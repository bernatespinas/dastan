use super::{super::prelude::*, TakeDamage};

pub struct Attack {
	pub attacker_id: EntityId,
	pub target_id: EntityId,
}

impl Command for Attack {
	fn execute(&self, game: &mut Game) -> Event {
		let attacker = game.map().get_by_id::<Creature>(&self.attacker_id);

		let target = game.map().get_by_id::<Creature>(&self.target_id);

		let damage = attacker.attack_damage(target);

		TakeDamage {
			creature_id: self.target_id,
			amount: damage,
		}
		.execute(game)
	}
}
