use super::super::prelude::*;

pub struct Kill {
	pub creature_id: EntityId,
}

impl Command for Kill {
	fn execute(&self, game: &mut Game) -> Event {
		let message = format!("{} dies.", creature_name(game, &self.creature_id));

		let creature_zone_pos = *game.map().entity_zone_pos(&self.creature_id).unwrap();

		self.creature_to_corpse(game);

		Event::default()
			.sent_by_entity(self.creature_id)
			.with_message(message, self.creature_id, Severity::Bad)
			.with_updated_tiles(vec![creature_zone_pos])
	}
}

impl Kill {
	pub fn creature_to_corpse(&self, game: &mut Game) {
		let creature_zone_pos = *game.map().entity_zone_pos(&self.creature_id).unwrap();

		let creature = game
			.map_mut()
			.take_by_zone_pos::<Creature>(&creature_zone_pos);

		// TOO I remove any `Item` that was already in that `Tile`, which is not ideal.
		if game.map().zone_tile(&creature_zone_pos).has_item() {
			game.map_mut().take_by_zone_pos::<Item>(&creature_zone_pos);
		}

		let corpse = generate_corpse(creature);

		game.map_mut().put(&creature_zone_pos, corpse);
	}
}
