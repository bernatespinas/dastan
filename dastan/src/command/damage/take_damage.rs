use super::{super::prelude::*, Kill};

pub struct TakeDamage {
	pub creature_id: EntityId,
	pub amount: u16,
}

impl Command for TakeDamage {
	fn execute(&self, game: &mut Game) -> Event {
		let creature = game.map_mut().get_by_id_mut::<Creature>(&self.creature_id);

		creature.take_damage(self.amount);

		if creature.is_alive() {
			let creature_name = creature_name(game, &self.creature_id);

			Event::default()
				.sent_by_entity(self.creature_id)
				.with_message(
					format!("{creature_name} takes {} damage.", self.amount),
					self.creature_id,
					Severity::Bad,
				)
		} else {
			Kill {
				creature_id: self.creature_id,
			}
			.execute(game)
		}
	}
}
