use super::{super::prelude::*, humanoid_equip};

#[derive(Clone)]
pub struct EquipFromInventory {
	pub creature_id: EntityId,
	pub index: usize,
}

impl Command for EquipFromInventory {
	fn execute(&self, game: &mut Game) -> Event {
		let equipment = {
			let inventory = game
				.map_mut()
				.entity_component_mut::<Inventory>(&self.creature_id)
				.unwrap();

			inventory.remove_item(self.index)
		};

		humanoid_equip(game, &self.creature_id, equipment)
	}

	fn player_validate(&self, game: &Game) -> Result<(), String> {
		let inventory = game
			.map()
			.entity_component::<Inventory>(&self.creature_id)
			.unwrap();

		let body = game
			.map()
			.entity_component::<HumanoidBody>(&self.creature_id)
			.unwrap();

		let item = inventory.items().get(self.index).unwrap();

		if body.can_equip_in(&item.components.humanoid_equipment.as_ref().unwrap().slot) {
			Ok(())
		} else {
			Err("Can't equip!".to_string())
		}
	}
}
