use super::super::prelude::*;

#[derive(Clone)]
pub struct Dequip {
	pub creature_id: EntityId,
	pub slot: HumanoidSlot,
}

impl Command for Dequip {
	fn execute(&self, game: &mut Game) -> Event {
		let item = {
			let body = game
				.map_mut()
				.entity_component_mut::<HumanoidBody>(&self.creature_id)
				.unwrap();

			body.dequip(&self.slot).unwrap()
		};

		let creature_name = super::creature_name(game, &self.creature_id);

		let message = format!("{creature_name} dequips {}.", item.entity.name());

		let inventory = game
			.map_mut()
			.entity_component_mut::<Inventory>(&self.creature_id)
			.unwrap();

		inventory.pick_up(item).unwrap();

		Event::default()
			.sent_by_entity(self.creature_id)
			.with_message(message, self.creature_id, Severity::Neutral)
	}
}
