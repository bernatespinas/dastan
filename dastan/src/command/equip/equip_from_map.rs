use super::{super::prelude::*, humanoid_equip};

#[derive(Clone)]
pub struct EquipFromMap {
	pub creature_id: EntityId,
	pub zone_pos: ZonePosition,
}

impl Command for EquipFromMap {
	fn execute(&self, game: &mut Game) -> Event {
		let item = game.map_mut().take_by_zone_pos::<Item>(&self.zone_pos);

		humanoid_equip(game, &self.creature_id, item)
	}

	fn player_validate(&self, game: &Game) -> Result<(), String> {
		let item = game.map().get_by_zone_pos::<Item>(&self.zone_pos).unwrap();

		// TODO Should I unwrap or handle the None case?
		let equipment_component = game
			.map()
			.entity_component::<HumanoidEquipment>(item.id())
			.unwrap();

		let body = game
			.map()
			.entity_component::<HumanoidBody>(&self.creature_id)
			.unwrap();

		if body.can_equip_in(&equipment_component.slot) {
			Ok(())
		} else {
			Err("Can't equip! TODO Give more info.".to_string())
		}
	}
}
