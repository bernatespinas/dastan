use super::prelude::*;

mod dequip;
pub use dequip::Dequip;

mod equip_from_inventory;
pub use equip_from_inventory::EquipFromInventory;

mod equip_from_map;
pub use equip_from_map::EquipFromMap;

fn humanoid_equip(game: &mut Game, creature_id: &EntityId, item: ItemWithComponents) -> Event {
	let creature_name = creature_name(game, creature_id);

	let message = format!("{creature_name} equips {}.", item.entity.name());

	let body = game
		.map_mut()
		.entity_component_mut::<HumanoidBody>(creature_id)
		.unwrap();

	let slot = item.components.humanoid_equipment.as_ref().unwrap().slot;

	body.equip(&slot, item);

	// TODO: Update attributes.
	// let creature = game.map_mut().creature_mut(creature_id);

	Event::default().sent_by_entity(*creature_id).with_message(
		message,
		*creature_id,
		Severity::Neutral,
	)
}
