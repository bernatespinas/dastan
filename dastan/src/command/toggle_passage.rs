use super::prelude::*;

#[derive(Clone)]
pub struct TogglePassage {
	pub entity_id: EntityId,
}

impl Command for TogglePassage {
	fn execute(&self, game: &mut Game) -> Event {
		let (blocks_passage, new_icon) = {
			let passage_component = game
				.map_mut()
				.entity_component_mut::<PassageComponent>(&self.entity_id)
				.unwrap();

			passage_component.blocks_passage = !passage_component.blocks_passage;

			let new_icon = if passage_component.blocks_passage {
				passage_component.closed_icon
			} else {
				passage_component.opened_icon
			};

			(passage_component.blocks_passage, new_icon)
		};

		let entity = game.map_mut().get_by_id_mut::<Prop>(&self.entity_id);

		entity.change_icon(new_icon);

		let message = if blocks_passage {
			format!("{} is locked.", entity.name())
		} else {
			format!("{} is unlocked.", entity.name())
		};

		Event::default().with_global_message(message, Severity::Neutral)
	}
}
