use super::prelude::*;

#[derive(Clone)]
pub struct PickItemUpFromMap {
	pub creature_id: EntityId,
	pub zone_pos: ZonePosition,
}

impl Command for PickItemUpFromMap {
	fn execute(&self, game: &mut Game) -> Event {
		let item = game.map_mut().take_by_zone_pos::<Item>(&self.zone_pos);

		let creature_name = creature_name(game, &self.creature_id);

		let message = format!("{creature_name} picks up {}.", item.entity.name());

		let inventory = game
			.map_mut()
			.entity_component_mut::<Inventory>(&self.creature_id)
			.unwrap();

		inventory.pick_up(item).unwrap();

		Event::default()
			.sent_by_entity(self.creature_id)
			.with_message(message, self.creature_id, Severity::Neutral)
	}

	fn player_validate(&self, game: &Game) -> Result<(), String> {
		let item = game.map().get_by_zone_pos::<Item>(&self.zone_pos).unwrap();

		let inventory = game
			.map()
			.entity_component::<Inventory>(&self.creature_id)
			.unwrap();

		inventory.can_pick_up(item)
	}
}
