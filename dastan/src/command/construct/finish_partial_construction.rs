use super::super::prelude::*;

#[derive(Clone, Copy)]
pub struct FinishPartialConstruction {
	pub zone_pos: ZonePosition,
}

impl Command for FinishPartialConstruction {
	fn execute(&self, game: &mut Game) {
		let partial_construction: PropWithComponents = game
			.map_mut()
			.element_take(&self.zone_pos)
			// .zone_tile_mut(&self.zone_pos)
			// .item_take()
			// .map(|item| match item {
				// Item::Furniture(box Furniture::PartialConstruction(p)) => p,
				// _ => panic!(),
			// })
			// .unwrap();
			;

		match partial_construction.blueprint.output {
			BlueprintOutput::Furniture(key) => {
				let furniture = game.gen_bundle.generate_furniture(&key);
				game.map_mut()
					.zone_tile_mut(&self.zone_pos)
					.item_put(furniture);
			}
			BlueprintOutput::Terrain(new_terrain_type) => {
				game.map_mut()
					.terrain_cover(&self.zone_pos, new_terrain_type);
			}
		};
	}
}
