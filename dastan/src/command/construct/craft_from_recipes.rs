use super::super::prelude::*;

#[derive(Clone, Copy)]
pub struct CraftFromRecipes {
	pub creature_id: EntityId,
	pub profession: Profession,
	pub index: usize,
	// pub ingredients: HashSet<usize>,
}

impl Command for CraftFromRecipes {
	fn execute(&self, game: &mut Game) -> Event {
		let (ingredients, output_item_key) = {
			let recipe_book = game
				.map()
				.entity_component::<RecipeBook>(&self.creature_id)
				.unwrap();

			let recipe = recipe_book
				.recipes_by_profession(self.profession)
				.get(self.index)
				.unwrap();

			(recipe.input.clone(), recipe.output.clone())
		};

		{
			let item: ItemWithComponents =
				game.gen_bundle.generate_with_random_id(&output_item_key);

			let inventory: &mut Inventory = game
				.map_mut()
				.entity_component_mut::<Inventory>(&self.creature_id)
				.unwrap();

			let indexes = inventory.check_all_present(&ingredients).unwrap();

			inventory.remove_multiple(&indexes);

			inventory.add_item(item);
		}

		let creature_name = creature_name(game, &self.creature_id);

		Event::default()
			.sent_by_entity(self.creature_id)
			.with_message(
				format!("{creature_name} crafts {output_item_key}."),
				self.creature_id,
				Severity::Neutral,
			)
	}

	fn player_validate(&self, game: &Game) -> Result<(), String> {
		let recipe_book = game
			.map()
			.entity_component::<RecipeBook>(&self.creature_id)
			.unwrap();
		let inventory = game
			.map()
			.entity_component::<Inventory>(&self.creature_id)
			.unwrap();

		let names = &recipe_book
			.recipes_by_profession(self.profession)
			.get(self.index)
			.unwrap()
			.input;

		if inventory.check_all_present(names).is_ok() {
			Ok(())
		} else {
			Err("Missing ingredients!".to_string())
		}
	}
}
