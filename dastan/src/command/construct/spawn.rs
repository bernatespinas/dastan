use super::super::prelude::*;

// TODO Only allow one thing. spawn: SpawnThing::Creature(String)
pub struct Spawn {
	pub zone_pos: ZonePosition,
	pub creature: Option<String>,
	pub prop: Option<String>,
	pub item: Option<String>,
	// pub terrain: Option<Terrain>,
}

impl Command for Spawn {
	fn execute(&self, game: &mut Game) -> Event {
		if let Some(creature_code) = &self.creature {
			let creature: CreatureWithComponents =
				game.gen_bundle.generate_with_random_id(creature_code);

			game.map_mut().put(&self.zone_pos, creature);
		}

		if let Some(prop_code) = &self.prop {
			let prop: PropWithComponents = game.gen_bundle.generate_with_random_id(prop_code);

			game.map_mut().put(&self.zone_pos, prop);
		}

		if let Some(item_code) = &self.item {
			let item: ItemWithComponents = game.gen_bundle.generate_with_random_id(item_code);

			game.map_mut().put(&self.zone_pos, item);
		}

		Event::default()
	}
}
