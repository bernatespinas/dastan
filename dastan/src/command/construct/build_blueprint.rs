use crate::map::Map;

use super::super::prelude::*;

#[derive(Clone, Copy)]
pub struct BuildBlueprint {
	pub creature_id: EntityId,
	pub profession: Profession,
	pub index: usize,
	// pub ingredients: HashSet<usize>,
	pub zone_pos: Option<ZonePosition>,
}

impl BuildBlueprint {
	pub fn validate_ingredients(&self, map: &Map) -> Result<(), String> {
		let recipe_book = map
			.entity_component::<RecipeBook>(&self.creature_id)
			.unwrap();

		let names = &recipe_book
			.blueprints_by_profession(self.profession)
			.get(self.index)
			.unwrap()
			.input;

		let inventory = map
			.entity_component::<Inventory>(&self.creature_id)
			.unwrap();

		if inventory.check_all_present(names).is_ok() {
			Ok(())
		} else {
			Err("Missing ingredients!".to_string())
		}
	}

	fn validate_zone_pos(&self, _game: &Game) -> Result<(), String> {
		// TODO
		Ok(())
	}
}

impl Command for BuildBlueprint {
	fn execute(&self, game: &mut Game) -> Event {
		let blueprint = {
			let recipe_book = game
				.map()
				.entity_component::<RecipeBook>(&self.creature_id)
				.unwrap();

			recipe_book
				.blueprints_by_profession(self.profession)
				.get(self.index)
				.unwrap()
				.clone()
		};

		let inventory = game
			.map_mut()
			.entity_component_mut::<Inventory>(&self.creature_id)
			.unwrap();

		let indexes = inventory.check_all_present(&blueprint.input).unwrap();

		// let partial_construction = PartialConstruction::new(blueprint);
		inventory.remove_multiple(&indexes);

		let zone_pos = self
			.zone_pos
			.as_ref()
			.expect("BuildBlueprint: zone_pos has not been set, so I don't know where to build!");

		match blueprint.output {
			BlueprintOutput::Prop(key) => {
				let prop: PropWithComponents = game.gen_bundle.generate_with_random_id(&key);

				let creature_name = creature_name(game, &self.creature_id);

				let message = format!("{creature_name} builds {}.", prop.entity.name());

				game.map_mut().put(zone_pos, prop);

				Event::default()
					.sent_by_entity(self.creature_id)
					.with_message(message, self.creature_id, Severity::Neutral)
			}
			BlueprintOutput::Terrain(terrain_type) => todo!("build {terrain_type:?}"),
		}
	}

	fn player_validate(&self, game: &Game) -> Result<(), String> {
		self.validate_ingredients(game.map())?;
		self.validate_zone_pos(game)?;

		Ok(())
	}
}
