use crate::event::Event;
use crate::game::Game;

mod construct;
pub use construct::*;

mod damage;
pub use damage::*;

mod drop;
pub use drop::*;

mod eat;
pub use eat::*;

mod equip;
pub use equip::*;

mod movement;
pub use movement::*;

mod loot_inventory;
pub use loot_inventory::LootInventory;

mod pick_item_up_from_map;
pub use pick_item_up_from_map::PickItemUpFromMap;

mod skip_turn;
pub use skip_turn::SkipTurn;

mod target;
pub use target::Target;

mod toggle_passage;
pub use toggle_passage::TogglePassage;

mod prelude {
	pub use crate::{
		command::Command,
		ecs::prelude::*,
		ecs_components::{body::*, *},
		event::{Event, Severity},
		game::Game,
		map::ZonePosition,
	};

	pub fn creature_name<'a>(game: &'a Game, creature_id: &EntityId) -> &'a String {
		game.map().get_by_id::<Creature>(creature_id).name()
	}
}

pub trait Command {
	fn execute(&self, game: &mut Game) -> Event;

	// TODO I only use it to show `Popup`s... Rename it, remove it or make it more general.
	fn player_validate(&self, _game: &Game) -> Result<(), String> {
		Ok(())
	}
}

///
pub trait ValidateCommand: Command {
	fn validate_command(self, game: &Game) -> Result<Box<dyn Command>, String>;
}

impl<T: 'static + Command> ValidateCommand for T {
	fn validate_command(self, game: &Game) -> Result<Box<dyn Command>, String> {
		match self.player_validate(game) {
			Err(message) => Err(message),
			Ok(()) => Ok(Box::new(self)),
		}
	}
}
