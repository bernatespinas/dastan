use crate::game::PlayerMapChange;

use super::super::prelude::*;

impl Command for PlayerMapChange {
	fn execute(&self, game: &mut Game) -> Event {
		game.player_map_change = Some(self.clone());

		Event::default()
	}
}
