use super::super::prelude::*;

pub struct MoveCreature {
	pub src_zone_pos: ZonePosition,
	pub dst_zone_pos: ZonePosition,
}

impl Command for MoveCreature {
	fn execute(&self, game: &mut Game) -> Event {
		game.map_mut()
			.move_entity::<Creature>(&self.src_zone_pos, &self.dst_zone_pos);

		Event::default().with_updated_tiles(vec![self.src_zone_pos, self.dst_zone_pos])
	}
}
