use super::{super::prelude::*, MoveCreature};

pub struct MovePlayer {
	pub dst_zone_pos: ZonePosition,
}

impl Command for MovePlayer {
	fn execute(&self, game: &mut Game) -> Event {
		let src_zone_pos = *game.player_zone_pos();

		MoveCreature {
			src_zone_pos,
			dst_zone_pos: self.dst_zone_pos,
		}
		.execute(game)
	}
}
