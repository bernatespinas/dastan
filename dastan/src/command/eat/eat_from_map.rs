use super::{super::prelude::*, creature_eat};

#[derive(Clone)]
pub struct EatFromMap {
	pub creature_id: EntityId,
	pub zone_pos: ZonePosition,
}

impl Command for EatFromMap {
	fn execute(&self, game: &mut Game) -> Event {
		let item = game.map_mut().take_by_zone_pos::<Item>(&self.zone_pos);

		creature_eat(game, &self.creature_id, &item)
	}
}
