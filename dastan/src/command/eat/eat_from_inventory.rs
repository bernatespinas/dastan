use super::{super::prelude::*, creature_eat};

#[derive(Clone)]
pub struct EatFromInventory {
	pub creature_id: EntityId,
	pub index: usize,
}

impl Command for EatFromInventory {
	fn execute(&self, game: &mut Game) -> Event {
		let inventory = game
			.map_mut()
			.entity_component_mut::<Inventory>(&self.creature_id)
			.unwrap();

		let item: ItemWithComponents = inventory.remove_item(self.index);

		creature_eat(game, &self.creature_id, &item)
	}
}
