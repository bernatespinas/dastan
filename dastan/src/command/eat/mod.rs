use super::prelude::*;

mod eat_from_inventory;
pub use eat_from_inventory::EatFromInventory;

mod eat_from_map;
pub use eat_from_map::EatFromMap;

// This is not a Command because I need to own `food`. EatCommand would need to own it, but fn execute asks for &self.
fn creature_eat(game: &mut Game, creature_id: &EntityId, item: &ItemWithComponents) -> Event {
	let effect = item.components.food.as_ref().unwrap().effect;

	let creature = game.map_mut().get_by_id_mut::<Creature>(creature_id);

	match effect {
		Effect::Heal(heal_amount) => {
			creature.heal(heal_amount);

			Event::default().sent_by_entity(*creature_id).with_message(
				format!(
					"{} eats {} and heals for {}.",
					creature.name(),
					item.entity.name(),
					heal_amount
				),
				*creature_id,
				Severity::Good,
			)
		}
	}
}
