use std::{
	collections::HashMap,
	sync::{Arc, RwLock},
};

use uuid::Uuid;

use crate::{
	ecs::prelude::SpecializedEntityTracker,
	file_utils,
	game::UniverseBuilderData,
	generator::GeneratorBundle,
	map::MapOverview,
	map_generator::MapLayout,
	save_manager::{map_dir, save_zone},
	terrain::TerrainType,
	time::Time,
};

use super::{EntityMapTracker, Map, ZoneId};

mod map_info;
pub use map_info::MapInfo;

mod entry_point;

mod zone_builder;
pub use zone_builder::ZoneBuilder;

pub struct MapBuilder {
	map: Map,

	entity_map_tracker: EntityMapTracker,
}

impl MapBuilder {
	pub fn new(map_info: &MapInfo) -> Result<Self, String> {
		let zones_capacity = (map_info.height * map_info.width) as usize;

		let id = Uuid::new_v4();
		let dir_path = map_dir(&map_info.universe_name, &id);

		let map = Map {
			id,
			name: map_info.name.clone(),
			universe_name: map_info.universe_name.clone(),

			map_mode: map_info.map_mode,

			map_height: map_info.height,
			map_width: map_info.width,

			zone_height: map_info.zone_height,
			zone_width: map_info.zone_width,

			zones: HashMap::with_capacity(zones_capacity),
			extra_zones: Arc::new(RwLock::new(HashMap::new())),

			time: Time::default(),

			specialized_entity_tracker: SpecializedEntityTracker::default(),

			overview: MapOverview::new(map_info.height as usize, map_info.width as usize),
		};

		log::debug!("New Map will be at directory: {dir_path:?}.");
		file_utils::create_dir_all(&dir_path, "new map")?;

		Ok(Self {
			map,
			entity_map_tracker: EntityMapTracker::default(),
		})
	}

	pub fn with_layout(
		mut self,
		map_layout: &MapLayout,
		universe_builder_data: &mut UniverseBuilderData,
		gen_bundle: &GeneratorBundle,
	) -> Result<Self, String> {
		let mut entities_zone_pos_tracker: Vec<SpecializedEntityTracker> = Vec::new();

		let mut map_overview =
			MapOverview::new(self.map.map_height as usize, self.map.map_width as usize);

		for y in 0..self.map.map_height as usize {
			for x in 0..self.map.map_width as usize {
				let base_terrain = map_layout.zone_base_terrain(y, x);

				let zone_id = (y as u16, x as u16);

				log::debug!("### Zone {zone_id:?}: creation begins.");

				let mut zone_builder = ZoneBuilder::new(
					zone_id,
					self.map.zone_height,
					self.map.zone_width,
					base_terrain,
				);

				for zone_feature in map_layout.zone_features(y, x) {
					zone_feature.deploy(&mut zone_builder, universe_builder_data, gen_bundle)?;
				}

				save_zone(
					self.map.universe_name(),
					self.map.id(),
					&zone_id,
					zone_builder.zone(),
				);

				let (_, _, specialized_entity_tracker, zone_overview) = zone_builder.build();

				entities_zone_pos_tracker.push(specialized_entity_tracker);

				map_overview.set_zone_overview(&zone_id, zone_overview);

				log::debug!("### Zone {zone_id:?}: creation ends.");
			}
		}

		for entity_zone_pos_tracker in entities_zone_pos_tracker.into_iter() {
			self.import_tracked_entities(entity_zone_pos_tracker);
		}

		self.map.overview = map_overview;

		Ok(self)
	}

	pub fn map(&self) -> &Map {
		&self.map
	}

	pub fn map_mut(&mut self) -> &mut Map {
		&mut self.map
	}

	/// Creates and returns a `Zone` built for this `Map` and initialized with
	/// `Tile`s which contain the same `TerrainType` only.
	pub fn zone_builder(&self, zone_id: ZoneId, terrain_type: TerrainType) -> ZoneBuilder {
		ZoneBuilder::new(
			zone_id,
			self.map.zone_height,
			self.map.zone_width,
			terrain_type,
		)
	}

	pub fn insert_zone(&mut self, zone_builder: ZoneBuilder) {
		let (zone_id, zone, entity_zone_pos_tracker, zone_overview) = zone_builder.build();

		self.import_tracked_entities(entity_zone_pos_tracker);

		self.map.zones.insert(zone_id, zone);

		self.map.overview.set_zone_overview(&zone_id, zone_overview);
	}

	/// Imports the `Entity`s in a `SpecializedEntityTracker` into the one in
	/// the `Map` which is being built.
	pub fn import_tracked_entities(&mut self, entity_tracker: SpecializedEntityTracker) {
		for (entity_id, _) in entity_tracker.teleport.iter() {
			self.entity_map_tracker.set(*entity_id, self.map.id);
		}

		self.map.specialized_entity_tracker.import(entity_tracker);
	}

	pub fn build(self, universe_builder_data: &mut UniverseBuilderData) -> Result<Map, String> {
		universe_builder_data
			.entity_map_tracker
			.import(self.entity_map_tracker);

		Ok(self.map)
	}
}
