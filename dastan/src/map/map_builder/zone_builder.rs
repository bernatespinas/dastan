use crate::{
	ecs::prelude::{
		Creature, CreatureWithComponents, Entity, EntityWithComponents, Identify, Item,
		ItemWithComponents, Prop, PropWithComponents, SpecializedEntityTracker,
	},
	map::{
		entity_grid::EntityGrid, map_overview::ZoneOverview, tile::TileElement, Position, Tile,
		Zone, ZoneFeature, ZoneId, ZonePosition,
	},
	terrain::TerrainType,
};

pub struct ZoneBuilder {
	zone_id: ZoneId,
	zone: Zone,

	// TODO Now that I'm not using threads anymore when creating `Zone`s, do I need this here?
	entity_zone_pos_tracker: SpecializedEntityTracker,

	overview: ZoneOverview,
}

impl ZoneBuilder {
	pub fn new(zone_id: ZoneId, height: u16, width: u16, terrain_type: TerrainType) -> Self {
		Self {
			zone_id,
			zone: Zone::new(height, width, terrain_type),
			entity_zone_pos_tracker: SpecializedEntityTracker::default(),
			overview: ZoneOverview::new(terrain_type),
		}
	}

	pub fn zone(&self) -> &Zone {
		&self.zone
	}

	pub fn zone_id(&self) -> &ZoneId {
		&self.zone_id
	}

	pub fn put<E>(&mut self, position: &Position, entity_with_components: EntityWithComponents<E>)
	where
		E: Entity,
		Tile: TileElement<E>,
	{
		let zone_pos = ZonePosition::new(self.zone_id, *position);

		self.entity_zone_pos_tracker.set(
			*entity_with_components.entity.id(),
			&entity_with_components.components.mask(),
			zone_pos,
		);

		self.zone.put(position, entity_with_components);
	}

	pub fn clear_tile(&mut self, position: &Position) {
		if let Some(creature) = self.zone.tile(position).get::<Creature>() {
			self.entity_zone_pos_tracker.remove(creature.id());

			let _: CreatureWithComponents = self.zone.entity_take(position);
		}

		if let Some(prop) = self.zone.tile(position).get::<Prop>() {
			self.entity_zone_pos_tracker.remove(prop.id());

			let _: PropWithComponents = self.zone.entity_take(position);
		}

		if let Some(item) = self.zone.tile(position).get::<Item>() {
			self.entity_zone_pos_tracker.remove(item.id());

			let _: ItemWithComponents = self.zone.entity_take(position);
		}
	}

	pub fn clear_tile_and_put<E>(
		&mut self,
		position: &Position,
		entity_with_components: EntityWithComponents<E>,
	) where
		E: Entity,
		Tile: TileElement<E>,
	{
		self.clear_tile(position);
		self.put(position, entity_with_components);
	}

	pub fn terrain_put(&mut self, pos: &Position, terrain_type: TerrainType) {
		self.zone.terrain_put(pos, terrain_type);
	}

	pub fn set_overview_feature(&mut self, feature: ZoneFeature) {
		self.overview.feature.replace(feature);
	}

	pub fn build(self) -> (ZoneId, Zone, SpecializedEntityTracker, ZoneOverview) {
		(
			self.zone_id,
			self.zone,
			self.entity_zone_pos_tracker,
			self.overview,
		)
	}
}
