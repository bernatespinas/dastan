use crate::{ecs::prelude::EntityId, map::ZonePosition};

#[derive(Debug, Clone)]
pub struct EntryPoint {
	/// The `EntityId` ___ teleports to/at.
	pub entity_id: EntityId,

	pub position: Option<ZonePosition>,
}
