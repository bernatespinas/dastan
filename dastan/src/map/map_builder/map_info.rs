use std::collections::HashMap;

use crate::{
	ecs::prelude::EntityId,
	map::{MapMode, ZonePosition},
};

use super::entry_point::EntryPoint;

#[derive(Debug, Clone)]
pub struct MapInfo {
	// / The name of the universe the `Map` belongs to.
	pub universe_name: String,

	/// The name of the `Map`.
	pub name: String,

	pub map_mode: MapMode,

	/// The height of the `Map`, specified in number of `Zone`s.
	pub height: u16,

	/// The width of the `Map`, specified in number of `Zone`s.
	pub width: u16,

	/// The height of each `Zone`, specified in number of `Tile`s.
	pub zone_height: u16,

	/// The width of each `Zone`, specified in number of `Tile`s.
	pub zone_width: u16,

	///
	pub entry_points: HashMap<&'static str, EntryPoint>,
}

impl MapInfo {
	pub fn copy(&self, name: String, height: u16, width: u16) -> Self {
		MapInfo {
			universe_name: self.universe_name.clone(),

			name,
			height,
			width,

			entry_points: HashMap::new(),
			..*self
		}
	}

	pub fn with_entry_point(
		mut self,
		name: &'static str,
		entity_id: EntityId,
		zone_pos: Option<ZonePosition>,
	) -> Self {
		self.entry_points.insert(
			name,
			EntryPoint {
				entity_id,
				position: zone_pos,
			},
		);

		self
	}
}
