use std::collections::HashMap;

use hierarchical_pathfinding::internals::AbstractPath;
use hierarchical_pathfinding::{prelude::ManhattanNeighborhood, PathCache, PathCacheConfig};

use crate::map::{Map, Position, ZoneId, ZonePosition};

const COST_MAP: [isize; 3] = [-1, 1, 2];

fn cost_fn(grid: &[Vec<u8>]) -> impl '_ + Fn((usize, usize)) -> isize {
	move |(x, y)| COST_MAP[*grid.get(y).unwrap().get(x).unwrap() as usize]
}

fn cost_fn_array(grid: &[[u8; 3]; 3]) -> impl '_ + Fn((usize, usize)) -> isize {
	move |(x, y)| COST_MAP[grid[y][x] as usize]
}

pub struct Pathfinding {
	per_zone_path_cache: HashMap<ZoneId, PathCache<ManhattanNeighborhood>>,
	zone_cost_matrix: HashMap<ZoneId, Vec<Vec<u8>>>,
	map_zone_overview: PathCache<ManhattanNeighborhood>,
	// path_cache: PathCache,
}

impl Pathfinding {
	pub fn new(map: &Map) -> Pathfinding {
		let per_zone_path_cache = HashMap::new();

		// for (zone_id, cost_matrix) in map.zones_cost_matrixes(terrain_bundle) {
		// 	// let zone = map.zone(&zone_id);

		// 	let height = cost_matrix.len();
		// 	let width = cost_matrix.get(0).unwrap().len();

		// 	let zone_patch_cache = PathCache::new(
		// 		(width, height),
		// 		cost_fn(&cost_matrix),
		// 		ManhattanNeighborhood::new(width, height),
		// 		// PathCacheConfig {
		// 		// 	chunk_size:
		// 		// }
		// 		PathCacheConfig::default(),
		// 	);

		// 	per_zone_path_cache.insert(
		// 		zone_id,
		// 		zone_patch_cache
		// 	);
		// }

		let map_zone_overview_cost_matrix = map.zone_overview_cost_matrix();

		let map_zone_overview = PathCache::new(
			(3, 3),
			cost_fn_array(&map_zone_overview_cost_matrix),
			ManhattanNeighborhood::new(3, 3),
			PathCacheConfig::default(),
		);

		Pathfinding {
			per_zone_path_cache,
			zone_cost_matrix: HashMap::new(),
			map_zone_overview,
		}
	}

	fn add_zone_path_cache(&mut self, zone_id: &ZoneId, zone_cost_matrix: Vec<Vec<u8>>) {
		let height = zone_cost_matrix.len();
		let width = zone_cost_matrix.get(0).unwrap().len();

		self.zone_cost_matrix.insert(*zone_id, zone_cost_matrix);

		let zone_path_cache = PathCache::new(
			(width, height),
			cost_fn(self.zone_cost_matrix.get(zone_id).unwrap()),
			ManhattanNeighborhood::new(width, height),
			PathCacheConfig::default(),
		);

		self.per_zone_path_cache.insert(*zone_id, zone_path_cache);
	}

	pub fn invalidate_zone(&mut self, zone_id: &ZoneId) {
		self.per_zone_path_cache.remove(zone_id);
	}

	pub fn find_path(
		&mut self,
		start: &ZonePosition,
		goal: &ZonePosition,
		map: &Map,
	) -> Option<AbstractPath<ManhattanNeighborhood>> {
		if start.is_in_same_zone(goal) {
			self.find_path_same_zone(&start.zone_id, &start.pos, &goal.pos, map)
		} else {
			// todo!("find path between zones")
			None
		}
	}

	pub fn cache_needs_update(&self, zone_pos: &ZonePosition, map: &Map) -> bool {
		if let Some(zone_cost_matrix) = self.zone_cost_matrix.get(&zone_pos.zone_id) {
			let map_tile = map.zone_tile(zone_pos);

			let cache_tile_cost = *zone_cost_matrix
				.get(zone_pos.pos.y as usize)
				.unwrap()
				.get(zone_pos.pos.x as usize)
				.unwrap();

			map_tile.cost() != cache_tile_cost
		} else {
			true
		}
	}

	fn find_path_same_zone(
		&mut self,
		zone_id: &ZoneId,
		start: &Position,
		goal: &Position,
		map: &Map,
	) -> Option<AbstractPath<ManhattanNeighborhood>> {
		let zone = map.zone(zone_id);
		let grid = zone.cost_matrix();

		if !self.per_zone_path_cache.contains_key(zone_id) {
			debug!("{zone_id:?} not present, loading cost_matrix");
			self.add_zone_path_cache(zone_id, grid.clone());
		}

		self.per_zone_path_cache
			.get_mut(zone_id)
			.unwrap()
			.find_path(
				start.into(),
				goal.into(),
				cost_fn(&self.zone_cost_matrix.get(zone_id).unwrap()),
			)
	}
}
