#[derive(Debug, Copy, Clone, PartialEq, Serialize, Deserialize)]
pub enum MapMode {
	/// A looping `Map` divided in `Zone`s.
	ZonedLooping,

	/// A non-loopin `Map` divided in `Zone`s.
	Zoned,

	/// A non-looping `Map` with a single `Zone`.
	Simple,
}
