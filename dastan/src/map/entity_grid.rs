use crate::ecs::prelude::{Entity, EntityWithComponents};

use super::{tile::TileElement, Tile};

pub trait EntityGrid<P, E>
where
	E: Entity,
	Tile: TileElement<E>,
{
	fn entity_put(&mut self, position: &P, entity_with_components: EntityWithComponents<E>);
	fn entity_take(&mut self, position: &P) -> EntityWithComponents<E>;
}
