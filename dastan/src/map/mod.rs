use std::collections::HashMap;
use std::sync::{Arc, RwLock};

use direction_simple::Direction;
use uuid::Uuid;

use crate::ecs::prelude::*;
use crate::ecs_components::EcsComponent;
use crate::file_utils;
use crate::save_manager::{map_dir, map_file, save_zones_in_buffer_dir};
use crate::terrain::TerrainType;
use crate::time::Time;

mod map_builder;
pub use map_builder::{MapBuilder, MapInfo, ZoneBuilder};

mod map_mode;
pub use map_mode::MapMode;

mod position;
pub use position::{MapZonePos, Position, ZoneId, ZonePosCursor, ZonePosition};

mod zone;
pub use zone::Zone;

mod entity_grid;
use entity_grid::EntityGrid;

mod tile;
pub use tile::Tile;
use tile::TileElement;

mod map_manager;
pub use map_manager::MapManager;

mod zone_manager;
use zone_manager::{SingleZoneManager, ZoneManager, ZonedLoopingZoneManager};

// pub mod pathfinding;

mod entity_tracker;
pub use entity_tracker::{EntityMapTracker, EntityZonePosTracker};

mod map_overview;
pub use map_overview::{MapOverview, ZoneFeature, ZoneOverview};

/// A map that is divided in Zones. This makes it easier to load and unload them.
#[derive(Serialize, Deserialize)]
pub struct Map {
	/// The unique ID of the `Map`.
	id: Uuid,

	/// The name of the `Map`.
	name: String,

	/// The name of the universe the `Map` belongs to.
	universe_name: String,

	/// Number of `Zone` "rows".
	pub map_height: u16,

	/// Number of `Zone` "columns".
	pub map_width: u16,

	/// The height of each `Zone`, which should always be the same.
	pub zone_height: u16,

	/// The width of each `Zone`, which should always be the same.
	pub zone_width: u16,

	map_mode: MapMode,

	// TODO Create a `ZoneManager` or expand the current one to handle everything related to `Zone`s.
	#[serde(skip)]
	/// The currently loaded and active Zones.
	zones: HashMap<ZoneId, Zone>,

	#[serde(skip)]
	/// Inactive `Zone`s which have been loaded and are ready to be used.
	extra_zones: Arc<RwLock<HashMap<ZoneId, Zone>>>,

	pub time: Time,

	specialized_entity_tracker: SpecializedEntityTracker,

	overview: MapOverview,
}

impl Map {
	pub fn id(&self) -> &Uuid {
		&self.id
	}

	pub fn name(&self) -> &str {
		&self.name
	}

	pub fn universe_name(&self) -> &str {
		&self.universe_name
	}

	pub fn height_total(&self) -> u16 {
		(self.map_height) * (self.zone_height)
	}

	pub fn width_total(&self) -> u16 {
		(self.map_width) * (self.zone_width)
	}

	pub fn overview(&self) -> &MapOverview {
		&self.overview
	}

	fn zone(&self, zone_id: &ZoneId) -> &Zone {
		self.zones
			.get(zone_id)
			.unwrap_or_else(|| panic!("Tried to get zone {zone_id:?} but it's not loaded."))
	}

	fn zone_mut(&mut self, zone_id: &ZoneId) -> &mut Zone {
		self.zones.get_mut(zone_id).unwrap()
	}

	pub fn entity_components(&self, entity_id: &EntityId) -> EntityComponentsRef {
		let zone_id = self
			.specialized_entity_tracker
			.get(entity_id)
			.unwrap()
			.zone_id;

		self.zone(&zone_id)
			.components
			.entity_components_ref(entity_id)
	}

	pub fn entity_component<C>(&self, entity_id: &EntityId) -> Option<&C>
	where
		C: EcsComponent,
		ZoneComponents: ZoneComponentManager<C>,
	{
		let zone_id = self
			.specialized_entity_tracker
			.get(entity_id)
			.unwrap()
			.zone_id;

		self.zone(&zone_id).components.component(entity_id)
	}

	pub fn entity_component_mut<C>(&mut self, entity_id: &EntityId) -> Option<&mut C>
	where
		C: EcsComponent,
		ZoneComponents: ZoneComponentManager<C>,
	{
		let zone_id = self
			.specialized_entity_tracker
			.get(entity_id)
			.unwrap()
			.zone_id;

		self.zone_mut(&zone_id).components.component_mut(entity_id)
	}

	pub fn entity_component_add<C>(&mut self, entity_id: &EntityId, component: C)
	where
		C: EcsComponent,
		ZoneComponents: ZoneComponentManager<C>,
	{
		let zone_id = self
			.specialized_entity_tracker
			.get(entity_id)
			.unwrap()
			.zone_id;

		self.zone_mut(&zone_id)
			.components
			.add_component(*entity_id, component);
	}

	pub fn entity_component_remove<C>(&mut self, entity_id: &EntityId) -> Option<C>
	where
		C: EcsComponent,
		ZoneComponents: ZoneComponentManager<C>,
	{
		let zone_id = self
			.specialized_entity_tracker
			.get(entity_id)
			.unwrap()
			.zone_id;

		self.zone_mut(&zone_id)
			.components
			.remove_component(entity_id)
	}

	pub fn get_by_zone_pos<T>(&self, zone_pos: &ZonePosition) -> Option<&T>
	where
		Tile: TileElement<T>,
	{
		self.zone_tile(zone_pos).get()
	}

	pub fn get_by_zone_pos_mut<T>(&mut self, zone_pos: &ZonePosition) -> Option<&mut T>
	where
		Tile: TileElement<T>,
	{
		self.zone_tile_mut(zone_pos).get_mut_ref()
	}

	pub fn get_by_id<E>(&self, entity_id: &EntityId) -> &E
	where
		E: Entity,
		Tile: TileElement<E>,
	{
		let zone_pos = self.entity_zone_pos(entity_id).unwrap();

		self.get_by_zone_pos(zone_pos).unwrap()
	}

	pub fn get_by_id_mut<E>(&mut self, entity_id: &EntityId) -> &mut E
	where
		E: Entity,
		Tile: TileElement<E>,
	{
		let zone_pos = *self.entity_zone_pos(entity_id).unwrap();

		self.get_by_zone_pos_mut(&zone_pos).unwrap()
	}

	pub fn take_by_zone_pos<E>(&mut self, zone_pos: &ZonePosition) -> EntityWithComponents<E>
	where
		E: Entity,
		Tile: TileElement<E>,
	{
		self.entity_take(zone_pos)
	}

	pub fn put<E>(
		&mut self,
		zone_pos: &ZonePosition,
		entity_with_components: EntityWithComponents<E>,
	) where
		E: Entity,
		Tile: TileElement<E>,
	{
		self.entity_put(zone_pos, entity_with_components);
	}

	pub fn entity_zone_pos(&self, entity_id: &EntityId) -> Option<&ZonePosition> {
		self.specialized_entity_tracker.get(entity_id)
	}

	/// Moves an `Entity` from one `ZonePosition` to another. If the `Zone` is
	/// the same, just move the `Entity`. If the `Zone` is different, move the
	/// `Entity` and its components.
	pub fn move_entity<E>(&mut self, src_zone_pos: &ZonePosition, dst_zone_pos: &ZonePosition)
	where
		E: Entity,
		Tile: TileElement<E>,
	{
		if src_zone_pos.is_in_same_zone(dst_zone_pos) {
			let entity: E = self.zone_tile_mut(src_zone_pos).take().unwrap();

			let entity_components_mask = self.entity_components(entity.id()).mask();

			self.specialized_entity_tracker.set(
				*entity.id(),
				&entity_components_mask,
				*dst_zone_pos,
			);

			self.zone_tile_mut(dst_zone_pos).put(entity);
		} else {
			let entity_with_components = self.take_by_zone_pos::<E>(src_zone_pos);

			self.entity_put(dst_zone_pos, entity_with_components);
		}
	}

	pub fn zone_tile(&self, zone_pos: &ZonePosition) -> &Tile {
		self.zones
			.get(&zone_pos.zone_id)
			.unwrap_or_else(|| panic!("invalid zone_id: {:?}", zone_pos.zone_id))
			.tile(&zone_pos.pos)
	}

	pub fn zone_tile_mut(&mut self, zone_pos: &ZonePosition) -> &mut Tile {
		self.zones
			.get_mut(&zone_pos.zone_id)
			.unwrap()
			.tile_mut(&zone_pos.pos)
	}

	pub fn load_zone(&mut self, zone_id: &ZoneId) -> Result<(), String> {
		let zone =
			crate::save_manager::load_zone(&map_dir(&self.universe_name, &self.id), zone_id)?;

		self.zones.insert(*zone_id, zone);

		Ok(())
	}

	pub fn load_initial_zones(&mut self, player_zone_id: &ZoneId) -> Result<(), String> {
		match self.map_mode {
			MapMode::Simple => SingleZoneManager::load_initial_zones(self, player_zone_id),
			MapMode::Zoned => todo!(),
			MapMode::ZonedLooping => {
				ZonedLoopingZoneManager::load_initial_zones(self, player_zone_id)
			}
		}
	}

	pub fn update_loaded_zones(&mut self, player_zone_id: &ZoneId) -> Result<(), String> {
		match self.map_mode {
			MapMode::Simple => SingleZoneManager::update_loaded_zones(self, player_zone_id),
			MapMode::Zoned => todo!(),
			MapMode::ZonedLooping => {
				ZonedLoopingZoneManager::update_loaded_zones(self, player_zone_id)
			}
		}
	}

	pub fn save(&self) -> Result<(), String> {
		log::debug!("Saving map {}...", self.id);

		save_zones_in_buffer_dir(self, &self.zones)?;
		save_zones_in_buffer_dir(self, &self.extra_zones.read().unwrap())?;

		let map_file_path = map_file(&self.universe_name, &self.id);
		file_utils::serialize_and_write(&self, map_file_path, "map")?;

		// Move Zones' files from the buffer dir to the zones dir.
		let map_dir_path = map_dir(&self.universe_name, &self.id);
		// TODO This should not be a function in `file_utils`.
		file_utils::replace_buffer_zones(&map_dir_path)?;

		Ok(())
	}

	// TODO Return an `Option`.
	pub fn pos_apply_dir(
		&self,
		zone_pos: &ZonePosition,
		dir: Direction,
	) -> Result<ZonePosition, ()> {
		match self.map_mode {
			MapMode::Simple => SingleZoneManager::pos_apply_dir(self, zone_pos, dir),
			MapMode::Zoned => todo!(),
			MapMode::ZonedLooping => ZonedLoopingZoneManager::pos_apply_dir(self, zone_pos, dir),
		}
	}

	/// Tries to apply a Direction to a ZonePosition many times. Returns the last valid ZonePosition.
	pub fn pos_apply_dir_amount(
		&self,
		zone_pos: &ZonePosition,
		dir: Direction,
		amount: usize,
	) -> Result<ZonePosition, ZonePosition> {
		match self.map_mode {
			MapMode::Simple => SingleZoneManager::pos_apply_dir_amount(self, zone_pos, dir, amount),
			MapMode::Zoned => todo!(),
			MapMode::ZonedLooping => {
				ZonedLoopingZoneManager::pos_apply_dir_amount(self, zone_pos, dir, amount)
			}
		}
	}

	pub fn zone_pos_to_pos(&self, zone_pos: &ZonePosition) -> Position {
		let y = zone_pos.zone_id.0 * self.zone_height + zone_pos.pos.y;
		let x = zone_pos.zone_id.1 * self.zone_width + zone_pos.pos.x;

		Position { y, x }
	}

	pub fn are_adjacent(&self, zone_pos_1: &ZonePosition, zone_pos_2: &ZonePosition) -> bool {
		// Apply every Direction to a ZonePosition and check if the other ZonePosition appears.
		for dir in Direction::list().iter() {
			if let Ok(zone_pos_dir) = self.pos_apply_dir(zone_pos_1, *dir) {
				if *zone_pos_2 == zone_pos_dir {
					return true;
				}
			}
		}

		false
	}
}

impl<E> EntityGrid<ZonePosition, E> for Map
where
	E: Entity,
	Tile: TileElement<E>,
{
	fn entity_put(
		&mut self,
		zone_pos: &ZonePosition,
		entity_with_components: EntityWithComponents<E>,
	) {
		self.specialized_entity_tracker.set(
			*entity_with_components.id(),
			&entity_with_components.components.mask(),
			*zone_pos,
		);

		self.zone_mut(&zone_pos.zone_id)
			.entity_put(&zone_pos.pos, entity_with_components);
	}

	fn entity_take(&mut self, zone_pos: &ZonePosition) -> EntityWithComponents<E> {
		let entity_with_components = self.zone_mut(&zone_pos.zone_id).entity_take(&zone_pos.pos);

		self.specialized_entity_tracker
			.remove(entity_with_components.id());

		entity_with_components
	}
}
