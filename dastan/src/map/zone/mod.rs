use crate::ecs::prelude::*;

use super::{entity_grid::EntityGrid, Position, TerrainType, Tile, TileElement};

#[derive(Serialize, Deserialize)]
pub struct Zone {
	grid: Vec<Vec<Tile>>,
	pub components: ZoneComponents,
}

impl Zone {
	pub fn new(height: u16, width: u16, terrain_type: TerrainType) -> Zone {
		let grid = vec![vec![Tile::new(terrain_type); width as usize]; height as usize];

		Zone {
			grid,
			components: ZoneComponents::default(),
		}
	}

	pub fn height(&self) -> u16 {
		self.grid.len() as u16
	}

	pub fn width(&self) -> u16 {
		self.grid.get(0).unwrap().len() as u16
	}

	pub fn tile_yx(&self, y: usize, x: usize) -> &Tile {
		&self.grid[y][x]
	}

	pub fn tile(&self, pos: &Position) -> &Tile {
		&self.grid[pos.y as usize][pos.x as usize]
	}

	// fn tile_mut_yx(&mut self, y: usize, x: usize) -> &mut Tile {
	// 	&mut self.grid[y][x]
	// }

	pub(super) fn tile_mut(&mut self, pos: &Position) -> &mut Tile {
		&mut self.grid[pos.y as usize][pos.x as usize]
	}

	pub fn put<E>(&mut self, position: &Position, entity_with_components: EntityWithComponents<E>)
	where
		E: Entity,
		Tile: TileElement<E>,
	{
		self.entity_put(position, entity_with_components);
	}

	pub fn clear_tile(&mut self, position: &Position) {
		if self.tile(position).has_creature() {
			let _: CreatureWithComponents = self.entity_take(position);
		}

		if self.tile(position).has_prop() {
			let _: PropWithComponents = self.entity_take(position);
		}

		if self.tile(position).has_item() {
			let _: ItemWithComponents = self.entity_take(position);
		}
	}

	pub fn clear_tile_and_put<E>(
		&mut self,
		position: &Position,
		entity_with_components: EntityWithComponents<E>,
	) where
		E: Entity,
		Tile: TileElement<E>,
	{
		self.clear_tile(position);
		self.put(position, entity_with_components);
	}

	pub fn terrain_put(&mut self, pos: &Position, terrain_type: TerrainType) {
		self.tile_mut(pos).terrain_type = terrain_type;
	}

	pub fn grid(&self) -> &Vec<Vec<Tile>> {
		&self.grid
	}
}

impl<E> EntityGrid<Position, E> for Zone
where
	E: Entity,
	Tile: TileElement<E>,
{
	fn entity_put(&mut self, position: &Position, entity_with_components: EntityWithComponents<E>) {
		let creature_id = entity_with_components.entity.id();

		self.components
			.import_entity(creature_id, entity_with_components.components);

		self.tile_mut(position).put(entity_with_components.entity);
	}

	fn entity_take(&mut self, position: &Position) -> EntityWithComponents<E> {
		let entity: E = self.tile_mut(position).take().unwrap();
		let components = self.components.export_entity(entity.id());

		EntityWithComponents { entity, components }
	}
}
