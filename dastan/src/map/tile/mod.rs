use crate::ecs::prelude::*;
use crate::size::Size;
use crate::terrain::TerrainType;

mod tile_element;
pub use tile_element::TileElement;

/// In each tile of the map...:
///  · There is a terrain (floor, wall, water...).
///    · Terrains might or might not block passage.
///    · Since there probably is the same Terrain in multiple Tiles, I store a TerrainType instead, which is smaller, to represent each actual Terrain.
///  · There might be an item, but only one (a potion, a sword...).
///    · Items might or might not block passage.
///  · There might be a creature, but only one.
///    · Creatures always block passage.
#[derive(Clone, Serialize, Deserialize)]
pub struct Tile {
	creature: Option<Creature>,
	prop: Option<Prop>,
	item: Option<Item>,
	pub terrain_type: TerrainType,
}

impl Tile {
	pub fn new(terrain_type: TerrainType) -> Self {
		Self {
			creature: None,
			prop: None,
			item: None,
			terrain_type,
		}
	}

	pub fn terrain_put(&mut self, terrain_type: TerrainType) {
		self.terrain_type = terrain_type;
	}

	pub fn get<T>(&self) -> Option<&T>
	where
		Tile: TileElement<T>,
	{
		self.get_ref()
	}

	pub fn has_creature(&self) -> bool {
		self.creature.is_some()
	}

	pub fn has_prop(&self) -> bool {
		self.prop.is_some()
	}

	pub fn has_item(&self) -> bool {
		self.item.is_some()
	}

	// TODO `Prop`s might not always block passage. `PassageComponent` exists!
	pub fn blocks_passage(&self) -> bool {
		self.terrain_type.blocks_passage()
			|| self.creature.is_some()
			|| self.prop.is_some()
			|| self.item_blocks_passage()
	}

	pub fn item_blocks_passage(&self) -> bool {
		if let Some(item) = &self.item {
			item.size() == Size::Big
		} else {
			false
		}
	}

	pub fn leave_terrain_only(&mut self) {
		self.creature = None;
		self.prop = None;
		self.item = None;
	}
}

impl TileElement<Creature> for Tile {
	fn get_ref(&self) -> Option<&Creature> {
		self.creature.as_ref()
	}

	fn get_mut_ref(&mut self) -> Option<&mut Creature> {
		self.creature.as_mut()
	}

	fn take(&mut self) -> Option<Creature> {
		self.creature.take()
	}

	fn put(&mut self, t: Creature) {
		if let Some(cretin) = &self.creature {
			log::debug!(
				"creature_put in tile with {} already there, panic now",
				cretin.name()
			);
		}
		debug_assert!(
			self.creature.is_none(),
			"creature_put -> self.creature.is_some()!!"
		);

		self.creature.replace(t);
	}
}

impl TileElement<Prop> for Tile {
	fn get_ref(&self) -> Option<&Prop> {
		self.prop.as_ref()
	}

	fn get_mut_ref(&mut self) -> Option<&mut Prop> {
		self.prop.as_mut()
	}

	fn take(&mut self) -> Option<Prop> {
		self.prop.take()
	}

	fn put(&mut self, t: Prop) {
		self.prop.replace(t);
	}
}

impl TileElement<Item> for Tile {
	fn get_ref(&self) -> Option<&Item> {
		self.item.as_ref()
	}

	fn get_mut_ref(&mut self) -> Option<&mut Item> {
		self.item.as_mut()
	}

	fn take(&mut self) -> Option<Item> {
		self.item.take()
	}

	fn put(&mut self, t: Item) {
		debug_assert!(self.item.is_none(), "item_put -> self.item.is_some()!!");

		self.item.replace(t);
	}
}
