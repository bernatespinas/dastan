pub trait TileElement<T> {
	fn get_ref(&self) -> Option<&T>;
	fn get_mut_ref(&mut self) -> Option<&mut T>;
	fn take(&mut self) -> Option<T>;
	fn put(&mut self, t: T);
}
