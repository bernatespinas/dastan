use crate::terrain::TerrainType;

use super::ZoneFeature;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ZoneOverview {
	/// The "main/predominant" `TerrainType` of the `Zone`.
	pub terrain: TerrainType,

	/// The main feature of the `Zone`.
	pub feature: Option<ZoneFeature>,
}

impl ZoneOverview {
	pub fn new(terrain: TerrainType) -> Self {
		Self {
			terrain,
			feature: None,
		}
	}

	pub fn set_feature(&mut self, feature: ZoneFeature) {
		self.feature.replace(feature);
	}
}
