use std::collections::HashMap;

use super::ZoneId;

mod zone_overview;
pub use zone_overview::ZoneOverview;

mod zone_feature;
pub use zone_feature::ZoneFeature;

#[derive(Debug, Serialize, Deserialize)]
pub struct MapOverview {
	zones: HashMap<ZoneId, ZoneOverview>,

	height: usize,
	width: usize,
}

impl MapOverview {
	pub fn new(height: usize, width: usize) -> Self {
		Self {
			zones: HashMap::with_capacity(height * width),
			height,
			width,
		}
	}

	pub fn height(&self) -> u16 {
		self.height as u16
	}

	pub fn width(&self) -> u16 {
		self.width as u16
	}

	pub fn get(&self, zone_id: &ZoneId) -> &ZoneOverview {
		self.zones.get(zone_id).unwrap()
	}

	pub fn set_zone_overview(&mut self, zone_id: &ZoneId, zone_overview: ZoneOverview) {
		self.zones.insert(*zone_id, zone_overview);
	}
}
