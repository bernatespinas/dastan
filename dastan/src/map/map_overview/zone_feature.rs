#[derive(Debug, Clone, Copy, Eq, PartialEq, Serialize, Deserialize)]
pub enum ZoneFeature {
	Forest,
	River,
	Settlement,
}
