use std::collections::HashMap;
use std::sync::{Arc, RwLock};
use std::thread;

use direction_simple::Direction;

use crate::map::{Map, Position, Zone, ZoneId, ZonePosition};
use crate::save_manager::{load_zone, map_dir};

use super::ZoneManager;

pub struct ZonedLoopingZoneManager {}

impl ZonedLoopingZoneManager {
	/// Returns the current and surrounding ZoneIds (9 in total), ordered from top-left to bot-right.
	fn current_and_surrounding_zones_ids(map: &Map, current_zone_id: &ZoneId) -> [ZoneId; 9] {
		let curr_y = current_zone_id.0;
		let curr_x = current_zone_id.1;

		[
			(get_zone_y_up(map, curr_y), get_zone_x_left(map, curr_x)),
			(get_zone_y_up(map, curr_y), curr_x),
			(get_zone_y_up(map, curr_y), get_zone_x_right(map, curr_x)),
			(curr_y, get_zone_x_left(map, curr_x)),
			*current_zone_id,
			(curr_y, get_zone_x_right(map, curr_x)),
			(get_zone_y_down(map, curr_y), get_zone_x_left(map, curr_x)),
			(get_zone_y_down(map, curr_y), curr_x),
			(get_zone_y_down(map, curr_y), get_zone_x_right(map, curr_x)),
		]
	}

	/// ¿Concurrently? load the Zones that are exactly $radius "distance" from the Zone the player is in/at/_.
	fn load_extra_zones(map: &mut Map, current_zone_id: &ZoneId, radius: u8) -> Result<(), String> {
		let extra_zones_ids = Self::radius_zones_ids(map, current_zone_id, radius);

		log::debug!("Found {} extra Zones to load.", extra_zones_ids.len());

		for zone_id in extra_zones_ids {
			let extra_zones = map.extra_zones.clone();
			let map_dir = map_dir(&map.universe_name, &map.id);

			// TODO Should I `join` them or something...? Has it not crashed because it's not too slow?
			thread::spawn(move || {
				// Before loading a Zone, check that it's not already loaded.
				if extra_zones.read().unwrap().contains_key(&zone_id) {
					log::debug!("Not loading extra Zone {zone_id:?} from file, already loaded.");
				} else {
					log::debug!("Loading extra Zone {zone_id:?} from file.");

					// TODO Unwrapping Result, I'd like to "?" it.
					let zone = load_zone(&map_dir, &zone_id).unwrap();

					extra_zones.write().unwrap().insert(zone_id, zone);

					log::debug!("Loaded extra Zone {zone_id:?} from file.");
				}
			});
		}

		Ok(())
	}

	///
	/// 1 1 1 1 2
	/// 4 . . . 2
	/// 4 . X . 2
	/// 4 . . . 2
	/// 4 3 3 3 3
	/// X: Zone where the player is ¿at/in/_?.
	/// .: Immediatly surrounding Zone, stored in self.zones.
	/// 1-4: Zones loaded in self.extra_zones, in order of insertion.
	fn radius_zones_ids(map: &Map, current_zone_id: &ZoneId, radius: u8) -> Vec<ZoneId> {
		let mut zones_ids: Vec<ZoneId> = Vec::with_capacity(2 * radius as usize - 1);

		log::debug!("Current zone_id is {current_zone_id:?}.");

		ZonedLoopingZoneManager::add_zones_src_dir_amount(
			map,
			current_zone_id,
			Direction::UpLeft,
			radius,
			Direction::Right,
			&mut zones_ids,
		);

		log::debug!(
			"Added {} Zones after {:?}: {zones_ids:?}.",
			zones_ids.len(),
			Direction::Right,
		);

		ZonedLoopingZoneManager::add_zones_src_dir_amount(
			map,
			current_zone_id,
			Direction::UpRight,
			radius,
			Direction::Down,
			&mut zones_ids,
		);

		log::debug!(
			"Added {} Zones after {:?}: {zones_ids:?}.",
			zones_ids.len(),
			Direction::Down,
		);

		ZonedLoopingZoneManager::add_zones_src_dir_amount(
			map,
			current_zone_id,
			Direction::DownRight,
			radius,
			Direction::Left,
			&mut zones_ids,
		);

		log::debug!(
			"Added {} Zones after {:?}: {zones_ids:?}.",
			zones_ids.len(),
			Direction::Left,
		);

		ZonedLoopingZoneManager::add_zones_src_dir_amount(
			map,
			current_zone_id,
			Direction::DownLeft,
			radius,
			Direction::Up,
			&mut zones_ids,
		);

		log::debug!(
			"Added {} Zones after {:?}: {zones_ids:?}.",
			zones_ids.len(),
			Direction::Up,
		);

		log::debug!("{} Zones before retain.", zones_ids.len());

		log::debug!(
			"self.zones: {:?}",
			map.zones.keys().collect::<Vec<&ZoneId>>()
		);
		log::debug!("extra_zones_ids: {zones_ids:?}");

		zones_ids.retain(|&zone_id| !map.zones.contains_key(&zone_id));

		log::debug!("{} Zones after retain.", zones_ids.len());

		zones_ids
	}

	fn zone_id_apply_dir(map: &Map, zone_id: &ZoneId, dir: Direction) -> ZoneId {
		match dir {
			Direction::Up => (get_zone_y_up(map, zone_id.0), zone_id.1),
			Direction::Down => (get_zone_y_down(map, zone_id.0), zone_id.1),
			Direction::Left => (zone_id.0, get_zone_x_left(map, zone_id.1)),
			Direction::Right => (zone_id.0, get_zone_x_right(map, zone_id.1)),
			Direction::UpLeft => (
				get_zone_y_up(map, zone_id.0),
				get_zone_x_left(map, zone_id.1),
			),
			Direction::UpRight => (
				get_zone_y_up(map, zone_id.0),
				get_zone_x_right(map, zone_id.1),
			),
			Direction::DownLeft => (
				get_zone_y_down(map, zone_id.0),
				get_zone_x_left(map, zone_id.1),
			),
			Direction::DownRight => (
				get_zone_y_down(map, zone_id.0),
				get_zone_x_right(map, zone_id.1),
			),
		}
	}

	fn get_destination_zone_id(map: &Map, zone_id: &ZoneId, dir: Direction, amount: u8) -> ZoneId {
		let mut destination = *zone_id;

		for _ in 0..amount {
			destination = Self::zone_id_apply_dir(map, &destination, dir);
		}

		destination
	}

	fn add_zones_src_dir_amount(
		map: &Map,
		current_zone_id: &ZoneId,
		dir_src: Direction,
		amount: u8,
		dir_dest: Direction,
		zones_ids: &mut Vec<ZoneId>,
	) {
		let src = Self::get_destination_zone_id(map, current_zone_id, dir_src, amount);

		log::debug!("src ({dir_src:?}, {amount}) is {src:?}.");

		for i in 0..amount * 2 {
			zones_ids.push(Self::get_destination_zone_id(map, &src, dir_dest, i));
		}
	}
}

impl ZoneManager for ZonedLoopingZoneManager {
	fn load_initial_zones(map: &mut Map, current_zone_id: &ZoneId) -> Result<(), String> {
		let zone_ids =
			ZonedLoopingZoneManager::current_and_surrounding_zones_ids(map, current_zone_id);

		for zone_id in &zone_ids {
			let zone = load_zone(&map_dir(&map.universe_name, &map.id), zone_id)?;

			map.zones.insert(*zone_id, zone);
		}

		log::debug!(
			"{} Zones loaded (current: {current_zone_id:?} + surrounding)",
			map.zones.len()
		);

		ZonedLoopingZoneManager::load_extra_zones(map, current_zone_id, 2)
	}

	fn update_loaded_zones(map: &mut Map, current_zone_id: &ZoneId) -> Result<(), String> {
		// The new current_id and its immediate surrounding zones_ids.
		let new_zone_ids =
			ZonedLoopingZoneManager::current_and_surrounding_zones_ids(map, current_zone_id)
				.to_vec();

		// The HashMap that will contain the new Zones.
		let new_zones: Arc<RwLock<HashMap<ZoneId, Zone>>> = Arc::new(RwLock::new(HashMap::new()));

		// The currently loaded Zones. I extract them so I can move them.
		let mut old_zones = std::mem::take(&mut map.zones);

		let mut handles = Vec::new();

		// Fetch the "new" set of Zones, from map.zones if present or from their serialized files otherwise.
		for zone_id in new_zone_ids {
			let zone = old_zones.remove(&zone_id);
			let map_dir = map_dir(&map.universe_name, &map.id);

			let new_zones_clone = new_zones.clone();
			let extra_zones = map.extra_zones.clone();

			let handle = thread::spawn(move || {
				log::debug!("Loading Zone {zone_id:?}.");

				match zone {
					Some(zone) => {
						// Zone was already loaded in map.zones.
						new_zones_clone.write().unwrap().insert(zone_id, zone);
						log::debug!("Loaded Zone {zone_id:?} from old_zones.");
					}
					None => {
						// Zone was not already loaded in map.zones.
						match extra_zones.write().unwrap().remove(&zone_id) {
							Some(extra_zone) => {
								// Zone was already loaded in map.extra_zones.
								new_zones_clone.write().unwrap().insert(zone_id, extra_zone);
								log::debug!("Loaded Zone {zone_id:?} from extra_zones.");
							}
							None => {
								// Zone was not already loaded in map.extra_zones.
								// Deserialize its file and insert it.
								let zone_des = load_zone(&map_dir, &zone_id).unwrap();

								new_zones_clone.write().unwrap().insert(zone_id, zone_des);
								log::debug!("Loaded Zone {zone_id:?} from unloaded file.");
							}
						}
					}
				};
			});

			handles.push(handle);
		}

		// TODO I think I don't know when to use join.
		for handle in handles {
			handle.join().unwrap();
		}

		// Store all unneeded Zones in extra_zones.
		// TODO Save them or keep them around compressed.
		for (zone_id, zone) in old_zones {
			map.extra_zones.write().unwrap().insert(zone_id, zone);
		}

		ZonedLoopingZoneManager::load_extra_zones(map, current_zone_id, 2)?;

		map.zones = std::mem::take(&mut new_zones.write().unwrap());

		Ok(())
	}

	fn pos_apply_dir(
		map: &Map,
		zone_pos: &crate::map::ZonePosition,
		dir: Direction,
	) -> Result<ZonePosition, ()> {
		// Since the Map is expected to be looping, this is always Ok(..).
		Ok(match dir {
			Direction::Up => {
				if zone_pos.pos.y > 0 {
					ZonePosition {
						zone_id: zone_pos.zone_id,
						pos: Position::new(zone_pos.pos.y - 1, zone_pos.pos.x),
					}
				} else {
					ZonePosition {
						zone_id: (get_zone_y_up(map, zone_pos.zone_id.0), zone_pos.zone_id.1),
						pos: Position::new(map.zone_height - 1, zone_pos.pos.x),
					}
				}
			}

			Direction::Down => {
				if zone_pos.pos.y < map.zone_height - 1 {
					ZonePosition {
						zone_id: zone_pos.zone_id,
						pos: Position::new(zone_pos.pos.y + 1, zone_pos.pos.x),
					}
				} else {
					ZonePosition {
						zone_id: (get_zone_y_down(map, zone_pos.zone_id.0), zone_pos.zone_id.1),
						pos: Position::new(0, zone_pos.pos.x),
					}
				}
			}

			Direction::Right => {
				if zone_pos.pos.x < map.zone_width - 1 {
					ZonePosition {
						zone_id: zone_pos.zone_id,
						pos: Position::new(zone_pos.pos.y, zone_pos.pos.x + 1),
					}
				} else {
					ZonePosition {
						zone_id: (
							zone_pos.zone_id.0,
							get_zone_x_right(map, zone_pos.zone_id.1),
						),
						pos: Position::new(zone_pos.pos.y, 0),
					}
				}
			}

			Direction::Left => {
				if zone_pos.pos.x > 0 {
					ZonePosition {
						zone_id: zone_pos.zone_id,
						pos: Position::new(zone_pos.pos.y, zone_pos.pos.x - 1),
					}
				} else {
					ZonePosition {
						zone_id: (zone_pos.zone_id.0, get_zone_x_left(map, zone_pos.zone_id.1)),
						pos: Position::new(zone_pos.pos.y, map.zone_width - 1),
					}
				}
			}

			Direction::UpRight => {
				let (y, zone_id_y) = if zone_pos.pos.y > 0 {
					(zone_pos.pos.y - 1, zone_pos.zone_id.0)
				} else {
					(map.zone_height - 1, get_zone_y_up(map, zone_pos.zone_id.0))
				};
				let (x, zone_id_x) = if zone_pos.pos.x < map.zone_width - 1 {
					(zone_pos.pos.x + 1, zone_pos.zone_id.1)
				} else {
					(0, get_zone_x_right(map, zone_pos.zone_id.1))
				};

				ZonePosition {
					zone_id: (zone_id_y, zone_id_x),
					pos: Position::new(y, x),
				}
			}

			Direction::UpLeft => {
				let (y, zone_id_y) = if zone_pos.pos.y > 0 {
					(zone_pos.pos.y - 1, zone_pos.zone_id.0)
				} else {
					(map.zone_height - 1, get_zone_y_up(map, zone_pos.zone_id.0))
				};
				let (x, zone_id_x) = if zone_pos.pos.x > 0 {
					(zone_pos.pos.x - 1, zone_pos.zone_id.1)
				} else {
					(map.zone_width - 1, get_zone_x_left(map, zone_pos.zone_id.1))
				};

				ZonePosition {
					zone_id: (zone_id_y, zone_id_x),
					pos: Position::new(y, x),
				}
			}

			Direction::DownRight => {
				let (y, zone_id_y) = if zone_pos.pos.y < map.zone_height - 1 {
					(zone_pos.pos.y + 1, zone_pos.zone_id.0)
				} else {
					(0, get_zone_y_down(map, zone_pos.zone_id.0))
				};
				let (x, zone_id_x) = if zone_pos.pos.x < map.zone_width - 1 {
					(zone_pos.pos.x + 1, zone_pos.zone_id.1)
				} else {
					(0, get_zone_x_right(map, zone_pos.zone_id.1))
				};

				ZonePosition {
					zone_id: (zone_id_y, zone_id_x),
					pos: Position::new(y, x),
				}
			}

			Direction::DownLeft => {
				let (y, zone_id_y) = if zone_pos.pos.y < map.zone_height - 1 {
					(zone_pos.pos.y + 1, zone_pos.zone_id.0)
				} else {
					(0, get_zone_y_down(map, zone_pos.zone_id.0))
				};
				let (x, zone_id_x) = if zone_pos.pos.x > 0 {
					(zone_pos.pos.x - 1, zone_pos.zone_id.1)
				} else {
					(map.zone_width - 1, get_zone_x_left(map, zone_pos.zone_id.1))
				};

				ZonePosition {
					zone_id: (zone_id_y, zone_id_x),
					pos: Position::new(y, x),
				}
			}
		})
	}

	fn pos_apply_dir_amount(
		map: &Map,
		zone_pos: &crate::map::ZonePosition,
		dir: Direction,
		amount: usize,
	) -> Result<ZonePosition, ZonePosition> {
		let mut final_zone_pos = *zone_pos;

		for _i in 0..amount {
			final_zone_pos = Self::pos_apply_dir(map, &final_zone_pos, dir).unwrap();
		}

		Ok(final_zone_pos)
	}
}

fn get_zone_y_up(map: &Map, y: u16) -> u16 {
	if y == 0 {
		map.map_height - 1
	} else {
		y - 1
	}
}

fn get_zone_y_down(map: &Map, y: u16) -> u16 {
	if y == map.map_height - 1 {
		0
	} else {
		y + 1
	}
}

fn get_zone_x_left(map: &Map, x: u16) -> u16 {
	if x == 0 {
		map.map_width - 1
	} else {
		x - 1
	}
}

fn get_zone_x_right(map: &Map, x: u16) -> u16 {
	if x == map.map_width - 1 {
		0
	} else {
		x + 1
	}
}
