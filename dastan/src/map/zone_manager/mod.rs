use direction_simple::Direction;

use super::{Map, ZoneId, ZonePosition};

mod single_zone_manager;
pub use single_zone_manager::SingleZoneManager;

mod zoned_looping_zone_manager;
pub use zoned_looping_zone_manager::ZonedLoopingZoneManager;

pub trait ZoneManager {
	fn load_initial_zones(map: &mut Map, current_zone_id: &ZoneId) -> Result<(), String>;

	fn update_loaded_zones(map: &mut Map, current_zone_id: &ZoneId) -> Result<(), String>;

	/// Applies a Direction to a ZonePosition and returns the resulting ZonePosition.
	fn pos_apply_dir(
		map: &Map,
		zone_pos: &ZonePosition,
		dir: Direction,
	) -> Result<ZonePosition, ()>;

	fn pos_apply_dir_amount(
		map: &Map,
		zone_pos: &ZonePosition,
		dir: Direction,
		amount: usize,
	) -> Result<ZonePosition, ZonePosition>;
}
