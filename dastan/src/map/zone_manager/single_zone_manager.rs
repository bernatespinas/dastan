use direction_simple::Direction;

use crate::{
	map::{Map, Position, ZoneId, ZonePosition},
	save_manager::{load_zone, map_dir},
};

use super::ZoneManager;

pub struct SingleZoneManager {}

impl SingleZoneManager {
	/// Returns Ok with a Position if dir can be applied to pos; Err otherwise.
	fn pos_apply_dir_single(map: &Map, pos: &Position, dir: Direction) -> Result<Position, ()> {
		match dir {
			Direction::Up => {
				if pos.y > 0 {
					Ok(Position::new(pos.y - 1, pos.x))
				} else {
					Err(())
				}
			}

			Direction::Down => {
				if pos.y < map.zone_height - 1 {
					Ok(Position::new(pos.y + 1, pos.x))
				} else {
					Err(())
				}
			}

			Direction::Right => {
				if pos.x < map.zone_width - 1 {
					Ok(Position::new(pos.y, pos.x + 1))
				} else {
					Err(())
				}
			}

			Direction::Left => {
				if pos.x > 0 {
					Ok(Position::new(pos.y, pos.x - 1))
				} else {
					Err(())
				}
			}

			Direction::UpRight => {
				if pos.y > 0 && pos.x < map.zone_width - 1 {
					Ok(Position::new(pos.y - 1, pos.x + 1))
				} else {
					Err(())
				}
			}

			Direction::UpLeft => {
				if pos.y > 0 && pos.x > 0 {
					Ok(Position::new(pos.y - 1, pos.x - 1))
				} else {
					Err(())
				}
			}

			Direction::DownRight => {
				if pos.y < map.zone_height - 1 && pos.x < map.zone_width - 1 {
					Ok(Position::new(pos.y + 1, pos.x + 1))
				} else {
					Err(())
				}
			}

			Direction::DownLeft => {
				if pos.y < map.zone_height - 1 && pos.x > 0 {
					Ok(Position::new(pos.y + 1, pos.x - 1))
				} else {
					Err(())
				}
			}
		}
	}

	fn pos_apply_dir_amount_single(
		map: &Map,
		pos: &Position,
		dir: Direction,
		amount: usize,
	) -> Result<Position, Position> {
		let mut final_pos = *pos;

		for _i in 0..amount {
			final_pos = Self::pos_apply_dir_single(map, &final_pos, dir).map_err(|()| final_pos)?;
		}

		Ok(final_pos)
	}
}

impl ZoneManager for SingleZoneManager {
	fn load_initial_zones(map: &mut Map, current_zone_id: &ZoneId) -> Result<(), String> {
		let zone = load_zone(&map_dir(&map.universe_name, &map.id), current_zone_id)?;

		map.zones.insert(*current_zone_id, zone);

		Ok(())
	}

	fn update_loaded_zones(_map: &mut Map, _current_zone_id: &ZoneId) -> Result<(), String> {
		// Since there's only one Zone, and it should already be loaded, there's nothing to do.
		Ok(())
	}

	fn pos_apply_dir(
		map: &Map,
		zone_pos: &ZonePosition,
		dir: Direction,
	) -> Result<ZonePosition, ()> {
		Self::pos_apply_dir_single(map, &zone_pos.pos, dir).map(|pos| ZonePosition {
			zone_id: zone_pos.zone_id,
			pos,
		})
	}

	fn pos_apply_dir_amount(
		map: &Map,
		zone_pos: &ZonePosition,
		dir: Direction,
		amount: usize,
	) -> Result<ZonePosition, ZonePosition> {
		Self::pos_apply_dir_amount_single(map, &zone_pos.pos, dir, amount)
			.map(|pos| ZonePosition {
				zone_id: zone_pos.zone_id,
				pos,
			})
			.map_err(|pos| ZonePosition {
				zone_id: zone_pos.zone_id,
				pos,
			})
	}
}
