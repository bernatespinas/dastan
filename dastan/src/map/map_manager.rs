use std::collections::HashMap;

use uuid::Uuid;

use crate::save_manager::map_file;

use super::{file_utils, Map, ZoneId};

#[derive(Default)]
pub struct MapManager {
	maps: HashMap<Uuid, Map>,
}

impl MapManager {
	pub fn load_map(&mut self, universe: &str, map_id: &Uuid) -> Result<(), String> {
		log::debug!("Loading map {map_id}...");

		if self.maps.contains_key(map_id) {
			log::debug!("Map {map_id} is already loaded.");
		} else {
			let map_file_path = map_file(universe, map_id);

			let map_file = file_utils::read(map_file_path, "map")?;
			let map: Map = file_utils::deserialize(&map_file, "map")?;

			self.maps.insert(*map_id, map);

			log::debug!("Map {map_id} loaded.");
		}

		Ok(())
	}

	/// Load a Map and the required Zones around a specific Zone.
	pub fn load_map_and_required_zones(
		&mut self,
		universe: &str,
		map_id: &Uuid,
		zone_id: &ZoneId,
	) -> Result<(), String> {
		self.load_map(universe, map_id)?;
		log::debug!("load_map done");

		if self.map_zones_not_loaded(self.map(map_id)) {
			// If the Map doesn't have any Zones loaded, it might (?) mean it's the first time it's being loaded and I need to load its initial Zones.
			log::debug!("calling load_initial_zones, not update_loaded_zones");

			self.map_mut(map_id).load_initial_zones(zone_id)
		} else {
			// If the Map and its Zones had already been loaded, make sure the Zones the player needs are the current ones.
			log::debug!("calling update_loaded_zones, not load_initial_zones");

			self.map_mut(map_id).update_loaded_zones(zone_id)
		}
	}

	pub fn insert_map(&mut self, map: Map) {
		log::debug!("Inserting map {}...", map.id);

		self.maps.insert(map.id, map);
	}

	pub fn save_maps(&self) -> Result<(), String> {
		for map in self.maps.values() {
			map.save()?;
		}

		Ok(())
	}

	pub fn map(&self, map_id: &Uuid) -> &Map {
		self.maps.get(map_id).unwrap()
	}

	pub fn map_mut(&mut self, map_id: &Uuid) -> &mut Map {
		self.maps.get_mut(map_id).unwrap()
	}

	pub fn map_is_loaded(&self, map_id: &Uuid) -> bool {
		self.maps.contains_key(map_id)
	}

	fn map_zones_not_loaded(&self, map: &Map) -> bool {
		map.zones.is_empty()
	}
}
