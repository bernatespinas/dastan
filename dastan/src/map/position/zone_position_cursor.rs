use direction_simple::Direction;

use crate::map::Map;

use super::ZonePosition;

pub struct ZonePosCursor {
	pub zone_pos: ZonePosition,

	initial_zone_pos: ZonePosition,
}

impl ZonePosCursor {
	pub fn new(zone_pos: ZonePosition) -> ZonePosCursor {
		ZonePosCursor {
			zone_pos,

			initial_zone_pos: zone_pos,
		}
	}

	pub fn go_right_if_possible(&mut self, map: &Map) {
		self.pos_apply_dir(map, Direction::Right);
	}

	pub fn go_left_if_possible(&mut self, map: &Map) {
		self.pos_apply_dir(map, Direction::Left);
	}

	pub fn go_down_if_possible(&mut self, map: &Map) {
		self.pos_apply_dir(map, Direction::Down);
	}

	pub fn go_up_if_possible(&mut self, map: &Map) {
		self.pos_apply_dir(map, Direction::Up);
	}

	fn pos_apply_dir(&mut self, map: &Map, direction: Direction) {
		if let Ok(zone_pos) = map.pos_apply_dir(&self.zone_pos, direction) {
			self.zone_pos = zone_pos;
		}
	}

	pub fn start_at_next_row(&mut self, map: &Map) {
		self.restore_x_coordinates();
		self.go_down_if_possible(map);
	}

	fn restore_x_coordinates(&mut self) {
		self.zone_pos.zone_id.1 = self.initial_zone_pos.zone_id.1;
		self.zone_pos.pos.x = self.initial_zone_pos.pos.x;
	}
}
