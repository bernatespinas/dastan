use std::fmt;

mod zone_position;
pub use zone_position::{ZoneId, ZonePosition};

mod map_zone_position;
pub use map_zone_position::MapZonePos;

mod zone_position_cursor;
pub use zone_position_cursor::ZonePosCursor;

#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Serialize, Deserialize)]
pub struct Position {
	pub y: u16,
	pub x: u16,
}

impl Position {
	pub fn new(y: u16, x: u16) -> Position {
		Position { y, x }
	}
}

impl fmt::Display for Position {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "({}, {})", self.y, self.x)
	}
}

impl From<(u16, u16)> for Position {
	fn from((y, x): (u16, u16)) -> Position {
		Position { y, x }
	}
}

impl From<&Position> for (u16, u16) {
	fn from(pos: &Position) -> (u16, u16) {
		(pos.x, pos.y)
	}
}

impl From<&Position> for (usize, usize) {
	fn from(pos: &Position) -> (usize, usize) {
		(pos.x as usize, pos.y as usize)
	}
}
