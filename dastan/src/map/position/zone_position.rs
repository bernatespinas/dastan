use super::Position;

pub type ZoneId = (u16, u16);

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct ZonePosition {
	pub pos: Position,
	pub zone_id: ZoneId,
}

impl ZonePosition {
	pub fn new(zone_id: ZoneId, pos: Position) -> ZonePosition {
		ZonePosition { zone_id, pos }
	}

	pub fn is_in_same_zone(&self, other: &ZonePosition) -> bool {
		self.zone_id == other.zone_id
	}

	pub fn y_coords_are_equal(&self, other: &ZonePosition) -> bool {
		self.pos.y == other.pos.y && self.zone_id.0 == other.zone_id.0
	}

	pub fn x_coords_are_equal(&self, other: &ZonePosition) -> bool {
		self.pos.x == other.pos.x && self.zone_id.1 == other.zone_id.1
	}
}
