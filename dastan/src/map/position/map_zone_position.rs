use uuid::Uuid;

use super::ZonePosition;

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct MapZonePos {
	pub map_id: Uuid,
	pub zone_pos: ZonePosition,
}
