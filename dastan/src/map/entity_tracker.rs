use std::collections::HashMap;

use uuid::Uuid;

use crate::ecs::prelude::EntityId;
use crate::map::ZonePosition;

pub type EntityZonePosTracker = EntityTracker<ZonePosition>;
pub type EntityMapTracker = EntityTracker<Uuid>;

#[derive(Serialize, Deserialize)]
pub struct EntityTracker<T>(HashMap<EntityId, T>);

impl<T> Default for EntityTracker<T> {
	fn default() -> Self {
		EntityTracker(HashMap::new())
	}
}

impl<T> EntityTracker<T> {
	pub fn get(&self, entity_id: &EntityId) -> Option<&T> {
		self.0.get(entity_id)
	}

	pub fn set(&mut self, entity_id: EntityId, t: T) {
		self.0.insert(entity_id, t);
	}

	pub fn remove(&mut self, entity_id: &EntityId) {
		self.0.remove(entity_id);
	}

	pub fn iter(&self) -> impl Iterator<Item = (&EntityId, &T)> {
		self.0.iter()
	}

	pub fn import(&mut self, other: Self) {
		self.0.extend(other.0.into_iter());
	}
}
