use std::fmt::Debug;
use std::path::{Path, PathBuf};

use serde::de::DeserializeOwned;

pub fn get_filenames_in_path(path: &PathBuf) -> Result<Vec<String>, String> {
	let save_files = std::fs::read_dir(path).map_err(|_| format!("Unable to read {path:?}."))?;

	Ok(save_files
		.filter_map(|entry| {
			entry.ok().and_then(|e| {
				e.path()
					.file_name()
					.and_then(|n| n.to_str().map(String::from))
			})
		})
		.collect::<Vec<String>>())
}

/// Move all Zone files in a Map's buffer dir to its zones dir.
pub fn replace_buffer_zones(map_dir: &Path) -> Result<(), String> {
	let zones_dir = map_dir.join("zones");
	let buffer_zones_dir = zones_dir.join("buffer");

	let buffer_zones_dir_files = get_filenames_in_path(&buffer_zones_dir)?;
	for zone_path in &buffer_zones_dir_files {
		replace_file(
			&buffer_zones_dir.join(zone_path),
			&zones_dir.join(zone_path),
			"buffer zone",
		)?;
	}

	Ok(())
}

// Generic operation.

pub fn write_file<P, C>(path: P, contents: C, tag: &str) -> Result<(), String>
where
	P: AsRef<Path> + Debug,
	C: AsRef<[u8]>,
{
	std::fs::write(&path, contents)
		.map_err(|error| format!("Error while writing {tag} file ({path:?}) ({error})."))
}

pub fn serialize<T>(value: &T, tag: &str) -> Result<String, String>
where
	T: serde::Serialize,
{
	ron::ser::to_string(value).map_err(|_| format!("Error while serializing {tag}."))
}

pub fn serialize_and_write<T, P>(value: &T, path: P, tag: &str) -> Result<(), String>
where
	T: serde::Serialize,
	P: AsRef<Path> + Debug,
{
	// debug!("serializing and writing something at {path}");

	let contents = serialize(value, tag)?;

	write_file(path, contents, tag)
}

pub fn serialize_compress_and_write<T, P>(value: &T, path: P, tag: &str) -> Result<(), String>
where
	T: serde::Serialize,
	P: AsRef<Path> + Debug,
{
	let contents = serialize(value, tag)?;

	let zstd_contents = compress(contents);

	write_file(path, zstd_contents, tag)
}

fn compress(source: String) -> Vec<u8> {
	let mut destination: Vec<u8> = Vec::new();

	zstd::stream::copy_encode(source.as_bytes(), &mut destination, 1).unwrap();

	destination
}

pub fn decompress_file<P>(path: P) -> Result<Vec<u8>, String>
where
	P: AsRef<Path> + Debug,
{
	let source =
		std::fs::File::open(&path).map_err(|_| format!("Error while opening file {path:?}."))?;
	let mut destination: Vec<u8> = Vec::new();

	zstd::stream::copy_decode(&source, &mut destination)
		.map_err(|_| format!("Error while decompressing (zstd) {path:?}."))?;

	Ok(destination)
}

pub fn read<P>(path: P, tag: &str) -> Result<String, String>
where
	P: AsRef<Path> + Debug,
{
	// debug!("reading something at {path}");

	std::fs::read_to_string(&path)
		.map_err(|error| format!("Error while reading \"{tag}\" file ({path:?}): {error}."))
}

pub fn deserialize<'a, T>(s: &'a str, tag: &str) -> Result<T, String>
where
	T: serde::Deserialize<'a>,
{
	ron::de::from_str(s).map_err(|error| format!("Error while deserializing {tag}: {error}."))
}

pub fn deserialize_owned<T>(s: &str, tag: &str) -> Result<T, String>
where
	T: serde::de::DeserializeOwned,
{
	ron::de::from_str(s).map_err(|error| format!("Error while deserializing {tag}: {error}."))
}

// pub fn deserialize_json<'a, T>(s: &'a str, tag: &str) -> Result<T, String>
// where
// 	T: serde::Deserialize<'a>,
// {
// 	serde_json::de::from_str(s).map_err(|_| format!("Error while deserializing {tag}."))
// }

pub fn deserialize_bytes<'a, T>(s: &'a [u8], tag: &str) -> Result<T, String>
where
	T: serde::Deserialize<'a>,
{
	ron::de::from_bytes(s).map_err(|_| format!("Error while deserializing {tag}."))
}

pub fn read_and_deserialize<T, P>(path: P, tag: &str) -> Result<T, String>
where
	T: DeserializeOwned,
	P: AsRef<Path> + Debug,
{
	deserialize(&read(path, tag)?, tag)
}

pub fn read_decompress_and_deserialize<T, P>(path: P, tag: &str) -> Result<T, String>
where
	T: DeserializeOwned,
	P: AsRef<Path> + Debug,
{
	deserialize_bytes(&decompress_file(path)?, tag)
}

pub fn remove_file<P>(path: P, tag: &str) -> Result<(), String>
where
	P: AsRef<Path>,
{
	std::fs::remove_file(path).map_err(|_| format!("Error while removing {tag} file."))
}

pub fn remove_dir_all<P>(path: P, tag: &str) -> Result<(), String>
where
	P: AsRef<Path>,
{
	std::fs::remove_dir_all(path).map_err(|_| format!("Error while removing {tag} directory."))
}

pub fn replace_file(src: &PathBuf, dest: &PathBuf, tag: &str) -> Result<(), String> {
	std::fs::rename(src, dest)
		.map_err(|_| format!("Error while renaming src {src:?} file to {dest:?} ({tag})."))
}

pub fn create_dir_all<P>(path: P, tag: &str) -> Result<(), String>
where
	P: AsRef<Path>,
{
	std::fs::create_dir_all(path)
		.map_err(|_| format!("Error while creating {tag} dir and subdirs."))
}
