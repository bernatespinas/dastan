use std::{
	collections::HashMap,
	path::{Path, PathBuf},
};

use uuid::Uuid;

use crate::{
	file_utils,
	map::{Map, Zone, ZoneId},
};

const SAVES_ROOT_DIR_PATH: &str = "saves";

fn universes_dir() -> PathBuf {
	PathBuf::from(SAVES_ROOT_DIR_PATH).join("universes")
}

pub fn initialize_save_data() -> Result<(), String> {
	std::fs::create_dir_all(universes_dir())
		.map_err(|err| format!("Unable to create universes dir: {err}."))
}

pub fn universe_dir(universe: &str) -> PathBuf {
	universes_dir().join(universe)
}

pub fn universe_file(universe: &str) -> PathBuf {
	universe_dir(universe).join("universe")
}

pub fn list_universes() -> Result<Vec<String>, String> {
	file_utils::get_filenames_in_path(&universes_dir())
}

pub fn map_dir(universe: &str, map_id: &Uuid) -> PathBuf {
	universe_dir(universe).join(map_id.to_string())
}

pub fn map_file(universe: &str, map_id: &Uuid) -> PathBuf {
	map_dir(universe, map_id).join("map")
}

/// Read a compressed Zone file, decompress it, and deserialize it.
pub fn load_zone(map_dir: &Path, zone_id: &ZoneId) -> Result<Zone, String> {
	let zstd_file_path = map_dir
		.join("zones")
		.join(format!("{}_{}", zone_id.0, zone_id.1));

	file_utils::read_decompress_and_deserialize(zstd_file_path, &format!("zone {zone_id:?}"))
}

// TODO Use threads?
pub fn save_zones_in_buffer_dir(map: &Map, zones: &HashMap<ZoneId, Zone>) -> Result<(), String> {
	let zones_buffer_dir = map_dir(map.universe_name(), map.id())
		.join("zones")
		.join("buffer");

	file_utils::create_dir_all(&zones_buffer_dir, "map and zones")?;

	for (zone_id, zone) in zones {
		file_utils::serialize_compress_and_write(
			zone,
			zones_buffer_dir.join(format!("{}_{}", zone_id.0, zone_id.1)),
			&format!("zone {zone_id:?}"),
		)?;
	}

	Ok(())
}

pub fn save_zone(universe: &str, map_id: &Uuid, zone_id: &ZoneId, zone: &Zone) {
	let zones_buffer_dir = map_dir(universe, map_id).join("zones");

	file_utils::create_dir_all(&zones_buffer_dir, "map and zones").unwrap();

	file_utils::serialize_compress_and_write(
		zone,
		zones_buffer_dir.join(format!("{}_{}", zone_id.0, zone_id.1)),
		&format!("zone {zone_id:?}"),
	)
	.unwrap();
}
