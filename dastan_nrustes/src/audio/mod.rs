use std::path::PathBuf;

mod audio_players;
use audio_players::{PlayAudio, RodioAudioQueue, VoidAudioQueue};

pub struct AudioSystem {
	audio_queue: Box<dyn PlayAudio>,

	sounds_dir_path: PathBuf,
}

impl AudioSystem {
	pub fn no_audio() -> Self {
		AudioSystem {
			audio_queue: Box::new(VoidAudioQueue::default()),
			sounds_dir_path: PathBuf::new(),
		}
	}

	pub fn rodio(sounds_dir_path: PathBuf) -> Self {
		AudioSystem {
			audio_queue: Box::new(RodioAudioQueue::new()),
			sounds_dir_path,
		}
	}

	pub fn play_background(&mut self, file_name: &str) -> Result<(), String> {
		self.audio_queue
			.play_background(&self.sounds_dir_path.join("ambiance").join(file_name))
	}

	pub fn play_short_sound(&mut self, file_name: &str) -> Result<(), String> {
		self.audio_queue
			.play_short_sound(&self.sounds_dir_path.join(file_name))
	}

	pub fn stop(&mut self) -> Result<(), String> {
		self.audio_queue.stop()
	}
}
