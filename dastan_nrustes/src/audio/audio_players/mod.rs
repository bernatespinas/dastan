use std::path::PathBuf;

mod rodio_audio_queue;
pub use rodio_audio_queue::RodioAudioQueue;

mod void_audio_queue;
pub use void_audio_queue::VoidAudioQueue;

pub trait PlayAudio {
	/// Plays a sound in the background. Stops any other background sound which was being played.
	fn play_background(&mut self, file_path: &PathBuf) -> Result<(), String>;

	/// Plays a short sound. Does not interrupt anything.
	fn play_short_sound(&mut self, file_path: &PathBuf) -> Result<(), String>;

	/// Stops every sound being played and clears any queued sound.
	fn stop(&mut self) -> Result<(), String>;
}
