use std::path::PathBuf;

use super::PlayAudio;

#[derive(Default)]
pub struct VoidAudioQueue {}

impl PlayAudio for VoidAudioQueue {
	fn play_background(&mut self, _file_path: &PathBuf) -> Result<(), String> {
		Ok(())
	}

	fn play_short_sound(&mut self, _file_path: &PathBuf) -> Result<(), String> {
		Ok(())
	}

	fn stop(&mut self) -> Result<(), String> {
		Ok(())
	}
}
