use std::{fs::File, io::BufReader, path::PathBuf};

use rodio::{source::Source, Decoder, OutputStream, OutputStreamHandle, Sink};

use super::PlayAudio;

pub struct RodioAudioQueue {
	/// A `Sink` which will be used to play sound files which are meant to be background sounds.
	background: Sink,

	/// A `Sink` which is used to play short sound files.
	sound: Sink,

	/// The `OutputStream` given by `rodio`. I need to keep it here to avoid it from being dropped - otherwise, I won't be able to play sounds.
	_stream: OutputStream,

	/// The `OutputStreamHandle` given by `rodio`. I need to keep it here to avoid it from being dropped - otherwise, I won't be able to play sounds.
	_stream_handle: OutputStreamHandle,
}

impl RodioAudioQueue {
	pub fn new() -> RodioAudioQueue {
		let (_stream, stream_handle) = OutputStream::try_default().unwrap();

		let background = Sink::try_new(&stream_handle).unwrap();
		let sound = Sink::try_new(&stream_handle).unwrap();

		RodioAudioQueue {
			background,
			sound,
			_stream,
			_stream_handle: stream_handle,
		}
	}

	fn load_file(file_path: &PathBuf) -> File {
		File::open(file_path).unwrap()
	}

	fn decoder_from_path(file_path: &PathBuf) -> Decoder<BufReader<File>> {
		let file = RodioAudioQueue::load_file(file_path);

		Decoder::new(BufReader::new(file)).unwrap()
	}
}

impl PlayAudio for RodioAudioQueue {
	fn play_background(&mut self, file_path: &PathBuf) -> Result<(), String> {
		let source = RodioAudioQueue::decoder_from_path(file_path).stoppable();

		self.background.stop();
		self.background.append(source);
		self.background.play();

		Ok(())
	}
	fn play_short_sound(&mut self, file_path: &PathBuf) -> Result<(), String> {
		let source = RodioAudioQueue::decoder_from_path(file_path);
		self.sound.append(source);
		self.sound.play();

		Ok(())
	}

	fn stop(&mut self) -> Result<(), String> {
		self.background.pause();
		self.background.stop();

		Ok(())
	}
}
