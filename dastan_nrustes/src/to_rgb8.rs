use nrustes::prelude::RGB8;

use dastan::color::Color;

pub trait ToRGB8 {
	fn to_rgb8(self) -> RGB8;
}

impl ToRGB8 for Color {
	fn to_rgb8(self) -> RGB8 {
		match self {
			Color::Red => RGB8::new(255, 25, 25),
			Color::Orange => RGB8::new(255, 140, 25),
			Color::Yellow => RGB8::new(255, 255, 25),
			Color::LGreen => RGB8::new(25, 255, 25),
			Color::DGreen => RGB8::new(8, 82, 8),
			Color::LBlue => RGB8::new(25, 255, 255),
			Color::DBlue => RGB8::new(25, 25, 255),
			Color::LPurple => RGB8::new(179, 25, 255),
			Color::DPurple => RGB8::new(102, 0, 204),
			Color::LPink => RGB8::new(255, 25, 25),
			Color::DPink => RGB8::new(153, 0, 153),
			Color::LBrown => RGB8::new(153, 102, 0),
			Color::DBrown => RGB8::new(102, 68, 0),
			Color::LGrey => RGB8::new(186, 186, 186),
			Color::DGrey => RGB8::new(115, 115, 115),
			Color::Black => RGB8::new(0, 0, 0),
			Color::White => RGB8::new(255, 255, 255),
		}
	}
}
