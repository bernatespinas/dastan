use std::{
	error::Error,
	fmt::Debug,
	fs::read_to_string,
	path::{Path, PathBuf},
};

use nrustes::nrustes_config::NrustesConfig;

use dastan::file_utils;

#[derive(Debug, Serialize, Deserialize)]
pub struct DastanConfig {
	/// The path of the directory which contains resources for the game, such as audio files, sprites, fonts...
	pub resources_dir_path: PathBuf,

	/// The path of the configuration file for `nrustes`.
	pub nrustes_config_file_path: PathBuf,

	pub enable_sound: bool,
}

impl DastanConfig {
	pub fn load<T: AsRef<Path> + Debug>(path: T) -> Result<DastanConfig, String> {
		let dastan_config_file = file_utils::read(path, "Dastan config")?;
		let dastan_config = file_utils::deserialize(&dastan_config_file, "Dastan config")?;

		Ok(dastan_config)
	}

	pub fn load_nrustes_config(&self) -> Result<NrustesConfig, Box<dyn Error>> {
		let nrustes_config_file = read_to_string(&self.nrustes_config_file_path)?;

		ron::de::from_str(&nrustes_config_file).map_err(|error| error.into())
	}

	pub fn texture_atlases_dir_path(&self) -> PathBuf {
		self.resources_dir_path.join("texture_atlases")
	}
}
