use nrustes::prelude::*;

pub fn about(context: &mut impl Context) {
	let mut win: Window = WindowBuilder::full("Dastan -- About", context).build();

	win.print_accent((4, 1), "Name: Dastan");
	win.print_accent((6, 1), "Developer: Elieroz");
	win.print_accent((8, 1), "Version: 22.02.18");
	win.print_accent((10, 1), ";)");

	loop {
		match context.input_key().gamify() {
			Key::Back => break,
			_ => continue,
		};
	}
}
