use nrustes::prelude::*;

use dastan::game::Game;
use dastan::generator::GeneratorBundle;

mod load_universe_view;

mod about;

pub enum MainMenuOutput {
	Play(Game),
	Exit,
}

#[derive(Debug, EnumIter)]
enum MainMenuItem {
	Play,
	About,
}

impl std::fmt::Display for MainMenuItem {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{self:?}")
	}
}

pub fn main_menu(gen_bundle: &GeneratorBundle, context: &mut impl Context) -> MainMenuOutput {
	let mut scrollable: Scrollable<MainMenuItem> = WindowBuilder::full("Dastan", context)
		.scrollable_builder()
		.with_no_prefix()
		.with_items(MainMenuItem::iter().collect())
		.build();

	loop {
		match scrollable.handle_input_and_redraw(context).unwrap() {
			Key::Select => match scrollable.hovered() {
				Some(MainMenuItem::Play) => match choose_universe(gen_bundle, context) {
					Some(Ok(universe)) => return MainMenuOutput::Play(universe),
					Some(Err(message)) => {
						ErrorPopup::centered_in_screen(message, context)
							.show(&mut scrollable, context);
					}
					None => {}
				},
				Some(MainMenuItem::About) => about::about(context),
				_ => continue,
			},
			Key::Back => return MainMenuOutput::Exit,
			_ => continue,
		};
	}
}

fn choose_universe(
	gen_bundle: &GeneratorBundle,
	context: &mut impl Context,
) -> Option<Result<Game, String>> {
	match load_universe_view::choose_universe(gen_bundle, context) {
		Ok(Some(universe)) => Some(Ok(universe)),
		Ok(None) => None,
		Err(message) => Some(Err(message)),
	}
}
