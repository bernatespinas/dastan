use nrustes::prelude::*;

use dastan::game::Game;
use dastan::generator::GeneratorBundle;
use dastan::{file_utils, save_manager};

mod create_universe_view;

#[derive(Clone)]
pub enum Action {
	Load(String),
	Create,
	Delete,
	Scroll,
}

#[derive(Draw, DoubleViewHolder)]
pub struct ChooseUnvierseView<'a> {
	#[draw]
	view: DoubleViewState<Scrollable<String>, Window, Action>,

	gen_bundle: &'a GeneratorBundle,
}

impl<'a> ChooseUnvierseView<'a> {
	fn new(gen_bundle: &'a GeneratorBundle, context: &mut impl Context) -> Result<Self, String> {
		let scrollable: Scrollable<String> =
			WindowBuilder::left("Dastan - Play -- Universe", context)
				.scrollable_builder()
				.with_items(save_manager::list_universes()?)
				.build();

		let window: Window = WindowBuilder::right("Properties", context).build();

		Ok(ChooseUnvierseView {
			view: DoubleViewState::new(scrollable, window),

			gen_bundle,
		})
	}
}

impl DoubleView for ChooseUnvierseView<'_> {
	type Output = Game;

	fn initialize(&mut self) {
		self.scrollable_mut()
			.bind_movement_shortcuts(Action::Scroll);

		self.scrollable_mut().bind_shortcut(
			Key::Char('n'),
			Shortcut::new(Action::Create).with_label("New").permanent(),
		);

		self.update();
	}

	fn handle_action(
		&mut self,
		action: &Self::Action,
		context: &mut impl Context,
	) -> Option<Result<Self::Output, String>> {
		match action {
			Action::Load(hovered) => Some(load_universe(hovered, self, context)),
			Action::Create => {
				match create_universe_view::create_universe(self.gen_bundle, context) {
					Ok(()) => {
						let string_options = match save_manager::list_universes() {
							Ok(ok) => ok,
							Err(msg) => return Some(Err(msg)),
						};

						self.scrollable_mut()
							.container_mut()
							.change_items(string_options);

						self.update();
						self.schedule_draw();

						None
					}
					Err(message) => Some(Err(message)),
				}
			}
			Action::Delete => {
				let hovered = self.scrollable().container().hovered().cloned().unwrap();

				let delete_game = || -> Result<(), String> {
					file_utils::remove_dir_all(save_manager::universe_dir(&hovered), "universe")
				};

				match ProgressPopup::centered_in_container(
					"Deleting",
					self.scrollable().window(),
					context,
				)
				.process(delete_game, self, context)
				{
					Ok(()) => {
						self.scrollable_mut().container_mut().delete_hovered();
						self.update();

						None
					}
					Err(message) => Some(Err(message)),
				}
			}
			Action::Scroll => {
				self.update();

				None
			}
		}
	}
}

impl<'a> ChooseUnvierseView<'a> {
	fn update(&mut self) {
		self.window_mut().clear_shortcuts();
		self.window_mut().delete_content();

		self.window_mut().print_accent((3, 1), "TODO");

		if let Some(hovered) = self.scrollable().container().hovered().cloned() {
			self.window_mut().bind_shortcut(
				Key::Char('l'),
				Shortcut::new(Action::Load(hovered)).with_label("Load"),
			);

			self.window_mut().bind_shortcut(
				Key::Char('d'),
				Shortcut::new(Action::Delete).with_label("Delete"),
			);
		}

		self.window_mut().schedule_draw();
	}
}

fn load_universe<D: Draw + Darken>(
	game_name: &str,
	view: &mut D,
	context: &mut impl Context,
) -> Result<Game, String> {
	let loading_game = || Game::load(game_name);

	ProgressPopup::centered_in_screen("Loading universe", context).process(
		loading_game,
		view,
		context,
	)
}

pub fn choose_universe(
	gen_bundle: &GeneratorBundle,
	context: &mut impl Context,
) -> Result<Option<Game>, String> {
	match ChooseUnvierseView::new(gen_bundle, context)?.input_loop(context) {
		None => Ok(None),
		some => Ok(some),
	}
}
