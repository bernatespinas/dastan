use std::collections::HashMap;

use nrustes::form::FieldType;
use nrustes::prelude::*;

use dastan::game::generate_universe;
use dastan::game::player_info::PlayerInfo;
use dastan::generator::GeneratorBundle;
use dastan::list_enum::ListEnum;
use dastan::map::{MapInfo, MapMode};
use dastan::map_generator::Earth2;
use dastan::size::Size;
use dastan::{file_utils, save_manager};

#[derive(Clone, Copy)]
enum Action {
	EnterValue,
	Submit,
	Scroll,
}

#[derive(Draw, DoubleViewHolder)]
struct CreateUniverseView {
	#[draw]
	view: DoubleViewState<Form, Window, Action>,
}

impl CreateUniverseView {
	fn new(context: &mut impl Context) -> Self {
		let form: Form = Form::new(WindowBuilder::left("Create your universe!", context).build())
			.with_required_field("Name".to_string(), FieldType::String)
			.with_required_field("Size".to_string(), FieldType::Choice(Size::list_string()));

		let window: Window = WindowBuilder::right("Details", context).build();

		CreateUniverseView {
			view: DoubleViewState::new(form, window),
		}
	}
}

impl DoubleView for CreateUniverseView {
	type Output = MapInfo;

	fn initialize(&mut self) {
		self.form_mut().bind_movement_shortcuts(Action::Scroll);

		self.form_mut()
			.bind_shortcut(Key::Select, Shortcut::new(Action::EnterValue).permanent());

		self.form_mut().bind_shortcut(
			Key::Char('s'),
			Shortcut::new(Action::Submit)
				.with_label("Submit")
				.permanent(),
		);

		self.update();
	}

	fn handle_action(
		&mut self,
		action: &Self::Action,
		context: &mut impl Context,
	) -> Option<Result<Self::Output, String>> {
		match action {
			Action::EnterValue => {
				let field = self.form().container().hovered_field().unwrap().clone();

				match field.field_type {
					FieldType::Choice(choices) => {
						let choice_index = ContextMenu::new(
							&field.name,
							choices.clone(),
							self.form().window(),
							context,
						)
						.show(self, context)?;

						match self
							.form_mut()
							.container_mut()
							.set_hovered_field_choice(choice_index)
						{
							Ok(()) => None,
							Err(message) => Some(Err(message.to_string())),
						}
					}
					_ => {
						let input = TextInputModal::new(&field.name, self.form().window(), context)
							.show(self, context)?;

						match self
							.form_mut()
							.container_mut()
							.set_hovered_field_value(input)
						{
							Ok(()) => None,
							Err(message) => Some(Err(message.to_string())),
						}
					}
				}
			}
			Action::Submit => {
				let mut form = match self.form().container().export() {
					Ok(f) => f,
					Err(message) => return Some(Err(message)),
				};

				let map_size = match form.get("Size").unwrap().as_str() {
					"Tiny" => Size::Tiny,
					"Small" => Size::Small,
					"Medium" => Size::Medium,
					"Big" => Size::Big,
					"Huge" => Size::Huge,
					_ => unreachable!(),
				};

				let (height, width) = size_to_map_height_width(map_size);

				Some(Ok(MapInfo {
					universe_name: form.remove("Name").unwrap(),
					name: "Space".to_string(),

					map_mode: MapMode::ZonedLooping,

					height,
					width,

					zone_height: u8::MAX as u16,
					zone_width: u8::MAX as u16,

					entry_points: HashMap::new(),
				}))
			}
			Action::Scroll => {
				self.update();

				None
			}
		}
	}
}

impl CreateUniverseView {
	fn update(&mut self) {
		let hovered_field = self
			.form()
			.container()
			.hovered_field()
			.unwrap()
			.name
			.clone();

		self.window_mut().change_title(hovered_field);

		self.window_mut().schedule_draw();
	}
}

pub fn create_universe(
	gen_bundle: &GeneratorBundle,
	context: &mut impl Context,
) -> Result<(), String> {
	let mut create_universe_view = CreateUniverseView::new(context);

	let Some(universe_info) = create_universe_view.input_loop(context) else {
		return Ok(());
	};

	let player_info = PlayerInfo {
		name: universe_info.universe_name.clone(),
		race: dastan::race::Race::Elf,
	};

	let creating_universe = || -> Result<(), String> {
		let game = generate_universe(&universe_info, &player_info, &Earth2 {}, gen_bundle)?;

		game.save_universe()
	};

	// TODO Use `inspect_err` (or its equivalent) once it's available.
	// ProgressPopup::centered_in_screen("Creating universe", context)
	// 	.process(creating_universe, &mut create_universe_view, context)
	// 	.inspect_err(|_| {
	// 		file_utils::remove_dir_all(
	// 			save_manager::universe_dir(&universe_info.universe_name),
	// 			"universe",
	// 		)
	// 		.unwrap()
	// 	})

	let create_result = ProgressPopup::centered_in_screen("Creating universe", context).process(
		creating_universe,
		&mut create_universe_view,
		context,
	);

	if create_result.is_err() {
		file_utils::remove_dir_all(
			save_manager::universe_dir(&universe_info.universe_name),
			"universe",
		)
		.or_else(|_| create_result.clone())?;
	}

	create_result
}

fn size_to_map_height_width(size: Size) -> (u16, u16) {
	match size {
		Size::Tiny => (48, 48),
		Size::Small => (64, 64),
		Size::Medium => (80, 80),
		Size::Big => (96, 96),
		Size::Huge => (112, 112),
	}
}
