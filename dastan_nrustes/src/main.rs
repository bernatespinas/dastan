// #![feature(option_result_contains, result_option_inspect)]

#[macro_use]
extern crate serde;

use nrustes::context::Context;

use dastan::generator::GeneratorBundle;
use dastan::save_manager;

mod init;

mod audio;

mod game_logic;

mod main_menu;
use main_menu::MainMenuOutput;

mod to_rgb8;

mod dastan_config;
use dastan_config::DastanConfig;

fn main() -> Result<(), String> {
	init::initialize_log_system();

	init::log_dastan_mem_info();

	save_manager::initialize_save_data()?;

	let dastan_config = DastanConfig::load("dastan_config.ron")?;
	let nrustes_config = dastan_config
		.load_nrustes_config()
		.expect("Unable to load NrustesConfig");

	init::initialize_context_and_show_main_menu(dastan_config, nrustes_config)
}

fn main_menu_loop(context: &mut impl Context) -> Result<(), String> {
	let gen_bundle = GeneratorBundle::load();

	loop {
		match main_menu::main_menu(&gen_bundle, context) {
			MainMenuOutput::Play(universe) => match game_logic::main_loop(universe, context) {
				Ok(_game_action) => {}
				Err(message) => todo!("{message}"),
			},
			MainMenuOutput::Exit => return Ok(()),
		}
	}
}
