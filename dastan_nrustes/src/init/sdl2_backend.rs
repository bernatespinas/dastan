use nrustes::{
	nrustes_config::NrustesConfig,
	prelude::create_context_sdl2,
	texture_atlas::{LoadTextureAtlas, TextureAtlasLoader},
};

use crate::dastan_config::DastanConfig;

pub fn initialize_context_and_show_main_menu(
	dastan_config: DastanConfig,
	nrustes_config: NrustesConfig,
) -> Result<(), String> {
	create_context_sdl2!(
		context,
		texture_creator,
		nrustes_config: nrustes_config,
		window_title: "Dastan",
		window_mode: "fullscreen_windowed"
	);

	let texture_atlas_loader = TextureAtlasLoader::new(dastan_config.texture_atlases_dir_path());

	for graphics in ["morning", "afternoon", "evening", "night"] {
		texture_atlas_loader.load_graphics(
			graphics,
			&format!("{graphics}.png"),
			&format!("{graphics}.ron"),
			(64, 64),
			&mut context,
			&texture_creator,
		)?;
	}

	texture_atlas_loader.load_font(
		"font",
		"font.bmp",
		"font.ron",
		(14, 14),
		&mut context,
		&texture_creator,
	)?;

	context
		.texture_atlas_font
		.set_current_texture("font".to_string());

	crate::main_menu_loop(&mut context)
}
