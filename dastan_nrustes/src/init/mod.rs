use std::fs::File;

use dastan::simplelog::{Config, LevelFilter, WriteLogger};

#[cfg(feature = "with_termion")]
mod termion_backend;
#[cfg(feature = "with_termion")]
pub use termion_backend::initialize_context_and_show_main_menu;

#[cfg(feature = "with_crossterm")]
mod crossterm_backend;
#[cfg(feature = "with_crossterm")]
pub use crossterm_backend::initialize_context_and_show_main_menu;

#[cfg(feature = "with_sdl2")]
mod sdl2_backend;
#[cfg(feature = "with_sdl2")]
pub use sdl2_backend::initialize_context_and_show_main_menu;

pub fn initialize_log_system() {
	WriteLogger::init(
		LevelFilter::Debug,
		Config::default(),
		File::create("dastan.log").unwrap(),
	)
	.unwrap();

	log::debug!("Log system successfully initialized!");
}

pub fn log_dastan_mem_info() {
	log::debug!("Tile:");
	log::debug!("    Tile: {}.", std::mem::size_of::<dastan::map::Tile>());
	log::debug!(
		"    Creature: {}.",
		std::mem::size_of::<dastan::ecs::prelude::Creature>()
	);
	log::debug!(
		"    Prop: {}.",
		std::mem::size_of::<dastan::ecs::prelude::Prop>()
	);
	log::debug!(
		"    Item: {}.",
		std::mem::size_of::<dastan::ecs::prelude::Item>()
	);
	log::debug!(
		"    Terrain: {}.",
		std::mem::size_of::<dastan::terrain::Terrain>()
	);
	log::debug!("Components:");
	log::debug!(
		"    ZoneComponents: {}.",
		std::mem::size_of::<dastan::ecs::ZoneComponents>()
	);
	log::debug!(
		"    Inventory: {}.",
		std::mem::size_of::<dastan::ecs_components::Inventory>()
	);
	log::debug!(
		"    RecipeBook: {}.",
		std::mem::size_of::<dastan::ecs_components::RecipeBook>()
	);
	log::debug!(
		"        Recipe: {}.",
		std::mem::size_of::<dastan::ecs_components::Recipe>()
	);
	log::debug!(
		"        Blueprint: {}.",
		std::mem::size_of::<dastan::ecs_components::Blueprint>()
	);
	log::debug!(
		"    ComputerComponent: {}.",
		std::mem::size_of::<dastan::ecs_components::ComputerComponent>()
	);
	log::debug!(
		"    PassageComponent: {}.",
		std::mem::size_of::<dastan::ecs_components::PassageComponent>()
	);
	log::debug!(
		"    TeleportComponent: {}.",
		std::mem::size_of::<dastan::ecs_components::TeleportComponent>()
	);
	log::debug!(
		"    HumanoidBody: {}.",
		std::mem::size_of::<dastan::ecs_components::body::HumanoidBody>()
	);
	log::debug!(
		"    TargetComponent: {}.",
		std::mem::size_of::<dastan::ecs_components::TargetComponent>()
	);
	log::debug!(
		"    HumanoidEquipment: {}.",
		std::mem::size_of::<dastan::ecs_components::HumanoidEquipment>()
	);
	log::debug!(
		"    FoodComponent: {}.",
		std::mem::size_of::<dastan::ecs_components::FoodComponent>()
	);
	log::debug!(
		"    CorpseComponent: {}.",
		std::mem::size_of::<dastan::ecs_components::CorpseComponent>()
	);
}
