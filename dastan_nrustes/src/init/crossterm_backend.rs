use nrustes::nrustes_config::NrustesConfig;

use crate::dastan_config::DastanConfig;

pub fn initialize_context_and_show_main_menu(
	_dastan_config: DastanConfig,
	nrustes_config: NrustesConfig,
) -> Result<(), String> {
	let mut context = nrustes::backends::ContextCrossterm::new(nrustes_config);

	crate::main_menu_loop(&mut context)
}
