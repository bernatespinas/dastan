use nrustes::prelude::RGB8;

use dastan::color::Color;
use dastan::time::{PartOfDay, Time};

pub fn choose_palette(time: &Time) -> fn(color: Color) -> RGB8 {
	match time.part_of_day() {
		PartOfDay::Morning => morning,
		PartOfDay::Afternoon => afternoon,
		PartOfDay::Evening => evening,
		PartOfDay::Night => night,
	}
}

fn morning(color: Color) -> RGB8 {
	match color {
		Color::Red => RGB8::new(255, 25, 25),
		Color::Orange => RGB8::new(255, 140, 25),
		Color::Yellow => RGB8::new(255, 255, 25),
		Color::LGreen => RGB8::new(25, 255, 25),
		Color::DGreen => RGB8::new(8, 82, 8),
		Color::LBlue => RGB8::new(25, 255, 255),
		Color::DBlue => RGB8::new(25, 25, 255),
		Color::LPurple => RGB8::new(179, 25, 255),
		Color::DPurple => RGB8::new(102, 0, 204),
		Color::LPink => RGB8::new(255, 25, 25),
		Color::DPink => RGB8::new(153, 0, 153),
		Color::LBrown => RGB8::new(153, 102, 0),
		Color::DBrown => RGB8::new(102, 68, 0),
		Color::LGrey => RGB8::new(186, 186, 186),
		Color::DGrey => RGB8::new(115, 115, 115),
		Color::Black => RGB8::new(0, 0, 0),
		Color::White => RGB8::new(255, 255, 255),
	}
}

fn afternoon(color: Color) -> RGB8 {
	match color {
		Color::Red => RGB8::new(255, 25, 2),
		Color::Orange => RGB8::new(255, 140, 2),
		Color::Yellow => RGB8::new(255, 255, 2),
		Color::LGreen => RGB8::new(25, 255, 2),
		Color::DGreen => RGB8::new(8, 82, 0),
		Color::LBlue => RGB8::new(25, 255, 25),
		Color::DBlue => RGB8::new(25, 25, 25),
		Color::LPurple => RGB8::new(179, 25, 25),
		Color::DPurple => RGB8::new(102, 0, 20),
		Color::LPink => RGB8::new(255, 25, 2),
		Color::DPink => RGB8::new(153, 0, 15),
		Color::LBrown => RGB8::new(153, 102, 0),
		Color::DBrown => RGB8::new(102, 68, 0),
		Color::LGrey => RGB8::new(186, 186, 18),
		Color::DGrey => RGB8::new(115, 115, 11),
		Color::Black => RGB8::new(0, 0, 0),
		Color::White => RGB8::new(255, 255, 255),
	}
}

fn evening(color: Color) -> RGB8 {
	match color {
		Color::Red => RGB8::new(25, 2, 25),
		Color::Orange => RGB8::new(25, 14, 25),
		Color::Yellow => RGB8::new(25, 25, 25),
		Color::LGreen => RGB8::new(2, 25, 25),
		Color::DGreen => RGB8::new(0, 8, 8),
		Color::LBlue => RGB8::new(2, 25, 255),
		Color::DBlue => RGB8::new(2, 2, 255),
		Color::LPurple => RGB8::new(17, 2, 255),
		Color::DPurple => RGB8::new(10, 0, 204),
		Color::LPink => RGB8::new(25, 2, 25),
		Color::DPink => RGB8::new(15, 0, 153),
		Color::LBrown => RGB8::new(15, 10, 0),
		Color::DBrown => RGB8::new(10, 6, 0),
		Color::LGrey => RGB8::new(18, 18, 186),
		Color::DGrey => RGB8::new(11, 11, 115),
		Color::Black => RGB8::new(0, 0, 0),
		Color::White => RGB8::new(255, 255, 255),
	}
}

fn night(color: Color) -> RGB8 {
	match color {
		Color::Red => RGB8::new(25, 2, 25),
		Color::Orange => RGB8::new(25, 14, 25),
		Color::Yellow => RGB8::new(25, 25, 25),
		Color::LGreen => RGB8::new(2, 25, 25),
		Color::DGreen => RGB8::new(0, 8, 8),
		Color::LBlue => RGB8::new(2, 25, 255),
		Color::DBlue => RGB8::new(2, 2, 255),
		Color::LPurple => RGB8::new(17, 2, 255),
		Color::DPurple => RGB8::new(10, 0, 204),
		Color::LPink => RGB8::new(25, 2, 25),
		Color::DPink => RGB8::new(15, 0, 153),
		Color::LBrown => RGB8::new(15, 10, 0),
		Color::DBrown => RGB8::new(10, 6, 0),
		Color::LGrey => RGB8::new(18, 18, 186),
		Color::DGrey => RGB8::new(11, 11, 115),
		Color::Black => RGB8::new(0, 0, 0),
		Color::White => RGB8::new(255, 255, 255),
	}
}
// 		fn rain(&self, color: Color) -> RGB {
// 			match color {
// 				Color::Red			=> RGB::new(255, 25, 25),
// 				Color::Orange		=> RGB::new(255, 140, 25),
// 				Color::Yellow		=> RGB::new(255, 255, 25),
// 				Color::LGreen		=> RGB::new(25, 255, 25),
// 				Color::DGreen		=> RGB::new(8, 82, 8),
// 				Color::LBlue		=> RGB::new(25, 255, 255),
// 				Color::DBlue		=> RGB::new(25, 25, 255),
// 				Color::LPurple		=> RGB::new(179, 25, 255),
// 				Color::DPurple		=> RGB::new(102, 0, 204),
// 				Color::LPink		=> RGB::new(255, 25, 25),
// 				Color::DPink		=> RGB::new(153, 0, 153),
// 				Color::LBrown		=> RGB::new(153, 102, 0),
// 				Color::DBrown		=> RGB::new(102, 68, 0),
// 				Color::LGrey		=> RGB::new(186, 186, 186),
// 				Color::DGrey		=> RGB::new(115, 115, 115),
// 			}
// 		}
// 		fn cloud(&self, color: Color) -> RGB::new {
// 			match color {
// 				Color::Red			=> RGB::new(255, 25, 25),
// 				Color::Orange		=> RGB::new(255, 140, 25),
// 				Color::Yellow		=> RGB::new(255, 255, 25),
// 				Color::LGreen		=> RGB::new(25, 255, 25),
// 				Color::DGreen		=> RGB::new(8, 82, 8),
// 				Color::LBlue		=> RGB::new(25, 255, 255),
// 				Color::DBlue		=> RGB::new(25, 25, 255),
// 				Color::LPurple		=> RGB::new(179, 25, 255),
// 				Color::DPurple		=> RGB::new(102, 0, 204),
// 				Color::LPink		=> RGB::new(255, 25, 25),
// 				Color::DPink		=> RGB::new(153, 0, 153),
// 				Color::LBrown		=> RGB::new(153, 102, 0),
// 				Color::DBrown		=> RGB::new(102, 68, 0),
// 				Color::LGrey		=> RGB::new(186, 186, 186),
// 				Color::DGrey		=> RGB::new(115, 115, 115),
// 			}
// 		}
