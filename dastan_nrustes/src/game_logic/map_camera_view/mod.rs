use std::collections::HashSet;

use direction_simple::Direction;

use nrustes::prelude::*;

use dastan::color::Color;
use dastan::map::{Map, Position, ZonePosition};
use dastan::terrain::TerrainGenerator;

use crate::to_rgb8::ToRGB8;

// TODO Placeholder.
const TARGET_PLACEHOLDER_SPRITE: &str = "Target cursor";

mod tile_drawer;
pub use tile_drawer::FontSprites;
use tile_drawer::{FontTileDrawer, GraphicsTileDrawer};

pub mod color_palette;

mod map_scroll_grid;
use map_scroll_grid::MapScrollGrid;

pub struct MapCameraView {
	pad: Pad<ZonePosition>,

	font_tile_drawer: FontTileDrawer,
	graphics_tile_drawer: GraphicsTileDrawer,

	/// The `ZonePosition`s of the `Tile`s which have been modified and might
	/// need to be redrawn.
	tiles_pending_redraw: HashSet<ZonePosition>,
}

impl MapCameraView {
	fn new(window: Window, map: &Map, top_left: ZonePosition) -> Self {
		let color_palette = color_palette::choose_palette(&map.time);

		let font_sprites = FontSprites::load();

		Self {
			pad: Pad::new(window, top_left, &MapScrollGrid(map)),

			font_tile_drawer: FontTileDrawer {
				color_palette,
				font_sprites,
			},
			graphics_tile_drawer: GraphicsTileDrawer {},

			tiles_pending_redraw: HashSet::new(),
		}
	}

	pub fn with_top_left(top_left: ZonePosition, map: &Map, context: &impl Context) -> Self {
		let window = map_camera_view_window(context);

		MapCameraView::new(window, map, top_left)
	}

	pub fn centered_at(centered_at: &ZonePosition, map: &Map, context: &impl Context) -> Self {
		let window = map_camera_view_window(context);
		let top_left = top_left_position_with_center(&window, map, centered_at);

		MapCameraView::new(window, map, top_left)
	}

	pub fn is_visible(&self, zone_pos: &ZonePosition, map: &Map) -> bool {
		let top_left = map.zone_pos_to_pos(self.pad.top_left());
		let bot_right = map.zone_pos_to_pos(self.pad.bottom_right());
		let pos = map.zone_pos_to_pos(zone_pos);

		// When bot_right.y <= top_left.y, the "horizontal map edge" has been crossed.

		let valid_y = top_left.y <= pos.y && pos.y <= bot_right.y
			|| (bot_right.y < top_left.y
				&& (top_left.y <= pos.y && pos.y <= map.height_total()
					||
					/*0 <= pos.y && */pos.y <= bot_right.y));

		let valid_x = top_left.x <= pos.x && pos.x <= bot_right.x
			|| (bot_right.x < top_left.x
				&& (top_left.x <= pos.x && pos.x <= map.width_total()
					||
					/*0 <= pos.x && */pos.x <= bot_right.x));

		valid_y && valid_x
	}

	pub fn top_left(&self) -> &ZonePosition {
		self.pad.top_left()
	}

	pub fn set_top_left(&mut self, top_left: ZonePosition, map: &Map) {
		self.pad.set_top_left(top_left, &MapScrollGrid(map));
	}

	fn tile_zone_pos_to_screen_pos(&self, map: &Map, zone_pos: &ZonePosition) -> (u16, u16) {
		let (y, x) = {
			let top_left_pos = map.zone_pos_to_pos(self.top_left());
			let other_pos = map.zone_pos_to_pos(zone_pos);

			(
				if other_pos.y >= top_left_pos.y {
					other_pos.y - top_left_pos.y
				} else {
					map.height_total() - top_left_pos.y + other_pos.y
				},
				if other_pos.x >= top_left_pos.x {
					other_pos.x - top_left_pos.x
				} else {
					map.width_total() - top_left_pos.x + other_pos.x
				},
			)
		};

		(y, x)
	}

	pub fn try_to_center_position(&mut self, zone_pos: &ZonePosition, map: &Map) {
		let new_top_left = top_left_position_with_center(self.pad.window(), map, zone_pos);

		self.pad.set_top_left(new_top_left, &MapScrollGrid(map));
	}

	pub fn pan_camera_to_keep_position_visible(
		&mut self,
		zone_pos: &ZonePosition,
		map: &Map,
	) -> bool {
		let previous_top_left = *self.pad.top_left();

		let new_top_left = get_top_left_to_keep_position_visible(
			self.top_left(),
			self.pad.bottom_right(),
			self.pad.window(),
			zone_pos,
			map,
		);

		self.set_top_left(new_top_left, map);

		previous_top_left != new_top_left
	}

	pub fn draw_map(&self, map: &Map, terrain_gen: &TerrainGenerator, context: &mut impl Context) {
		if self.map_smaller_than_view(map) {
			self.pad.erase_all(context);
		}

		if context.can_render_graphics() {
			self.pad.draw(
				&MapScrollGrid(map),
				&self.graphics_tile_drawer,
				terrain_gen,
				context,
			);
		} else {
			self.pad.draw(
				&MapScrollGrid(map),
				&self.font_tile_drawer,
				terrain_gen,
				context,
			);
		}
	}

	/// If you have to draw the whole `Map`, the `draw_map` method will be more
	/// efficient than calling `draw_tile` for every `Tile`.
	pub fn draw_tile(
		&self,
		map: &Map,
		zone_pos: &ZonePosition,
		terrain_gen: &TerrainGenerator,
		context: &mut impl Context,
	) {
		let (y, x) = self.tile_zone_pos_to_screen_pos(map, zone_pos);

		if context.can_render_graphics() {
			self.pad.draw_cell_item(
				&MapScrollGrid(map),
				zone_pos,
				&self.graphics_tile_drawer,
				y,
				x,
				terrain_gen,
				context,
			);
		} else {
			self.pad.draw_cell_item(
				&MapScrollGrid(map),
				zone_pos,
				&self.font_tile_drawer,
				y,
				x,
				terrain_gen,
				context,
			);
		}
	}

	pub fn draw_target_cursor(
		&self,
		map: &Map,
		zone_pos: &ZonePosition,
		terrain_gen: &TerrainGenerator,
		context: &mut impl Context,
	) {
		let (y, x) = self.tile_zone_pos_to_screen_pos(map, zone_pos);

		if context.can_render_graphics() {
			context.render_graphics_sprite(&TARGET_PLACEHOLDER_SPRITE.to_string(), y, x);
		} else {
			context.render_char(y, x, 'x', Color::White.to_rgb8(), Color::Black.to_rgb8());
		}
	}

	pub fn map_smaller_than_view(&self, map: &Map) -> bool {
		map.height_total() < self.pad.height() || map.width_total() < self.pad.width()
	}

	pub fn color_palette(&self) -> fn(Color) -> RGB8 {
		self.font_tile_drawer.color_palette
	}

	pub fn set_color_palette(&mut self, color_palette: fn(Color) -> RGB8) {
		self.font_tile_drawer.color_palette = color_palette;
	}

	/// Adds the `ZonePosition`s of the `Tile`s which might need to be redrawn.
	pub fn add_tiles_pending_redraw(&mut self, tiles_pending_redraw: Vec<ZonePosition>) {
		self.tiles_pending_redraw.extend(tiles_pending_redraw)
	}

	/// Consumes and returns the collection of `ZonePosition`s of the `Tile`s
	/// which have been modified and might need to be redrawn.
	pub fn tiles_pending_redraw(&mut self) -> HashSet<ZonePosition> {
		std::mem::take(&mut self.tiles_pending_redraw)
	}
}

fn map_camera_view_window(context: &impl Context) -> Window {
	let (pixel_height, pixel_width) = context.size_in_smallest_units();
	let (font_height, _) = context.font_sprite_size();
	let (graphics_height, graphics_width) = context.graphics_sprite_size();

	let panel_area_height = 10 * font_height;

	let height = (pixel_height - panel_area_height) / graphics_height;
	let width = pixel_width / graphics_width;

	WindowBuilder::new(0, 0, height, width, context).build()
}

/// Returns the `ZonePosition` which should be at the top left corner to try yo
/// keep `center` at the center.
fn top_left_position_with_center(
	window: &Window,
	map: &Map,
	center: &ZonePosition,
) -> ZonePosition {
	let (view_height, view_width) = view_dimensions(window, map);

	let half_height = view_height as usize / 2;
	let half_width = view_width as usize / 2;

	let center_up = match map.pos_apply_dir_amount(center, Direction::Up, half_height) {
		Ok(pos) | Err(pos) => pos,
	};

	let center_left = match map.pos_apply_dir_amount(center, Direction::Left, half_width) {
		Ok(pos) | Err(pos) => pos,
	};

	ZonePosition::new(
		(center_up.zone_id.0, center_left.zone_id.1),
		Position::new(center_up.pos.y, center_left.pos.x),
	)
}

/// Returns the `ZonePosition` which should be at the top left corner to keep
/// `zone_pos` visible. It pans the whole view.
fn get_top_left_to_keep_position_visible(
	top_left: &ZonePosition,
	bottom_right: &ZonePosition,
	window: &Window,
	zone_pos: &ZonePosition,
	map: &Map,
) -> ZonePosition {
	let should_move_vertically = |limit_zone_pos: &ZonePosition, dir: Direction| -> bool {
		match map.pos_apply_dir(limit_zone_pos, dir) {
			Ok(moved) => moved.y_coords_are_equal(zone_pos),
			Err(()) => false,
		}
	};

	let should_move_horizontally = |limit_zone_pos, dir| -> bool {
		match map.pos_apply_dir(limit_zone_pos, dir) {
			Ok(moved) => moved.x_coords_are_equal(zone_pos),
			Err(()) => false,
		}
	};

	let go_up = should_move_vertically(top_left, Direction::Up);
	let go_down = should_move_vertically(bottom_right, Direction::Down);
	let go_left = should_move_horizontally(top_left, Direction::Left);
	let go_right = should_move_horizontally(bottom_right, Direction::Right);

	let mut new_top_left = *top_left;

	let camera_height = window.height() as usize;
	let camera_width = window.width() as usize;

	// TODO Hmm, will the Map crash if it's Zoned (not looping)?
	// Get the `Err` value if necessary?

	// See if I have to go up or down.
	if go_up {
		new_top_left = map
			.pos_apply_dir_amount(&new_top_left, Direction::Up, camera_height)
			.unwrap();
	} else if go_down {
		new_top_left = map
			.pos_apply_dir_amount(&new_top_left, Direction::Down, camera_height)
			.unwrap();
	}

	// See if I have to go left or right.
	if go_left {
		new_top_left = map
			.pos_apply_dir_amount(&new_top_left, Direction::Left, camera_width)
			.unwrap();
	} else if go_right {
		new_top_left = map
			.pos_apply_dir_amount(&new_top_left, Direction::Right, camera_width)
			.unwrap();
	}

	new_top_left
}

/// Returns the smallest dimensions (height and width) between
/// a `Map` and the `Window` it will be drawn to.
fn view_dimensions(window: &Window, map: &Map) -> (u16, u16) {
	let view_height = std::cmp::min(window.height(), map.height_total());
	let view_width = std::cmp::min(window.width(), map.width_total());

	(view_height, view_width)
}
