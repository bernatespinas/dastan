use nrustes::prelude::ScrollGrid;

use direction_simple::Direction;

use dastan::map::{Map, Tile, ZonePosition};

/// I need to create a new type since `Map` and `ScrollGrid` come from
/// different crates.
pub struct MapScrollGrid<'a>(pub &'a Map);

impl<'a> ScrollGrid for MapScrollGrid<'a> {
	type Position = ZonePosition;
	type CellItem = Tile;

	fn rows(&self) -> u16 {
		self.0.height_total()
	}

	fn columns(&self) -> u16 {
		self.0.width_total()
	}

	fn apply_direction(
		&self,
		position: &Self::Position,
		direction: Direction,
	) -> Option<Self::Position> {
		self.0.pos_apply_dir(position, direction).ok()
	}

	fn position_to_screen_coordinates(
		&self,
		zone_position: &Self::Position,
		top_left: &Self::Position,
	) -> Option<(u16, u16)> {
		let mut cursor = *top_left;

		let mut y = 0;

		while cursor.zone_id.0 != zone_position.zone_id.0 || cursor.pos.y != zone_position.pos.y {
			cursor = self.apply_direction(&cursor, Direction::Down).unwrap();

			y += 1;
		}

		let mut x = 0;

		while cursor.zone_id.1 != zone_position.zone_id.1 || cursor.pos.x != zone_position.pos.x {
			cursor = self.apply_direction(&cursor, Direction::Right).unwrap();

			x += 1;
		}

		Some((y, x))
	}

	fn cell_item(&self, position: &Self::Position) -> &Self::CellItem {
		self.0.zone_tile(position)
	}
}
