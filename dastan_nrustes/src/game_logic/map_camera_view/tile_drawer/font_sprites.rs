use std::{collections::HashMap, fs::File, io::BufReader, path::PathBuf};

use dastan::color::Color;

#[derive(Serialize, Deserialize)]
pub struct FontSprites {
	font_sprites: HashMap<String, FontSprite>,
}

impl FontSprites {
	pub fn load() -> Self {
		ron::de::from_reader(BufReader::new(
			File::open(
				PathBuf::from("resources")
					.join("sprites")
					.join("font_sprites.ron"),
			)
			.unwrap(),
		))
		.expect("Unable to load font sprites.")
	}

	pub fn font_sprite(&self, sprite: &str) -> FontSprite {
		*self
			.font_sprites
			.get(sprite)
			.expect(&format!("No sprite has been defined for \"{sprite}\"."))
	}
}

#[derive(Clone, Copy, Serialize, Deserialize)]
pub struct FontSprite {
	pub icon: char,
	pub color_fg: Color,
	pub color_bg: Option<Color>,
}
