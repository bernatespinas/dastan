use nrustes::prelude::{Container, Context, DrawGridCell, RGB8};

use dastan::color::Color;
use dastan::ecs::prelude::*;
use dastan::map::Tile;
use dastan::terrain::TerrainGenerator;

use super::font_sprites::FontSprites;

pub struct FontTileDrawer {
	pub color_palette: fn(Color) -> RGB8,

	pub font_sprites: FontSprites,
}

impl DrawGridCell for FontTileDrawer {
	type CellItem = Tile;
	type AuxData = TerrainGenerator;

	fn draw(
		&self,
		container: &impl Container,
		y: u16,
		x: u16,
		tile: &Self::CellItem,
		terrain_gen: &Self::AuxData,
		context: &mut impl Context,
	) {
		let terrain = terrain_gen.terrain(&tile.terrain_type);
		let terrain_font_sprite = self.font_sprites.font_sprite(terrain.sprite());

		let font_sprite = if let Some(creature) = tile.get::<Creature>() {
			self.font_sprites.font_sprite(creature.sprite())
		} else if let Some(prop) = tile.get::<Prop>() {
			self.font_sprites.font_sprite(prop.sprite())
		} else if let Some(item) = tile.get::<Item>() {
			self.font_sprites.font_sprite(item.sprite())
		} else {
			terrain_font_sprite
		};

		let fg = (self.color_palette)(font_sprite.color_fg);
		let bg = (self.color_palette)(
			font_sprite
				.color_bg
				.unwrap_or_else(|| terrain_font_sprite.color_bg.unwrap()),
		);

		context.render_char_container(container.window(), y, x, font_sprite.icon, fg, bg);
	}
}
