use nrustes::prelude::{Container, Context, DrawGridCell};

use dastan::ecs::prelude::{Creature, Entity, Item, Prop};
use dastan::map::Tile;
use dastan::terrain::TerrainGenerator;

pub struct GraphicsTileDrawer {}

impl DrawGridCell for GraphicsTileDrawer {
	type CellItem = Tile;
	type AuxData = TerrainGenerator;

	fn draw(
		&self,
		container: &impl Container,
		y: u16,
		x: u16,
		tile: &Self::CellItem,
		terrain_gen: &Self::AuxData,
		context: &mut impl Context,
	) {
		let fg_sprite_name = if let Some(creature) = tile.get::<Creature>() {
			Some(creature.name())
		} else if let Some(prop) = tile.get::<Prop>() {
			Some(prop.name())
		} else {
			tile.get::<Item>().map(|item| item.name())
		};

		let map_camera_view_y = container.y() + y;
		let map_camera_view_x = container.x() + x;

		let terrain = terrain_gen.terrain(&tile.terrain_type);

		context.render_graphics_sprite(terrain.name(), map_camera_view_y, map_camera_view_x);

		if let Some(sprite_name) = fg_sprite_name {
			context.render_graphics_sprite(sprite_name, map_camera_view_y, map_camera_view_x);
		}
	}
}
