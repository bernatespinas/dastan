use nrustes::prelude::Context;

use dastan::game::Game;

use super::{map_camera_view::MapCameraView, EventQueue, Flags, InputHandler, TurnAction};

pub fn handle_player_input<CTX: Context>(
	game: &mut Game,
	input_handler: &InputHandler<CTX>,
	map_camera_view: &MapCameraView,
	event_queue: &mut EventQueue,
	flags: &mut Flags,
	context: &mut CTX,
) {
	let current_player_pos = *game.player_zone_pos();

	let turn_action =
		input_handler.handle_input(map_camera_view, game, event_queue, flags, context);

	handle_turn_action(turn_action, flags);

	let new_player_pos = *game.player_zone_pos();

	if current_player_pos != new_player_pos {
		flags.player_zone_pos_has_changed = true;

		if !new_player_pos.is_in_same_zone(&current_player_pos) {
			flags.player_zone_id_has_changed = true;
		}
	}
}

fn handle_turn_action(turn_action: TurnAction, flags: &mut Flags) {
	flags.take_turn = false;
	flags.redraw = false;

	match turn_action {
		TurnAction::Redraw => flags.redraw = true,
		TurnAction::TakeTurn => flags.take_turn = true,
		TurnAction::TakeTurnAndRedraw => {
			flags.take_turn = true;
			flags.redraw = true;
		}
		TurnAction::Nothing => {}
	};
}
