use dastan::{game::Game, map::ZoneFeature};

#[derive(Default)]
pub struct Flags {
	/// Whether the player took a turn.
	pub take_turn: bool,

	/// Whether the UI needs to be fully drawn.
	pub redraw: bool,

	/// Whether the game has to be saved.
	pub save: bool,

	/// Whether the game has to be exited.
	pub exit: bool,

	/// Whether the player's `ZonePos` has changed.
	pub player_zone_pos_has_changed: bool,

	/// Whether the player's `ZoneId` has changed.
	pub player_zone_id_has_changed: bool,

	/// Whether the player has been moved to another `Map`.
	pub player_map_has_changed: bool,

	/// Whether new messages were added to the `MessageLog`.
	pub has_new_messages_to_display: bool,

	/// Whether the ambiance sound which plays in the background might need to be updated.
	pub ambiance_might_need_to_be_updated: bool,

	/// The `ZoneFeature` of the `Zone` in which the player was last turn.
	pub previous_zone_feature: Option<ZoneFeature>,

	/// The `ZoneFeature` of the `Zone` in which the player is now.
	pub current_zone_feature: Option<ZoneFeature>,
}

impl Flags {
	pub fn for_game_that_has_just_been_loaded(game: &Game) -> Self {
		let current_zone_feature = game
			.map()
			.overview()
			.get(&game.player_zone_pos().zone_id)
			.feature;

		Self {
			redraw: true,
			previous_zone_feature: current_zone_feature,
			current_zone_feature,

			..Default::default()
		}
	}

	pub fn update_zone_feature(&mut self, game: &Game) {
		let current_zone_feature = game
			.map()
			.overview()
			.get(&game.player_zone_pos().zone_id)
			.feature;

		log::trace!("Updating the ZoneFeatures in Flags.");

		log::trace!(
			"\tself.previous_zone_feature: from {:?} to {:?}.",
			self.previous_zone_feature,
			self.current_zone_feature,
		);

		log::trace!(
			"\tself.current_zone_feature: from {:?} to {current_zone_feature:?}.",
			self.current_zone_feature,
		);

		self.previous_zone_feature = self.current_zone_feature;

		self.current_zone_feature = current_zone_feature;
	}
}
