use dastan::map::ZoneFeature;

use crate::audio::AudioSystem;

use super::flags::Flags;

/// Updates the ambiance sound which plays in the background if necessary; does nothing otherwise.
pub fn update_ambiance_sound_if_necessary(flags: &mut Flags, audio_system: &mut AudioSystem) {
	if !flags.ambiance_might_need_to_be_updated {
		log::trace!("Ambiance sound does not need to be updated.");

		return;
	}

	log::trace!("I need to check if the ambiance sound actually needs to be updated.");

	flags.ambiance_might_need_to_be_updated = false;
	log::trace!("Set flags.ambiance_needs_to_be_updated to false.");

	if flags.previous_zone_feature == flags.current_zone_feature {
		log::trace!(
			"The ZoneFeature has not changed, so the ambiance sound does not need to be updated."
		);

		return;
	}

	log::debug!("The ambiance sound needs to be updated. I'm told the ZoneFeature has changed from {:?} to {:?}.", flags.previous_zone_feature, flags.current_zone_feature);

	let result = match flags.current_zone_feature {
		None => audio_system.stop(),
		Some(ZoneFeature::Forest) => audio_system.play_background(
			"665156__felixblume__woods-ambience-in-missouri-some-birds-singing-and-soft-breeze.ogg",
		),
		Some(ZoneFeature::River) => audio_system.stop(),
		Some(ZoneFeature::Settlement) => audio_system.stop(),
	};

	match result {
		Ok(()) => {}
		Err(message) => log::error!("Sound system error: {message}"),
	}
}
