pub enum TurnAction {
	Redraw,
	TakeTurn,
	TakeTurnAndRedraw,
	Nothing,
}
