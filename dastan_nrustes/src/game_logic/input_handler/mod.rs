use nrustes::prelude::*;

use dastan::{
	command::{Command, SkipTurn},
	ecs::prelude::*,
	game::{Game, PlayerMapChange},
};

use super::{map_camera_view::MapCameraView, EventQueue, Flags};

mod views;
use views::CommandProducer;

mod target;
pub use target::TargetCommand;

mod menu;

mod turn_action;
pub use turn_action::TurnAction;

mod player_input_handler;
use player_input_handler::PlayerInputHandler;

mod move_or_attack;

pub struct InputHandler<CTX: Context> {
	humanoid: PlayerInputHandler<CTX>,
	spaceship: PlayerInputHandler<CTX>,
}

impl<CTX: Context> Default for InputHandler<CTX> {
	fn default() -> Self {
		let humanoid = PlayerInputHandler::default()
			.with_view(Key::Char('i'), views::InventoryView::produce)
			.with_view(Key::Char('e'), views::EquipmentView::produce)
			.with_view(Key::Char('g'), views::GetView::produce)
			.with_view(Key::Char('b'), views::BuildView::produce)
			.with_view(Key::Char('r'), views::RecipeView::produce)
			.with_view(Key::Char('l'), views::MessageLogView::produce)
			.with_view(Key::Char('m'), views::MapView::produce)
			.with_view(Key::Char('c'), views::CalendarView::produce)
			.with_view(Key::Char('u'), views::interact)
			.with_view(Key::Char('t'), views::target_creature)
			.with_view(Key::Char('.'), skip_turn)
			.with_view(Key::Select, skip_turn)
			.with_menu(Key::Back, menu::menu);

		let spaceship = PlayerInputHandler::default().with_view(Key::Back, leave_spaceship);

		InputHandler {
			humanoid,
			spaceship,
		}
	}
}

fn leave_spaceship(
	_game: &Game,
	_controlled_creature: &Creature,
	_map_camera_view: &MapCameraView,
	_context: &mut impl Context,
) -> Option<Box<dyn Command>> {
	Some(Box::new(PlayerMapChange::StopControllingEntity))
}

fn skip_turn(
	_game: &Game,
	_controlled_creature: &Creature,
	_map_camera_view: &MapCameraView,
	_context: &mut impl Context,
) -> Option<Box<dyn Command>> {
	Some(Box::new(SkipTurn {}))
}
impl<CTX: Context> InputHandler<CTX> {
	pub fn handle_input(
		&self,
		map_camera_view: &MapCameraView,
		game: &mut Game,
		event_queue: &mut EventQueue,
		flags: &mut Flags,
		context: &mut CTX,
	) -> TurnAction {
		let key = context.input_key().gamify();

		// TODO A ver, funciona pero..., choices.
		let player_input_handler = if game
			.map()
			.entity_components(game.player().id())
			.humanoid_body
			.is_some()
		{
			&self.humanoid
		} else {
			&self.spaceship
		};

		let (command, turn_action) = player_input_handler.handle_input(
			key,
			game,
			game.player(),
			map_camera_view,
			flags,
			context,
		);

		if let Some(c) = command {
			let event = player_input_handler.handle_command(c, game);

			event_queue.add(event);
		}

		turn_action
	}
}
