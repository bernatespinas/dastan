use direction_simple::Direction;

use nrustes::prelude::*;

use dastan::ecs::prelude::Creature;
use dastan::game::Game;
use dastan::map::{Map, ZonePosition};

use crate::game_logic::map_camera_view::MapCameraView;
use crate::game_logic::panels::PanelManager;

mod tile_info_fn;
use tile_info_fn::TileInfoFn;

mod cursor_movement_fn;
use cursor_movement_fn::CursorMovementFn;

pub struct TargetCommand<'a, T: Copy> {
	map_camera_view: MapCameraView,

	panel_manager: PanelManager<Shortcuts<Window, T>>,

	initial_zone_pos: ZonePosition,
	zone_pos: ZonePosition,
	old_zone_pos: ZonePosition,

	cursor_movement_fn: CursorMovementFn<T>,
	tile_info_fn: TileInfoFn<'a, T>,
}

impl<'a, T: Copy> TargetCommand<'a, T> {
	pub fn new(
		game: &Game,
		original_map_camera_view: &MapCameraView,
		centered_at: ZonePosition,
		tile_info_fn: TileInfoFn<'a, T>,
		context: &mut impl Context,
	) -> Self {
		let map_camera_view = if game.is_current_map(game.map()) {
			MapCameraView::with_top_left(*original_map_camera_view.top_left(), game.map(), context)
		} else {
			MapCameraView::centered_at(&centered_at, game.map(), context)
		};

		TargetCommand {
			map_camera_view,

			panel_manager: PanelManager::new(context),

			initial_zone_pos: centered_at,
			zone_pos: centered_at,
			old_zone_pos: centered_at,

			cursor_movement_fn: cursor_movement_fn::visible_movement,
			tile_info_fn,
		}
	}

	// Call try_to_center_position only when the player is targeting
	// TODO Call try_to_center_position only when the player is targeting
	// something in another Map (to preserve the current view in the
	// current Map).
	fn center_map_camera_view_if_necessary(&mut self, game: &Game, map: &Map) {
		if !game.is_current_map(map) {
			log::debug!(
				"Map {} is not the current map - calling try_to_center_position at {:?}",
				map.id(),
				&self.zone_pos
			);

			self.map_camera_view
				.try_to_center_position(&self.zone_pos, map);
		}
	}

	pub fn adjacent(mut self) -> Self {
		self.cursor_movement_fn = cursor_movement_fn::adjacent_movement;

		self
	}

	pub fn input(
		&mut self,
		game: &Game,
		map: &'a Map,
		controlled_creature: &Creature,
		context: &mut impl Context,
	) -> Option<(T, ZonePosition)> {
		self.center_map_camera_view_if_necessary(game, map);

		self.map_camera_view
			.draw_map(map, &game.gen_bundle.terrain_gen, context);

		self.panel_manager.redraw_small_panels(
			game,
			controlled_creature,
			&self.map_camera_view,
			context,
		);

		self.panel_manager.custom_panel.draw(context);

		loop {
			self.draw(game, map, controlled_creature, context);

			let key = context.input_key().gamify();

			match key {
				Key::Movement(direction) => match self.validate_movement(map, direction) {
					None => continue,
					Some(zone_pos) => {
						self.update_zone_pos(zone_pos);
						self.clear_panels(context);
					}
				},
				Key::Back => break,
				k => match self.panel_manager.custom_panel.shortcut_data(&k) {
					None => {}
					Some(shortcut_data) => return Some((*shortcut_data, self.zone_pos)),
				},
			}
		}

		None
	}

	pub fn custom_panel(&mut self) -> &mut Shortcuts<Window, T> {
		&mut self.panel_manager.custom_panel
	}

	fn update_zone_pos(&mut self, zone_pos: ZonePosition) {
		self.old_zone_pos = self.zone_pos;
		self.zone_pos = zone_pos;
	}

	fn clear_panels(&mut self, context: &mut impl Context) {
		self.panel_manager.custom_panel.clear_shortcuts();
		self.panel_manager
			.custom_panel
			.erase_and_delete_drawn_content(context);

		self.panel_manager.target_health.erase_content(context);
	}

	fn validate_movement(&self, map: &Map, direction: Direction) -> Option<ZonePosition> {
		let Ok(dpos) = map.pos_apply_dir(&self.zone_pos, direction) else {
			return None;
		};

		(self.cursor_movement_fn)(self, map, dpos)
	}

	fn draw(
		&mut self,
		game: &Game,
		map: &'a Map,
		controlled_creature: &Creature,
		context: &mut impl Context,
	) {
		if context.drawing_is_slow() {
			self.map_camera_view.draw_tile(
				map,
				&self.old_zone_pos,
				&game.gen_bundle.terrain_gen,
				context,
			);
		} else {
			self.map_camera_view
				.draw_map(map, &game.gen_bundle.terrain_gen, context);
		}

		self.map_camera_view.draw_target_cursor(
			map,
			&self.zone_pos,
			&game.gen_bundle.terrain_gen,
			context,
		);

		let tile = map.zone_tile(&self.zone_pos);

		(self.tile_info_fn)(
			map,
			tile,
			controlled_creature,
			&mut self.panel_manager.custom_panel,
		);

		self.panel_manager.custom_panel.draw(context);

		if let Some(creature) = tile.get::<Creature>() {
			self.panel_manager
				.target_health
				.draw_health(creature, context);
		}
	}
}
