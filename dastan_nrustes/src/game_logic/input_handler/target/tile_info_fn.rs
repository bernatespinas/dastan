use nrustes::{shortcuts::Shortcuts, window::Window};

use dastan::{
	ecs::prelude::Creature,
	map::{Map, Tile},
};

/// A function that prints info about the currently targetted `Tile` and adds relevant shortcuts to a `Shortcuts` container.
pub type TileInfoFn<'a, T> = fn(
	map: &'a Map,
	tile: &Tile,
	controlled_creature: &Creature,
	details: &mut Shortcuts<Window, T>,
);
