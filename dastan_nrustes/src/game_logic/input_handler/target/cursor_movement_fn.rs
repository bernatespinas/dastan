use dastan::map::{Map, ZonePosition};

use super::TargetCommand;

/// A function that dictates how the targeting cursor moves across the `Map`.
/// For example, it might only allow movement in adjacent tiles.
pub type CursorMovementFn<T> =
	fn(target_command: &TargetCommand<T>, map: &Map, dpos: ZonePosition) -> Option<ZonePosition>;

// TODO Check if visible!! Or always center the player ¿on? the screen.
/// Restricts the targeting cursor's movement to only adjacent `Tile`s.
pub fn adjacent_movement<T: Copy>(
	target_command: &TargetCommand<T>,
	map: &Map,
	dpos: ZonePosition,
) -> Option<ZonePosition> {
	if !map.are_adjacent(&target_command.initial_zone_pos, &dpos)
		&& dpos != target_command.initial_zone_pos
	{
		None
	} else {
		Some(dpos)
	}
}

/// Restricts the movement of the targeting cursor to only visible `Tile`s.
pub fn visible_movement<T: Copy>(
	target_command: &TargetCommand<T>,
	map: &Map,
	dpos: ZonePosition,
) -> Option<ZonePosition> {
	if !target_command.map_camera_view.is_visible(&dpos, map) {
		None
	} else {
		Some(dpos)
	}
}
