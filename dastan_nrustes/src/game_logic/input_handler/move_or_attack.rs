use direction_simple::Direction;

use dastan::{
	command::{self, Command},
	ecs::prelude::{Creature, EntityId, Identify, Prop},
	ecs_components::PassageComponent,
	game::Game,
	map::{Map, Tile, ZonePosition},
};

pub fn move_or_attack(game: &Game, direction: Direction) -> Option<Box<dyn Command>> {
	let player_zone_pos = game.player_zone_pos();

	let Some(dst_zone_pos) = game.map().pos_apply_dir(player_zone_pos, direction).ok() else {return None;};

	let dst_tile = game.map().zone_tile(&dst_zone_pos);

	walk(game.player().id(), dst_tile, dst_zone_pos, game.map())
}

fn walk(
	creature_id: &EntityId,
	dst_tile: &Tile,
	dst_zone_pos: ZonePosition,
	map: &Map,
) -> Option<Box<dyn Command>> {
	if let Some(target) = dst_tile.get::<Creature>() {
		Some(Box::new(command::Attack {
			attacker_id: *creature_id,
			target_id: *target.id(),
		}))
	} else if !tile_blocks_passage(dst_tile, map) {
		Some(Box::new(command::MovePlayer { dst_zone_pos }))
	// Some(Box::new(command::CreatureMove {
	// 	src_zone_pos,
	// 	dst_zone_pos,
	// }))
	} else {
		None
	}
}

fn tile_blocks_passage(tile: &Tile, map: &Map) -> bool {
	if tile.terrain_type.blocks_passage() {
		return true;
	}

	if tile.has_creature() {
		return true;
	}

	if tile.item_blocks_passage() {
		return true;
	}

	if let Some(prop) = tile.get::<Prop>() {
		let passage_component = map.entity_component::<PassageComponent>(prop.id());

		match passage_component {
			None => return true,
			Some(pc) => {
				if pc.blocks_passage {
					return true;
				}
			}
		};
	}

	false
}
