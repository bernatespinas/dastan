use std::fmt::Display;

use direction_simple::Direction;

use nrustes::prelude::*;

use dastan::command::{Command, Spawn};
use dastan::game::Game;

use crate::game_logic::flags::Flags;

#[derive(Clone, Copy)]
pub enum Action {
	Select,
}

// type Output = GameAction<Humanoid>;

#[derive(Debug, Clone, Copy, EnumIter)]
pub enum MenuItem {
	SaveGame,
	ExitGame,
	Settings,
	Manual,
	Cheats,
}

impl std::fmt::Display for MenuItem {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			MenuItem::SaveGame => write!(f, "Save game"),
			MenuItem::ExitGame => write!(f, "Exit game"),
			other => write!(f, "{other:?}"),
		}
	}
}

#[derive(Draw, MonoViewHolder)]
pub struct MenuView {
	#[draw]
	#[main_container]
	scrollable: Shortcuts<Scrollable<MenuItem>, Action>,
}

impl MenuView {
	fn new(context: &mut impl Context) -> Self {
		let scrollable: Shortcuts<Scrollable<MenuItem>, Action> =
			WindowBuilder::full("Menu", context)
				.scrollable_builder()
				.with_items(MenuItem::iter().collect())
				.build();

		MenuView { scrollable }
	}
}

impl MonoView for MenuView {
	type Output = MenuItem;

	fn initialize(&mut self) {
		self.scrollable
			.bind_shortcut(Key::Select, Shortcut::new(Action::Select));
	}

	fn handle_action(
		&mut self,
		action: &Self::Action,
		_context: &mut impl Context,
	) -> Option<Result<Self::Output, String>> {
		match action {
			Action::Select => self
				.scrollable
				.container()
				.hovered()
				.map(|menu_item| Ok(*menu_item)),
		}
	}
}

pub fn menu(
	game: &Game,
	flags: &mut Flags,
	context: &mut impl Context,
) -> Option<Box<dyn Command>> {
	let mut menu_view = MenuView::new(context);

	loop {
		match menu_view.input_loop(context) {
			Some(MenuItem::SaveGame) => {
				flags.save = true;

				return None;
			}
			Some(MenuItem::ExitGame) => {
				flags.exit = true;

				return None;
			}
			Some(MenuItem::Cheats) => match cheats(game, context) {
				None => continue,
				some => return some,
			},
			Some(MenuItem::Settings) | Some(MenuItem::Manual) => continue,
			None => return None,
		};
	}
}

#[derive(Debug, EnumIter)]
enum CheatItem {
	// InfiniteHealth,
	// InfiniteSource,
	// Invisibility,
	// Hunger,
	// Immunity,
	// LearnRecipe,
	// InstaKill,
	SpawnThing,
}

impl std::fmt::Display for CheatItem {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{self:?}")
	}
}

fn cheats(game: &Game, context: &mut impl Context) -> Option<Box<dyn Command>> {
	let mut scrollable: Scrollable<CheatItem> = WindowBuilder::full("Cheats", context)
		.scrollable_builder()
		.with_items(CheatItem::iter().collect())
		.build();

	loop {
		match scrollable.handle_input_and_redraw(context).unwrap() {
			Key::Select => {
				if let Some(CheatItem::SpawnThing) = scrollable.hovered() {
					match spawn_thing(game, context) {
						None => {}
						Some(command) => return Some(command),
					}
				}
			}
			Key::Back => return None,
			_ => continue,
		};
	}
}

// fn learn_recipe(_game: &Game, _context: &mut impl Context) {
// 	// let recipes = game.gen_bundle.
// 	// Popup::error("Learn recipe", context);
// }

#[derive(Debug, Clone, Copy)]
enum GenerateThing {
	Creature,
	Prop,
	Item,
}

impl Display for GenerateThing {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{self:?}")
	}
}

fn spawn_thing(game: &Game, context: &mut impl Context) -> Option<Box<dyn Command>> {
	let items = vec![
		GenerateThing::Creature,
		GenerateThing::Prop,
		GenerateThing::Item,
	];

	let mut scrollable: Scrollable<GenerateThing> = WindowBuilder::full("Spawn things", context)
		.scrollable_builder()
		.with_items(items)
		.build();

	let spawn_pos = {
		let player_zone_pos = game.player_zone_pos();
		// TODO .unwrap() because I'd like to use target here.
		game.map()
			.pos_apply_dir(player_zone_pos, Direction::Up)
			.unwrap()
	};

	loop {
		match scrollable.handle_input_and_redraw(context).unwrap() {
			Key::Select => {
				let generate_thing = *scrollable.hovered().unwrap();

				let key = TextInputModal::new(
					format!("{generate_thing:?} key"),
					scrollable.window(),
					context,
				)
				.show(&mut scrollable, context);

				match key {
					None => {}
					Some(k) => match generate_thing {
						GenerateThing::Creature => {
							return Some(Box::new(Spawn {
								zone_pos: spawn_pos,
								creature: Some(k),
								prop: None,
								item: None,
							}))
						}
						GenerateThing::Prop => {
							return Some(Box::new(Spawn {
								zone_pos: spawn_pos,
								creature: None,
								prop: Some(k),
								item: None,
							}))
						}
						GenerateThing::Item => {
							return Some(Box::new(Spawn {
								zone_pos: spawn_pos,
								creature: None,
								prop: None,
								item: Some(k),
							}))
						}
					},
				}
			}

			Key::Back => return None,
			_ => continue,
		}
	}
}
