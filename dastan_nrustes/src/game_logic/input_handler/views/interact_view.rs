use nrustes::prelude::*;

use dastan::command::{self, Command};
use dastan::ecs::prelude::*;
use dastan::ecs_components::{ComputerComponent, Inventory, TeleportComponent};
use dastan::game::{Game, PlayerMapChange};
use dastan::map::{MapZonePos, Position, ZonePosition};

use crate::game_logic::input_handler::target::TargetCommand;
use crate::game_logic::map_camera_view::MapCameraView;

use super::{BootView, LootInventoryView};

mod interact_tile_info_fn;
use interact_tile_info_fn::{interact_tile_info_fn, InteractAction};

pub fn interact(
	game: &Game,
	controlled_creature: &Creature,
	map_camera_view: &MapCameraView,
	context: &mut impl Context,
) -> Option<Box<dyn Command>> {
	let controlled_creature_zone_pos = game
		.map()
		.entity_zone_pos(controlled_creature.id())
		.unwrap();

	let mut target_command = TargetCommand::new(
		game,
		map_camera_view,
		*controlled_creature_zone_pos,
		interact_tile_info_fn,
		context,
	)
	.adjacent();

	match target_command.input(game, game.map(), controlled_creature, context) {
		Some((action, _zone_pos)) => match action {
			InteractAction::BootComputer(computer) => boot_computer(computer, context),

			InteractAction::LootInventory(looted_inventory) => {
				loot_inventory(game, controlled_creature, looted_inventory, context)
			}

			InteractAction::TogglePassage(entity_id) => {
				Some(Box::new(command::TogglePassage { entity_id }))
			}

			InteractAction::Teleport(teleport_component) => teleport(teleport_component),
		},
		None => None,
	}
}

fn boot_computer(
	computer: &ComputerComponent,
	context: &mut impl Context,
) -> Option<Box<dyn Command>> {
	BootView::new(computer, context).boot(context).ok().unwrap()
}

fn loot_inventory(
	game: &Game,
	controlled_creature: &Creature,
	looted_inventory: &Inventory,
	context: &mut impl Context,
) -> Option<Box<dyn Command>> {
	let looter_inventory = game
		.map()
		.entity_component::<Inventory>(controlled_creature.id())
		.unwrap();

	LootInventoryView::new(looter_inventory, looted_inventory, context).input_loop(context)
}

fn teleport(teleport_component: &TeleportComponent) -> Option<Box<dyn Command>> {
	match teleport_component {
		TeleportComponent::AtMap(map_id) => {
			log::debug!("TeleportComponent::AtMap, using ZonePosition 0,0 0,0");

			let map_zone_pos = MapZonePos {
				map_id: *map_id,
				zone_pos: ZonePosition::new((0, 0), Position::new(0, 0)),
			};

			Some(Box::new(PlayerMapChange::TeleportToPosition(map_zone_pos)))
		}
		TeleportComponent::AtEntity(entity_id) => {
			Some(Box::new(PlayerMapChange::TeleportToEntity(*entity_id)))
		}
		TeleportComponent::NextToEntity(entity_id) => {
			Some(Box::new(PlayerMapChange::PickAdjacentPosition(*entity_id)))
		}
	}
}
