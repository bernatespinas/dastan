use nrustes::prelude::*;

use dastan::command::Command;
use dastan::ecs::prelude::Creature;
use dastan::game::Game;
use dastan::time::Time;

use crate::game_logic::input_handler::CommandProducer;
use crate::game_logic::map_camera_view::MapCameraView;

#[derive(Draw, MonoViewHolder)]
pub struct CalendarView<'a> {
	#[draw]
	#[main_container]
	calendar: Shortcuts<Window, ()>,

	#[draw]
	details: Window,

	#[draw]
	other: Window,

	time: &'a Time,
}

impl<'a> CalendarView<'a> {
	fn new(time: &'a Time, context: &mut impl Context) -> CalendarView<'a> {
		let panel_width = context.width() / 4;

		let calendar: Shortcuts<Window, ()> = WindowBuilder::new(
			0,
			0,
			context.height(),
			context.width() - panel_width,
			context,
		)
		.with_centered_title("Calendar")
		.build();

		let details: Window = WindowBuilder::new(
			0,
			context.width() - panel_width,
			context.height() / 3,
			panel_width,
			context,
		)
		.with_centered_title("Details")
		.build();

		let other = WindowBuilder::new(
			context.height() / 3,
			context.width() - panel_width,
			context.height() - details.height(),
			panel_width,
			context,
		)
		.with_title("OTHER")
		.with_centered_bottom_text("OTHER")
		.build();

		CalendarView {
			calendar,
			details,
			other,

			time,
		}
	}

	fn draw_calendar(&mut self) {
		self.calendar
			.window_mut()
			.print_accent((1, 1), "Mon Tue Wed Thu Fri Sat Sun");

		let mut day: u8 = 1;
		let mut y: u16 = 3;
		let mut x: u16 = 2;

		for _week in 0..4 {
			for _weekday in 0..7 {
				if self.time.day_number() == day {
					self.calendar.window_mut().print_accent((y, x), day);
				} else {
					self.calendar.window_mut().print_primary((y, x), day);
				}

				x += 4;
				day += 1;
			}

			x = 2;
			y += 2;
		}
	}
}

impl<'a> MonoView for CalendarView<'a> {
	type Output = ();

	fn initialize(&mut self) {
		self.draw_calendar();
	}

	fn handle_action(
		&mut self,
		_action: &Self::Action,
		_context: &mut impl Context,
	) -> Option<Result<Self::Output, String>> {
		None
	}
}

impl<'a> CommandProducer for CalendarView<'a> {
	fn produce(
		game: &Game,
		_controlled_creature: &Creature,
		_map_camera_view: &MapCameraView,
		context: &mut impl Context,
	) -> Option<Box<dyn Command>> {
		CalendarView::new(&game.map().time, context).input_loop(context);

		None
	}
}
