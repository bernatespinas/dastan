use nrustes::prelude::*;

use dastan::{command::Command, ecs::prelude::Entity, ecs_components::Inventory};

#[derive(Clone, Copy)]
pub enum Action {
	Swap,
	Select,
	Transfer,
}

struct InventoryScrollableState<'a> {
	name: &'a str,
	inventory: &'a Inventory,
	selected_indexes: Vec<usize>,

	/// The currently hovered position; `None` if there are no items.
	position: Option<usize>,
}

impl<'a> InventoryScrollableState<'a> {
	fn new(name: &'a str, inventory: &'a Inventory) -> Self {
		let position = if inventory.items().is_empty() {
			None
		} else {
			Some(0)
		};

		Self {
			name,
			inventory,
			selected_indexes: Vec::new(),
			position,
		}
	}

	fn scrollable_items(&self) -> Vec<String> {
		self.inventory
			.items()
			.iter()
			.map(|item| item.entity.name().to_string())
			.collect()
	}

	fn has_items_selected(&self) -> bool {
		!self.selected_indexes.is_empty()
	}

	fn selected_amount(&self) -> usize {
		self.selected_indexes.len()
	}
}

#[derive(Draw, DoubleViewHolder)]
pub struct LootInventoryView<'a> {
	#[draw]
	view: DoubleViewState<Scrollable<String>, Window, Action>,

	looter_state: InventoryScrollableState<'a>,
	looted_state: InventoryScrollableState<'a>,

	showing_looter_inventory: bool,
}

impl<'a> LootInventoryView<'a> {
	pub fn new(
		looter_inventory: &'a Inventory,
		looted_inventory: &'a Inventory,
		context: &mut impl Context,
	) -> LootInventoryView<'a> {
		let looter_state = InventoryScrollableState::new("Looter", looter_inventory);

		let scrollable: Scrollable<String> = WindowBuilder::left("Transfer???", context)
			.scrollable_builder()
			.with_items(looter_state.scrollable_items())
			.build();

		let window: Window = WindowBuilder::right("Details", context).build();

		let looted_state = InventoryScrollableState::new("Looted", looted_inventory);

		LootInventoryView {
			view: DoubleViewState::new(scrollable, window),

			looter_state,

			looted_state,

			showing_looter_inventory: true,
		}
	}

	fn current_inv_state(&mut self) -> &mut InventoryScrollableState<'a> {
		if self.showing_looter_inventory {
			&mut self.looter_state
		} else {
			&mut self.looted_state
		}
	}

	fn has_items_selected(&self) -> bool {
		self.looter_state.has_items_selected() || self.looted_state.has_items_selected()
	}

	fn backup_scrollable_state(&mut self) {
		self.current_inv_state().selected_indexes =
			self.scrollable().container().selected_indexes().to_vec();

		self.current_inv_state().position = self.scrollable().container().hovered_index();
	}

	fn restore_scrollable_state(&mut self) {
		let items = self.current_inv_state().scrollable_items();

		self.scrollable_mut().container_mut().change_items(items);

		if let Some(hovered_index) = self.current_inv_state().position {
			self.scrollable_mut()
				.container_mut()
				.set_hovered_index(hovered_index);
		}

		let selected_indexes = self.current_inv_state().selected_indexes.clone();

		self.scrollable_mut()
			.container_mut()
			.set_selected_indexes(selected_indexes);
	}

	fn update_window_counters(&mut self) {
		let looter_counter = format!(
			"Looter selected items: {}",
			self.looter_state.selected_amount()
		);

		self.window_mut().print_accent((2, 1), looter_counter);

		let looted_counter = format!(
			"Looted selected items: {}",
			self.looted_state.selected_amount()
		);

		self.window_mut().print_accent((4, 1), looted_counter);
	}

	fn update(&mut self) {
		self.update_window_counters();

		if self.has_items_selected() {
			self.window_mut().bind_shortcut(
				Key::Char('t'),
				Shortcut::new(Action::Transfer).with_label("Transfer"),
			);
		}
	}
}

impl DoubleView for LootInventoryView<'_> {
	type Output = Box<dyn Command>;

	fn initialize(&mut self) {
		self.scrollable_mut().bind_shortcut(
			Key::Char('s'),
			Shortcut::new(Action::Swap).with_label("Swap").permanent(),
		);

		self.scrollable_mut().bind_shortcut(
			Key::Select,
			Shortcut::new(Action::Select)
				.with_label("Select item")
				.permanent(),
		);

		self.update();
	}

	fn handle_action(
		&mut self,
		action: &Self::Action,
		_context: &mut impl Context,
	) -> Option<Result<Self::Output, String>> {
		match action {
			Action::Transfer => None,

			Action::Select => {
				self.scrollable_mut().container_mut().select_hovered();
				self.backup_scrollable_state();
				self.update_window_counters();

				self.update();
				self.schedule_draw();

				None
			}

			Action::Swap => {
				self.backup_scrollable_state();

				self.showing_looter_inventory = !self.showing_looter_inventory;

				self.restore_scrollable_state();

				self.update();
				self.schedule_draw();

				None
			}
		}
	}
}
