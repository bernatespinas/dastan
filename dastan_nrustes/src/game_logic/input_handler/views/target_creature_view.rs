use nrustes::prelude::*;

use dastan::color::Color;
use dastan::command::{self, Command};
use dastan::ecs::prelude::*;
use dastan::game::Game;
use dastan::map::{Map, Tile};

use crate::game_logic::input_handler::target::TargetCommand;
use crate::game_logic::map_camera_view::MapCameraView;
use crate::to_rgb8::ToRGB8;

fn target_creature_tile_info_fn(
	_map: &Map,
	tile: &Tile,
	controlled_creature: &Creature,
	details: &mut Shortcuts<Window, ()>,
) {
	if let Some(creature) = tile.get::<Creature>() {
		details
			.window_mut()
			.print_fg((1, 1), creature.name(), creature.color().to_rgb8());

		if controlled_creature.id() != creature.id() {
			details.bind_shortcut(Key::Select, Shortcut::new(()).with_label("Target"));
		}
	}

	if let Some(prop) = tile.get::<Prop>() {
		details
			.window_mut()
			.print_fg((3, 1), prop.name(), prop.color().to_rgb8());
	}

	if let Some(item) = tile.get::<Item>() {
		details
			.window_mut()
			.print_fg((5, 1), item.name(), item.color().to_rgb8());
	}

	details
		.window_mut()
		.print_fg((7, 1), tile.terrain_type.to_string(), Color::Red.to_rgb8());
}

pub fn target_creature(
	game: &Game,
	controlled_creature: &Creature,
	map_camera_view: &MapCameraView,
	context: &mut impl Context,
) -> Option<Box<dyn Command>> {
	let creature_zone_pos = game
		.map()
		.entity_zone_pos(controlled_creature.id())
		.unwrap();

	let mut target_command = TargetCommand::new(
		game,
		map_camera_view,
		*creature_zone_pos,
		target_creature_tile_info_fn,
		context,
	);

	match target_command.input(game, game.map(), controlled_creature, context) {
		Some((_, zone_pos)) => {
			let target = game.map().get_by_zone_pos::<Creature>(&zone_pos).unwrap();

			Some(Box::new(command::Target {
				creature_id: *game.player().id(),
				target_id: *target.id(),
			}))
		}
		None => None,
	}
}
