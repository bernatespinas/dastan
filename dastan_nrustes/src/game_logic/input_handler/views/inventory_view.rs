use nrustes::prelude::*;

use dastan::command::{self, Command, ValidateCommand};
use dastan::ecs::prelude::*;
use dastan::ecs_components::Inventory;
use dastan::game::Game;

use crate::game_logic::input_handler::CommandProducer;
use crate::game_logic::map_camera_view::MapCameraView;

use super::details::PrintDetails;

#[derive(Clone, Copy)]
pub enum Action {
	Eat,
	Equip,
	// TODO Drop is a built-in trait - find a new name?
	Drop,
	Search,
	Filter,
	Sort,
	Scroll,
}

#[derive(Draw, DoubleViewHolder)]
pub struct InventoryView<'a> {
	#[draw]
	view: DoubleViewState<Scrollable<&'a String>, Scrollbar, Action>,

	items: &'a Vec<ItemWithComponents>,
	indexes: Vec<usize>,

	game: &'a Game,
	controlled_creature: &'a Creature,
}

impl<'a> InventoryView<'a> {
	fn selected_index(&self) -> Option<usize> {
		if self.indexes.is_empty() {
			None
		} else {
			Some(self.indexes[self.scrollable().container().hovered_index().unwrap()])
		}
	}

	fn update(&mut self) {
		self.scrollbar_mut().clear_shortcuts();
		self.scrollbar_mut().delete_content();

		if let Some(index) = self.selected_index() {
			self.scrollbar_mut().bind_shortcut(
				Key::Char('d'),
				Shortcut::new(Action::Drop).with_label("Drop"),
			);

			let item = self.items.get(index).unwrap();

			item.print_details(self.scrollbar_mut().container_mut());

			if item.components.food.is_some() {
				self.scrollbar_mut()
					.bind_shortcut(Key::Char('e'), Shortcut::new(Action::Eat).with_label("Eat"));
			}

			if item.components.humanoid_equipment.is_some() {
				self.scrollbar_mut().bind_shortcut(
					Key::Char('e'),
					Shortcut::new(Action::Equip).with_label("Equip"),
				);
			}
		}

		self.scrollbar_mut().schedule_draw();
	}
}

impl<'a> DoubleView for InventoryView<'a> {
	type Output = Box<dyn Command>;

	fn initialize(&mut self) {
		self.scrollable_mut()
			.bind_movement_shortcuts(Action::Scroll);

		self.scrollable_mut().bind_shortcut(
			Key::Char('s'),
			Shortcut::new(Action::Search)
				.with_label("Search")
				.permanent(),
		);

		self.scrollable_mut().bind_shortcut(
			Key::Char('f'),
			Shortcut::new(Action::Filter)
				.with_label("Filter")
				.permanent(),
		);

		self.scrollable_mut().bind_shortcut(
			Key::Char('t'),
			Shortcut::new(Action::Sort).with_label("Sort").permanent(),
		);

		self.update();
	}

	fn handle_action(
		&mut self,
		action: &Self::Action,
		context: &mut impl Context,
	) -> Option<Result<Self::Output, String>> {
		match action {
			Action::Eat => Some(
				command::EatFromInventory {
					creature_id: *self.controlled_creature.id(),
					index: self.scrollable().container().hovered_index().unwrap(),
				}
				.validate_command(self.game),
			),
			Action::Equip => Some(
				command::EquipFromInventory {
					creature_id: *self.controlled_creature.id(),
					index: self.scrollable().container().hovered_index().unwrap(),
				}
				.validate_command(self.game),
			),
			Action::Drop => Some(
				command::DropFromInventory {
					creature_id: *self.controlled_creature.id(),
					index: self.scrollable().container().hovered_index().unwrap(),
				}
				.validate_command(self.game),
			),
			Action::Search => {
				let query = TextInputModal::new("Serch", self.scrollable().window(), context)
					.show(self, context)
					.map_or("".to_string(), |s| s.to_lowercase());

				self.indexes = self
					.items
					.iter()
					.enumerate()
					.filter(|(_i, x)| x.entity.name().to_lowercase().contains(query.as_str()))
					.map(|(i, _x)| i)
					.collect();

				let names: Vec<&String> = self
					.items
					.iter()
					.filter_map(|item| {
						let name = item.entity.name();

						if name.to_lowercase().contains(query.as_str()) {
							Some(name)
						} else {
							None
						}
					})
					.collect();

				self.scrollable_mut().container_mut().change_items(names);
				self.update();

				None
			}
			Action::Scroll => {
				self.update();

				None
			}
			Action::Filter | Action::Sort => None,
		}
	}
}

impl<'a> CommandProducer for InventoryView<'a> {
	fn produce(
		game: &Game,
		controlled_creature: &Creature,
		_map_camera_view: &MapCameraView,
		context: &mut impl Context,
	) -> Option<Box<dyn Command>> {
		let inventory = game
			.map()
			.entity_component::<Inventory>(controlled_creature.id())
			.unwrap();

		let items = inventory.items();

		let scrollbar: Scrollbar = WindowBuilder::right("Properties", context).build();

		// A Vector containing the ACTUAL/ORIGINAL indexes of the Items in the Inventory. NOT THE INDEX OF THE VECTOR OF CURRENTLY DISPLAYED ITEMS KK?
		let indexes: Vec<usize> = items.iter().enumerate().map(|(i, _x)| i).collect();

		// A Vector containing the names of the Items that will be displayed.
		let names: Vec<&String> = items.iter().map(|x| x.entity.name()).collect();

		let scrollable: Scrollable<&String> = WindowBuilder::left("Inventory", context)
			.scrollable_builder()
			.with_items(names)
			.build();

		InventoryView {
			view: DoubleViewState::new(scrollable, scrollbar),

			items,
			indexes,

			game,
			controlled_creature,
		}
		.input_loop(context)
	}
}
