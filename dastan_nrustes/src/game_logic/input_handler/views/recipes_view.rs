use nrustes::prelude::*;

use dastan::command::{self, Command, ValidateCommand};
use dastan::ecs::prelude::*;
use dastan::ecs_components::{Profession, Recipe, RecipeBook};
use dastan::game::Game;
use dastan::list_enum::ListEnum;

use crate::game_logic::input_handler::CommandProducer;
use crate::game_logic::map_camera_view::MapCameraView;

use super::details::PrintDetails;

#[derive(Clone)]
pub enum Action {
	Craft {
		profession: Profession,
		index: usize,
	},
	ChangeProfession,
	Search,
	Scroll,
}

#[derive(Draw, DoubleViewHolder)]
pub struct RecipeView<'a> {
	#[draw]
	view: DoubleViewState<Scrollable<&'a String>, Scrollbar, Action>,

	profession: Profession,
	recipe_book: &'a RecipeBook,
	recipes: &'a Vec<Recipe>,

	game: &'a Game,
	controlled_creature: &'a Creature,
}

impl<'a> DoubleView for RecipeView<'a> {
	type Output = Box<dyn Command>;

	fn initialize(&mut self) {
		self.scrollable_mut()
			.bind_movement_shortcuts(Action::Scroll);

		self.scrollable_mut().bind_shortcut(
			Key::Char('s'),
			Shortcut::new(Action::Search)
				.with_label("Search")
				.permanent(),
		);

		self.scrollable_mut().bind_shortcut(
			Key::Char('p'),
			Shortcut::new(Action::ChangeProfession)
				.with_label("Profession")
				.permanent(),
		);

		self.update();
	}

	fn handle_action(
		&mut self,
		action: &Self::Action,
		context: &mut impl Context,
	) -> Option<Result<Self::Output, String>> {
		match action {
			Action::Craft { profession, index } => Some(
				command::CraftFromRecipes {
					creature_id: *self.controlled_creature.id(),
					profession: *profession,
					index: *index,
				}
				.validate_command(self.game),
			),
			Action::Search => None,

			Action::ChangeProfession => {
				let professions = Profession::list_crafting();

				let mut context_menu = ContextMenu::new(
					"Professions",
					professions.iter().map(|p| p.to_string()).collect(),
					self.scrollable().window(),
					context,
				);

				match context_menu.show(self, context) {
					None => {}
					Some(pos) => {
						// TODO I list it again here...
						self.profession = Profession::list()[pos];

						self.recipes = self.recipe_book.recipes_by_profession(self.profession);

						let items: Vec<&String> = self.recipes.iter().map(|r| &r.name).collect();

						self.scrollable_mut().container_mut().change_items(items);

						let new_title = format!("Recipes - {}", self.profession);

						self.scrollable_mut().change_title(new_title);
					}
				}

				None
			}
			Action::Scroll => {
				self.update();

				None
			}
		}
	}
}

impl<'a> RecipeView<'a> {
	fn update(&mut self) {
		self.scrollbar_mut().delete_content();

		if let Some(index) = self.scrollable().container().hovered_index() {
			if let Some(recipe) = self.recipes.get(index) {
				recipe.print_details(self.scrollbar_mut().container_mut());

				let action = Action::Craft {
					profession: self.profession,
					index: self.scrollable().container().hovered_index().unwrap(),
				};

				self.scrollbar_mut()
					.bind_shortcut(Key::Char('e'), Shortcut::new(action).with_label("Craft"));
			}
		}

		self.scrollbar_mut().schedule_draw();
	}
}

impl<'a> CommandProducer for RecipeView<'a> {
	fn produce(
		game: &Game,
		controlled_creature: &Creature,
		_map_camera_view: &MapCameraView,
		context: &mut impl Context,
	) -> Option<Box<dyn Command>> {
		let recipe_book = game
			.map()
			.entity_component::<RecipeBook>(controlled_creature.id())?;

		let profession = Profession::Butchering;

		let scrollbar: Scrollbar = WindowBuilder::right("Properties", context).build();

		let recipes: &Vec<Recipe> = recipe_book.recipes_by_profession(profession);
		let items: Vec<&String> = recipes.iter().map(|r| &r.name).collect();

		let scrollable: Scrollable<&String> =
			WindowBuilder::left(format!("Recipes - {profession}"), context)
				.scrollable_builder()
				.with_items(items)
				.build();

		RecipeView {
			view: DoubleViewState::new(scrollable, scrollbar),

			profession,
			recipe_book,
			recipes,

			game,
			controlled_creature,
		}
		.input_loop(context)
	}
}
