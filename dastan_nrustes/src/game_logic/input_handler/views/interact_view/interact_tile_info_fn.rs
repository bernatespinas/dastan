use nrustes::prelude::*;

use dastan::{
	ecs::prelude::*,
	ecs_components::{ComputerComponent, Inventory, TeleportComponent},
	map::{Map, Tile},
};

use crate::to_rgb8::ToRGB8;

#[derive(Debug, Copy, Clone)]
pub enum InteractAction<'a> {
	LootInventory(&'a Inventory),
	BootComputer(&'a ComputerComponent),
	Teleport(&'a TeleportComponent),
	TogglePassage(EntityId),
}

pub fn interact_tile_info_fn<'a>(
	map: &'a Map,
	tile: &Tile,
	controlled_creature: &Creature,
	details: &mut Shortcuts<Window, InteractAction<'a>>,
) {
	print_highest_priority_tile_info(map, tile, details);

	let possible_entity_id = get_possible_entity_id_from_tile(map, tile, controlled_creature);

	if let Some(entity_id) = possible_entity_id {
		bind_entity_shortcuts(entity_id, map.entity_components(entity_id), details);
	}
}

fn get_possible_entity_id_from_tile<'a>(
	_map: &'a Map,
	tile: &'a Tile,
	controlled_creature: &Creature,
) -> Option<&'a EntityId> {
	if let Some(creature) = tile.get::<Creature>() {
		if controlled_creature.id() != creature.id() {
			Some(creature.id())
		} else {
			None
		}
	} else if let Some(prop) = tile.get::<Prop>() {
		Some(prop.id())
	} else {
		None
	}
}

fn print_highest_priority_tile_info<'a>(
	_map: &'a Map,
	tile: &Tile,
	details: &mut Shortcuts<Window, InteractAction<'a>>,
) {
	if let Some(creature) = tile.get::<Creature>() {
		details
			.window_mut()
			.print_fg((2, 1), creature.name(), creature.color().to_rgb8());
	} else if let Some(prop) = tile.get::<Prop>() {
		details
			.window_mut()
			.print_fg((2, 1), prop.name(), prop.color().to_rgb8());
	} else if let Some(item) = tile.get::<Item>() {
		details
			.window_mut()
			.print_fg((2, 1), item.name(), item.color().to_rgb8());
	} else {
		details.window_mut().print_accent(
			(2, 1),
			tile.terrain_type.to_string(),
			// terrain.color().to_rgb8()
		);
	}
}

fn bind_entity_shortcuts<'a>(
	entity_id: &EntityId,
	entity_components: EntityComponentsRef<'a>,
	details: &mut Shortcuts<Window, InteractAction<'a>>,
) {
	if let Some(inventory) = entity_components.inventory {
		details.bind_shortcut(
			Key::Select,
			Shortcut::new(InteractAction::LootInventory(inventory)).with_label("Loot"),
		);
	} else if entity_components.passage.is_some() {
		details.bind_shortcut(
			Key::Select,
			Shortcut::new(InteractAction::TogglePassage(*entity_id)).with_label("Toggle lock"),
		);
	} else if let Some(teleport) = entity_components.teleport {
		details.bind_shortcut(
			Key::Select,
			Shortcut::new(InteractAction::Teleport(teleport)).with_label("Enter"),
		);
	} else if let Some(computer) = entity_components.computer {
		details.bind_shortcut(
			Key::Select,
			Shortcut::new(InteractAction::BootComputer(computer)).with_label("Boot"),
		);
	}
}
