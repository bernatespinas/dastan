use nrustes::prelude::*;
use nrustes::shortcuts::ShortcutBinder;

use dastan::command::{self, Command, ValidateCommand};
use dastan::ecs::prelude::*;
use dastan::game::Game;
use dastan::map::{Tile, ZonePosition};

use crate::game_logic::input_handler::CommandProducer;
use crate::game_logic::map_camera_view::MapCameraView;
use crate::game_logic::panels::PanelManager;
use crate::to_rgb8::ToRGB8;

#[derive(Clone)]
pub enum Action {
	PickUp,
	Eat,
	Equip,
}

pub struct GetView<'a> {
	game: &'a Game,
	map_camera_view: &'a mut MapCameraView,
	panel_manager: PanelManager<Shortcuts<Window, Action>>,

	needs_draw: bool,

	/// The `Creature` which might perform an action.
	controlled_creature: &'a Creature,

	/// The position of the creature in the current map.
	controlled_creature_zone_pos: ZonePosition,

	/// The `Tile` the creature is in.
	tile: &'a Tile,
}

impl<'a> Draw for GetView<'a> {
	fn draw(&mut self, context: &mut impl Context) {
		self.map_camera_view
			.draw_map(self.game.map(), &self.game.gen_bundle.terrain_gen, context);

		self.panel_manager.redraw_full(
			self.game,
			self.controlled_creature,
			self.map_camera_view,
			context,
		);
	}

	fn needs_draw(&self) -> bool {
		self.needs_draw
	}

	fn schedule_draw(&mut self) {
		self.needs_draw = true;
	}
}

impl<'a> Darken for GetView<'a> {
	fn set_darkened(&mut self, darkened: bool) {
		self.main_container_mut().set_darkened(darkened);
	}
}

impl<'a> MonoViewHolder for GetView<'a> {
	type Container1 = Window;
	type Action = Action;

	fn main_container(&self) -> &Shortcuts<Self::Container1, Self::Action> {
		&self.panel_manager.custom_panel
	}

	fn main_container_mut(&mut self) -> &mut Shortcuts<Self::Container1, Self::Action> {
		&mut self.panel_manager.custom_panel
	}
}

impl<'a> MonoView for GetView<'a> {
	type Output = Box<dyn Command>;

	fn initialize(&mut self) {
		let terrain = self
			.game
			.gen_bundle
			.terrain_gen
			.terrain(&self.tile.terrain_type);

		self.details_window_mut()
			.print_fg((4, 1), terrain.name(), terrain.bg().to_rgb8());

		if let Some(item) = self.tile.get::<Item>() {
			let components = self.game.map().entity_components(item.id());

			self.details_window_mut()
				.print_fg((2, 1), item.name(), item.color().to_rgb8());

			self.shortcut_binder_mut().bind_shortcut(
				Key::Char('g'),
				Shortcut::new(Action::PickUp).with_label("Get"),
			);

			if components.food.is_some() {
				self.shortcut_binder_mut()
					.bind_shortcut(Key::Char('e'), Shortcut::new(Action::Eat).with_label("Eat"));
			}

			if components.humanoid_equipment.is_some() {
				self.shortcut_binder_mut().bind_shortcut(
					Key::Char('e'),
					Shortcut::new(Action::Equip).with_label("Equip"),
				);
			}
		}
	}

	fn handle_action(
		&mut self,
		action: &Self::Action,
		_context: &mut impl Context,
	) -> Option<Result<Self::Output, String>> {
		match action {
			Action::PickUp => Some(
				command::PickItemUpFromMap {
					creature_id: *self.controlled_creature.id(),
					zone_pos: self.controlled_creature_zone_pos,
				}
				.validate_command(self.game),
			),
			Action::Eat => Some(
				command::EatFromMap {
					creature_id: *self.controlled_creature.id(),
					zone_pos: self.controlled_creature_zone_pos,
				}
				.validate_command(self.game),
			),
			Action::Equip => Some(
				command::EquipFromMap {
					creature_id: *self.controlled_creature.id(),
					zone_pos: self.controlled_creature_zone_pos,
				}
				.validate_command(self.game),
			),
		}
	}
}

impl<'a> GetView<'a> {
	fn shortcut_binder_mut(&mut self) -> &mut ShortcutBinder<Action> {
		self.panel_manager.custom_panel.shortcut_binder_mut()
	}

	fn details_window_mut(&mut self) -> &mut Window {
		self.panel_manager.custom_panel.window_mut()
	}
}

impl<'a> CommandProducer for GetView<'a> {
	fn produce(
		game: &Game,
		controlled_creature: &Creature,
		original_map_camera_view: &MapCameraView,
		context: &mut impl Context,
	) -> Option<Box<dyn Command>> {
		let mut map_camera_view =
			MapCameraView::with_top_left(*original_map_camera_view.top_left(), game.map(), context);

		let mut panel_manager = PanelManager::<Shortcuts<Window, Action>>::new(context);
		panel_manager.custom_panel.change_title("Information");

		let controlled_creature_zone_pos = *game
			.map()
			.entity_zone_pos(controlled_creature.id())
			.unwrap();

		let tile = game.map().zone_tile(&controlled_creature_zone_pos);

		GetView {
			game,
			map_camera_view: &mut map_camera_view,
			panel_manager,
			needs_draw: true,
			controlled_creature,
			controlled_creature_zone_pos,
			tile,
		}
		.input_loop(context)
	}
}
