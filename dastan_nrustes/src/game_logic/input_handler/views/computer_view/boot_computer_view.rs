use nrustes::prelude::*;

use dastan::command::Command;
use dastan::ecs_components::ComputerComponent;

use super::computer_menu_view;

#[derive(Clone)]
pub enum Action {
	Boot,
}

#[derive(Draw, MonoViewHolder)]
pub struct BootView<'a> {
	#[draw]
	#[main_container]
	window: Shortcuts<Window, Action>,

	computer: &'a ComputerComponent,
}

impl<'a> BootView<'a> {
	pub fn new(computer: &'a ComputerComponent, context: &mut impl Context) -> BootView<'a> {
		let window: Window = WindowBuilder::full("SysTronic3000", context).build();

		BootView {
			window: window.into(),
			computer,
		}
	}

	pub fn boot(&mut self, context: &mut impl Context) -> Result<Option<Box<dyn Command>>, String> {
		match self.input_loop(context) {
			None => Ok(None),
			Some(command) => Ok(Some(command)),
		}
	}
}

impl<'a> MonoView for BootView<'a> {
	type Output = Box<dyn Command>;

	fn initialize(&mut self) {
		self.window.bind_shortcut(
			Key::Select,
			Shortcut::new(Action::Boot).with_label("Boot").permanent(),
		);
	}

	fn handle_action(
		&mut self,
		action: &Self::Action,
		context: &mut impl Context,
	) -> Option<Result<Self::Output, String>> {
		match action {
			Action::Boot => match computer_menu_view::menu(self.computer, context) {
				Ok(Some(command)) => Some(Ok(command)),
				Ok(None) => None,
				Err(message) => Some(Err(message)),
			},
		}
	}
}
