use nrustes::prelude::*;

use dastan::command::Command;
use dastan::ecs_components::ComputerComponent;

use super::linked_devices_view::LinkedDevicesView;

#[derive(Debug, EnumIter)]
enum MenuItem {
	LinkedDevices,
}

impl std::fmt::Display for MenuItem {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			MenuItem::LinkedDevices => write!(f, "Linked devices"),
			// other => write!(f, "{:?}", other),
		}
	}
}

pub fn menu(
	computer: &ComputerComponent,
	context: &mut impl Context,
) -> Result<Option<Box<dyn Command>>, String> {
	let mut scrollable: Scrollable<MenuItem> = WindowBuilder::full("SysTronic3000", context)
		.scrollable_builder()
		.with_items(MenuItem::iter().collect())
		.build();

	loop {
		match scrollable.handle_input_and_redraw(context).unwrap() {
			Key::Select => match scrollable.hovered() {
				Some(MenuItem::LinkedDevices) => {
					if let Some(result) =
						LinkedDevicesView::new(computer, context).linked_devices(context)
					{
						return result.map(Some);
					}
				}
				_ => continue,
			},

			Key::Back => break,
			_ => continue,
		};
	}

	Ok(None)
}
