use nrustes::prelude::*;

use dastan::{
	command::Command, ecs::prelude::EntityId, ecs_components::ComputerComponent,
	game::PlayerMapChange, Uuid,
};

#[derive(Clone, Copy)]
pub enum Action {
	Link,
	Interact,
	Unlink,
}

#[derive(Draw, DoubleViewHolder)]
pub struct LinkedDevicesView {
	#[draw]
	view: DoubleViewState<Scrollable<String>, Window, Action>,
}

impl LinkedDevicesView {
	pub fn new(computer: &ComputerComponent, context: &mut impl Context) -> Self {
		let linked_tracked_devices: Vec<String> = computer
			.linked_tracked_entities
			.iter()
			.map(|uuid| uuid.to_string())
			.collect();

		let scrollable: Scrollable<String> = WindowBuilder::left("Linked devices", context)
			.scrollable_builder()
			.with_items(linked_tracked_devices)
			.build();

		let window: Window = WindowBuilder::right("Details", context).build();

		LinkedDevicesView {
			view: DoubleViewState::new(scrollable, window),
		}
	}

	pub fn linked_devices(
		&mut self,
		context: &mut impl Context,
	) -> Option<Result<Box<dyn Command>, String>> {
		self.input_loop(context).map(Ok)
	}
}

impl DoubleView for LinkedDevicesView {
	type Output = Box<dyn Command>;

	fn initialize(&mut self) {
		self.scrollable_mut().bind_shortcut(
			Key::Char('n'),
			Shortcut::new(Action::Link).with_label("Link new device"),
		);

		if self.scrollable().container().has_items() {
			self.window_mut().bind_shortcut(
				Key::Char('e'),
				Shortcut::new(Action::Interact).with_label("Interact"),
			);

			self.window_mut().bind_shortcut(
				Key::Char('d'),
				Shortcut::new(Action::Unlink).with_label("Unlink"),
			);
		}
	}

	fn handle_action(
		&mut self,
		action: &Self::Action,
		_context: &mut impl Context,
	) -> Option<Result<Self::Output, String>> {
		match action {
			Action::Link => Some(Err("Manually enter device UUID".to_string())),
			Action::Interact => {
				let device_id =
					Uuid::parse_str(self.scrollable().container().hovered().unwrap()).unwrap();

				Some(interact_device(device_id))
			}
			Action::Unlink => Some(Err("TODO: Unlink device".to_string())),
		}
	}
}

pub fn interact_device(device_id: EntityId) -> Result<Box<dyn Command>, String> {
	// TODO Assuming device_id is a Spaceship.
	Ok(Box::new(PlayerMapChange::ControlCreature(device_id)))
}
