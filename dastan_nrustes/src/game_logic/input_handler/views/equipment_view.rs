use nrustes::prelude::*;

use dastan::command::{self, Command, ValidateCommand};
use dastan::ecs::prelude::*;
use dastan::ecs_components::body::{HumanoidBody, HumanoidSlot};
use dastan::game::Game;
use dastan::list_enum::ListEnum;

use crate::game_logic::input_handler::CommandProducer;
use crate::game_logic::map_camera_view::MapCameraView;

use super::details::PrintDetails;

#[derive(Clone, Copy)]
pub enum Action {
	Dequip(HumanoidSlot),
	Drop(HumanoidSlot),
	Scroll,
}

#[derive(Draw, DoubleViewHolder)]
pub struct EquipmentView<'a> {
	#[draw]
	view: DoubleViewState<Scrollable<String>, Scrollbar, Action>,

	slots: Vec<HumanoidSlot>,

	game: &'a Game,
	controlled_creature: &'a Creature,
	controlled_creature_body: &'a HumanoidBody,
}

impl<'a> DoubleView for EquipmentView<'a> {
	type Output = Box<dyn Command>;

	fn initialize(&mut self) {
		self.scrollable_mut()
			.bind_movement_shortcuts(Action::Scroll);

		self.update();
	}

	fn handle_action(
		&mut self,
		action: &Self::Action,
		_context: &mut impl Context,
	) -> Option<Result<Self::Output, String>> {
		match action {
			Action::Dequip(slot) => Some(
				command::Dequip {
					creature_id: *self.controlled_creature.id(),
					slot: *slot,
				}
				.validate_command(self.game),
			),
			Action::Drop(slot) => Some(
				command::DropFromEquipment {
					creature_id: *self.controlled_creature.id(),
					slot: *slot,
				}
				.validate_command(self.game),
			),
			Action::Scroll => {
				self.update();

				None
			}
		}
	}
}

impl<'a> EquipmentView<'a> {
	fn update(&mut self) {
		self.scrollbar_mut().delete_content();

		let slot = self.slots[self.scrollable().container().hovered_index().unwrap()];

		if let Some(equipment) = self.controlled_creature_body.piece(&slot) {
			self.scrollbar_mut().bind_shortcut(
				Key::Char('e'),
				Shortcut::new(Action::Dequip(slot)).with_label("Dequip"),
			);

			self.scrollbar_mut().bind_shortcut(
				Key::Char('d'),
				Shortcut::new(Action::Drop(slot)).with_label("Drop"),
			);

			equipment.print_details(self.scrollbar_mut().container_mut());
		}

		self.scrollbar_mut().schedule_draw();
	}
}

impl<'a> CommandProducer for EquipmentView<'a> {
	fn produce(
		game: &Game,
		controlled_creature: &Creature,
		_map_camera_view: &MapCameraView,
		context: &mut impl Context,
	) -> Option<Box<dyn Command>> {
		let scrollbar: Scrollbar = WindowBuilder::right("Properties", context).build();

		let controlled_creature_body = game
			.map()
			.entity_component::<HumanoidBody>(controlled_creature.id())
			.unwrap();

		let slots: Vec<HumanoidSlot> = HumanoidSlot::list()
			.into_iter()
			.filter(|s| controlled_creature_body.has_slot(s))
			.collect();

		let strings: Vec<String> = slots
			.iter()
			.map(|s| {
				if let Some(e) = controlled_creature_body.piece(s) {
					format!("{s:?}: {}", e.entity.name())
				} else {
					format!("{s:?}")
				}
			})
			.collect();

		let scrollable: Scrollable<String> = WindowBuilder::left("Equipment", context)
			.scrollable_builder()
			.with_items(strings)
			.build();

		EquipmentView {
			view: DoubleViewState::new(scrollable, scrollbar),

			slots,

			game,
			controlled_creature,
			controlled_creature_body,
		}
		.input_loop(context)
	}
}
