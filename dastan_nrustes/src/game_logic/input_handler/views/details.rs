use nrustes::colors::BLACK;
use nrustes::prelude::*;

use dastan::color::Color;
use dastan::ecs::prelude::{Entity, ItemWithComponents};
use dastan::ecs_components::Recipe;

use crate::to_rgb8::ToRGB8;

pub trait PrintDetails<C: Container> {
	fn print_details(&self, container: &mut C);
}

trait PrintKeyValue {
	fn print_key_value<D: std::fmt::Display>(&mut self, key: &str, value: D);

	fn print_key_value_color<D: std::fmt::Display>(
		&mut self,
		key: &str,
		value: D,
		value_color: Color,
	);
}

impl PrintKeyValue for Scrollbar {
	fn print_key_value<D: std::fmt::Display>(&mut self, key: &str, value: D) {
		self.print_compound_text(
			1,
			&[
				(key, self.accent_color(), BLACK),
				(&value.to_string(), self.primary_color(), BLACK),
			],
		);
	}

	fn print_key_value_color<D: std::fmt::Display>(
		&mut self,
		key: &str,
		value: D,
		value_color: Color,
	) {
		self.print_compound_text(
			1,
			&[
				(key, self.accent_color(), BLACK),
				(&value.to_string(), value_color.to_rgb8(), BLACK),
			],
		);
	}
}

impl PrintDetails<Scrollbar> for ItemWithComponents {
	fn print_details(&self, container: &mut Scrollbar) {
		container.print_key_value("Name: ", self.entity.name());
		container.print_key_value_color("Icon: ", self.entity.icon(), self.entity.color());
		container.print_key_value("Size: ", self.entity.size());
		container.print_key_value("Weight: ", self.entity.weight());

		container.add_empty_line();

		if let Some(equipment) = &self.components.humanoid_equipment {
			container.print_key_value("Vitality: ", equipment.vitality);
			container.print_key_value("Strength: ", equipment.strength);
			container.print_key_value("Defense: ", equipment.defense);

			container.add_empty_line();

			container.print_key_value("Slot: ", equipment.slot);
		}

		if let Some(food) = &self.components.food {
			container.print_key_value("Effect: ", food.effect);
		}
	}
}

impl PrintDetails<Scrollbar> for Recipe {
	fn print_details(&self, container: &mut Scrollbar) {
		container.print_key_value("Name: ", &self.name);

		container.add_empty_line();

		container.print_key_value("Input: ", format!("{:?}", self.input));
		container.print_key_value("Output: ", &self.output);
		container.print_key_value("Profession: ", self.profession);
	}
}

// pub fn terrain(win: &mut Window, terrain: &Terrain) {
// 	print_key_value(win, 3, "Name: ", terrain.name());

// 	print_key_value_color(win, 5, "Icon: ", terrain.icon(), terrain.color());
// }
