use nrustes::prelude::*;

use dastan::command::Command;
use dastan::ecs::prelude::Creature;
use dastan::game::Game;
use dastan::message_log::MessageLog;

use crate::game_logic::input_handler::CommandProducer;
use crate::game_logic::map_camera_view::MapCameraView;

#[derive(Clone, Copy)]
pub enum Action {
	Filter,
	Search,
}

#[derive(Draw, MonoViewHolder)]
pub struct MessageLogView<'a> {
	#[draw]
	#[main_container]
	scrollable: Shortcuts<Scrollable<&'a String>, Action>,

	message_log: &'a MessageLog,
}

impl<'a> MonoView for MessageLogView<'a> {
	type Output = ();

	fn initialize(&mut self) {
		self.scrollable.bind_shortcut(
			Key::Char('f'),
			Shortcut::new(Action::Filter)
				.with_label("Filter")
				.permanent(),
		);

		self.scrollable.bind_shortcut(
			Key::Char('s'),
			Shortcut::new(Action::Search)
				.with_label("Search")
				.permanent(),
		);
	}

	fn handle_action(
		&mut self,
		action: &Self::Action,
		context: &mut impl Context,
	) -> Option<Result<Self::Output, String>> {
		match action {
			Action::Filter => {
				ErrorPopup::centered_in_screen("Filter!", context).show(self, context);
			}
			Action::Search => {
				ErrorPopup::centered_in_screen("Search!", context).show(self, context);
			}
		};

		None
	}
}

impl<'a> CommandProducer for MessageLogView<'a> {
	fn produce(
		game: &Game,
		_controlled_creature: &Creature,
		_map_camera_view: &MapCameraView,
		context: &mut impl Context,
	) -> Option<Box<dyn Command>> {
		let messages: Vec<&String> = game.message_log.messages.iter().map(|m| &m.text).collect();

		let scrollable: Shortcuts<Scrollable<&String>, Action> =
			WindowBuilder::full("Log", context)
				.scrollable_builder()
				.with_no_prefix()
				.with_items(messages)
				.build();

		MessageLogView {
			scrollable,

			message_log: &game.message_log,
		}
		.input_loop(context);

		None
	}
}
