use nrustes::prelude::Context;

use dastan::command::Command;
use dastan::ecs::prelude::Creature;
use dastan::game::Game;

use crate::game_logic::map_camera_view::MapCameraView;

mod build_view;
pub use build_view::BuildView;

mod calendar_view;
pub use calendar_view::CalendarView;

mod computer_view;
use computer_view::BootView;

mod equipment_view;
pub use equipment_view::EquipmentView;

mod get_view;
pub use get_view::GetView;

mod interact_view;
pub use interact_view::interact;

mod inventory_view;
pub use inventory_view::InventoryView;

mod loot_inventory_view;
use loot_inventory_view::LootInventoryView;

mod map_view;
pub use map_view::MapView;

mod message_log_view;
pub use message_log_view::MessageLogView;

mod recipes_view;
pub use recipes_view::RecipeView;

mod target_creature_view;
pub use target_creature_view::target_creature;

mod details;

/// A trait which can be implemented by views which the player can interact
/// with to submit a `Command`.
pub trait CommandProducer {
	/// Presents the view and lets the player submit a `Command`.
	/// - `controlled_creature`: the `Creature` which might submit a `Command`. It
	///   doesn't necessarily have to be the one that represents the player.
	fn produce(
		game: &Game,
		controlled_creature: &Creature,
		map_camera_view: &MapCameraView,
		context: &mut impl Context,
	) -> Option<Box<dyn Command>>;
}
