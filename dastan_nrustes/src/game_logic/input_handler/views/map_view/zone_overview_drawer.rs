use std::cell::OnceCell;

use nrustes::prelude::{Container, Context, DrawGridCell};

use dastan::{
	map::{ZoneFeature, ZoneOverview},
	terrain::TerrainGenerator,
};

use crate::{game_logic::map_camera_view::FontSprites, to_rgb8::ToRGB8};

#[derive(Default)]
pub struct ZoneOverviewDrawer {
	/// Used to draw font sprites when not using "graphical" sprites.
	/// It's loaded on demand because it's not needed when using "graphical" sprites.
	font_sprites: OnceCell<FontSprites>,
}

impl DrawGridCell for ZoneOverviewDrawer {
	type CellItem = ZoneOverview;
	type AuxData = TerrainGenerator;

	fn draw(
		&self,
		container: &impl Container,
		y: u16,
		x: u16,
		zone_overview: &Self::CellItem,
		aux_data: &Self::AuxData,
		context: &mut impl Context,
	) {
		let terrain = aux_data.terrain(&zone_overview.terrain);

		if context.can_render_graphics() {
			context.render_graphics_sprite(terrain.name(), y, x);

			if let Some(zone_feature) = zone_overview.feature {
				let zone_feature_sprite = match zone_feature {
					ZoneFeature::Forest => "Pine".to_string(),
					ZoneFeature::River => "Troll".to_string(),
					ZoneFeature::Settlement => "Wood wall".to_string(),
				};

				context.render_graphics_sprite(&zone_feature_sprite, y, x);
			}
		} else {
			let font_sprites = self.font_sprites.get_or_init(|| FontSprites::load());

			let terrain_font_sprite = font_sprites.font_sprite(terrain.sprite());

			let font_sprite = if let Some(zone_feature) = zone_overview.feature {
				match zone_feature {
					ZoneFeature::Forest => font_sprites.font_sprite("FOREST"),
					ZoneFeature::River => font_sprites.font_sprite("RIVER"),
					ZoneFeature::Settlement => font_sprites.font_sprite("SETTLEMENT"),
				}
			} else {
				terrain_font_sprite
			};

			context.render_char_container(
				container,
				y,
				x,
				font_sprite.icon,
				font_sprite.color_fg.to_rgb8(),
				font_sprite
					.color_bg
					.unwrap_or(terrain_font_sprite.color_bg.unwrap())
					.to_rgb8(),
			);
		}
	}
}
