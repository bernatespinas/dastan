use std::cmp::min;

use direction_simple::Direction;

use nrustes::prelude::*;

use dastan::command::Command;
use dastan::ecs::prelude::Creature;
use dastan::game::{Game, PlayerMapChange};
use dastan::map::{Map, MapZonePos, Position, ZoneId, ZonePosition};
use dastan::terrain::TerrainGenerator;

use crate::game_logic::input_handler::CommandProducer;
use crate::game_logic::map_camera_view::MapCameraView;

mod map_overview_scroll_grid;
use map_overview_scroll_grid::MapOverviewScrollGrid;

mod zone_overview_drawer;
use zone_overview_drawer::ZoneOverviewDrawer;

pub struct MapView<'a> {
	pad: Pad<ZoneId>,
	details: Window,
	legend: Window,

	terrain_gen: &'a TerrainGenerator,
	map: &'a Map,

	zone_overview_drawer: ZoneOverviewDrawer,
	cursor: ZoneId,
	old_cursor: ZoneId,
}

impl<'a> Draw for MapView<'a> {
	fn draw(&mut self, context: &mut impl Context) {
		if context.needs_to_redraw_buffer() {
			context.clear();
		}

		self.draw_map_overview(context);
		self.draw_old_and_new_cursor_zones(context);

		self.details.draw(context);
		self.legend.draw(context);
	}

	fn needs_draw(&self) -> bool {
		self.pad.needs_draw() || self.details.needs_draw() || self.legend.needs_draw()
	}

	fn schedule_draw(&mut self) {
		self.pad.schedule_draw();
		self.details.schedule_draw();
		self.legend.schedule_draw();
	}
}

impl<'a> MapView<'a> {
	fn new(
		map: &'a Map,
		player_zone_pos: &'a ZonePosition,
		terrain_gen: &'a TerrainGenerator,
		context: &impl Context,
	) -> MapView<'a> {
		let pad = create_map_view_pad(map, player_zone_pos, context);

		let details: Window = WindowBuilder::new(
			0,
			context.width() - context.width() / 4,
			context.height() / 3,
			context.width() / 4,
			context,
		)
		.with_centered_title("Details")
		.build();

		let legend = WindowBuilder::new(
			details.height(),
			details.x(),
			context.height() - details.height(),
			details.width(),
			context,
		)
		.with_centered_title("Legend")
		.build();

		MapView {
			pad,
			details,
			legend,

			terrain_gen,
			map,

			zone_overview_drawer: ZoneOverviewDrawer::default(),
			cursor: player_zone_pos.zone_id,
			old_cursor: player_zone_pos.zone_id,
		}
	}

	fn update_cursor(&mut self, cursor_movement: Direction) {
		let new_cursor = MapOverviewScrollGrid(self.map.overview())
			.apply_direction(&self.cursor, cursor_movement)
			.unwrap();

		self.old_cursor = self.cursor;
		self.cursor = new_cursor;
	}

	fn draw_old_and_new_cursor_zones(&mut self, context: &mut impl Context) {
		let (y, x) = MapOverviewScrollGrid(self.map.overview())
			.position_to_screen_coordinates(&self.old_cursor, self.pad.top_left())
			.unwrap();

		self.pad.draw_cell_item(
			&MapOverviewScrollGrid(self.map.overview()),
			&self.old_cursor,
			&self.zone_overview_drawer,
			y,
			x,
			self.terrain_gen,
			context,
		);

		let (y, x) = MapOverviewScrollGrid(self.map.overview())
			.position_to_screen_coordinates(&self.cursor, self.pad.top_left())
			.unwrap();

		if context.can_render_graphics() {
			context.render_graphics_sprite(
				// TODO Placeholder sprite.
				&"Player".to_string(),
				self.pad.y() + y,
				self.pad.x() + x,
			);
		} else {
			context.render_char_container(
				self.pad.window(),
				y,
				x,
				'x',
				RGB8::new(255, 255, 255),
				RGB8::new(0, 0, 0),
			);
		}
	}

	fn update_auxiliary_windows(&mut self) {
		let map_overview_sg = MapOverviewScrollGrid(self.map.overview());
		let hovered_zone_overview = map_overview_sg.cell_item(&self.cursor);

		self.details
			.print_accent((2, 1), hovered_zone_overview.terrain);

		self.details
			.print_accent((4, 1), format!("{:?}", hovered_zone_overview.feature));
	}

	fn draw_map_overview(&self, context: &mut impl Context) {
		self.pad.draw_decorations(context);

		if self
			.pad
			.scroll_grid_is_smaller_than_view(&MapOverviewScrollGrid(self.map.overview()))
		{
			self.pad.erase_content(context);
		}

		self.pad.draw(
			&MapOverviewScrollGrid(self.map.overview()),
			&self.zone_overview_drawer,
			self.terrain_gen,
			context,
		);
	}
}

impl<'a> CommandProducer for MapView<'a> {
	fn produce(
		game: &Game,
		_controlled_creature: &Creature,
		_map_camera_view: &MapCameraView,
		context: &mut impl Context,
	) -> Option<Box<dyn Command>> {
		let mut map_view = MapView::new(
			game.map(),
			game.player_zone_pos(),
			&game.gen_bundle.terrain_gen,
			context,
		);

		map_view.update_auxiliary_windows();
		map_view.draw(context);

		loop {
			let key = context.input_key().gamify();

			match key {
				Key::Back => break,
				Key::Movement(direction) => {
					map_view.update_cursor(direction);
					map_view.update_auxiliary_windows();

					let old_top_left = *map_view.pad.top_left();

					let new_top_left = update_top_left(&map_view, direction, context);

					if old_top_left != new_top_left {
						map_view.pad.set_top_left(
							new_top_left,
							&MapOverviewScrollGrid(map_view.map.overview()),
						);

						map_view.draw(context);
					} else {
						map_view.draw_old_and_new_cursor_zones(context);
						map_view.details.draw(context);
						map_view.legend.draw(context);
					}
				}
				Key::Char('t') => {
					return Some(Box::new(PlayerMapChange::TeleportToPosition(MapZonePos {
						map_id: *map_view.map.id(),
						zone_pos: ZonePosition::new(map_view.cursor, Position::new(0, 0)),
					})));
				}
				_ => {
					if context.needs_to_redraw_buffer() {
						map_view.draw(context);
					}
				}
			}
		}

		None
	}
}

fn update_top_left(map_view: &MapView, direction: Direction, context: &impl Context) -> ZoneId {
	if context.needs_to_redraw_buffer() {
		let current_top_left = *map_view.pad.top_left();

		let new_top_left = MapOverviewScrollGrid(map_view.map.overview())
			.apply_direction(&current_top_left, direction)
			.unwrap();

		return new_top_left;
	}

	if map_view.map.overview().height() <= map_view.pad.height()
		&& map_view.map.overview().width() <= map_view.pad.width()
	{
		return *map_view.pad.top_left();
	}

	let top_left = *map_view.pad.top_left();
	let bottom_right = *map_view.pad.bottom_right();

	let new_cursor = MapOverviewScrollGrid(map_view.map.overview())
		.apply_direction(&map_view.cursor, direction)
		.unwrap();

	let mut new_top_left = *map_view.pad.top_left();

	if direction.is_up() {
		let top_left_up = MapOverviewScrollGrid(map_view.map.overview())
			.apply_direction(&top_left, Direction::Up)
			.unwrap();

		if top_left_up.0 == new_cursor.0 {
			log::info!("panning view up");

			for _ in 0..min(map_view.map.overview().height(), map_view.pad.height()) {
				new_top_left = MapOverviewScrollGrid(map_view.map.overview())
					.apply_direction(&new_top_left, Direction::Up)
					.unwrap();
			}
		}
	} else if direction.is_down() {
		let bot_right_down = MapOverviewScrollGrid(map_view.map.overview())
			.apply_direction(&bottom_right, Direction::Down)
			.unwrap();

		if bot_right_down.0 == new_cursor.0 {
			log::info!("panning view down");

			for _ in 0..min(map_view.map.overview().height(), map_view.pad.height()) {
				new_top_left = MapOverviewScrollGrid(map_view.map.overview())
					.apply_direction(&new_top_left, Direction::Down)
					.unwrap();
			}
		}
	}

	if direction.is_left() {
		let top_left_left = MapOverviewScrollGrid(map_view.map.overview())
			.apply_direction(&top_left, Direction::Left)
			.unwrap();

		if top_left_left.1 == new_cursor.1 {
			log::info!("panning view left");

			for _ in 0..min(map_view.map.overview().width(), map_view.pad.width()) {
				new_top_left = MapOverviewScrollGrid(map_view.map.overview())
					.apply_direction(&new_top_left, Direction::Left)
					.unwrap();
			}
		}
	} else if direction.is_right() {
		let bot_right_right = MapOverviewScrollGrid(map_view.map.overview())
			.apply_direction(&top_left, Direction::Right)
			.unwrap();

		if bot_right_right.1 == new_cursor.1 {
			log::info!("panning view right");

			for _ in 0..min(map_view.map.overview().width(), map_view.pad.width()) {
				new_top_left = MapOverviewScrollGrid(map_view.map.overview())
					.apply_direction(&new_top_left, Direction::Right)
					.unwrap();
			}
		}
	}

	new_top_left
}

fn create_map_view_pad(
	map: &Map,
	player_zone_pos: &ZonePosition,
	context: &impl Context,
) -> Pad<ZoneId> {
	let side_windows_width = context.width() / 4;

	let (height, width) = map_view_window_height_width(side_windows_width, context);

	let pad_window: Window = WindowBuilder::new(0, 0, height, width, context)
		.with_centered_title("Map")
		.build();

	let top_left = get_top_left_centered_at_the_player_zone_id(&pad_window, map, player_zone_pos);

	Pad::new(pad_window, top_left, &MapOverviewScrollGrid(map.overview()))
}

/// Returns the height and width the `Window` in which the `MapOverview` is drawn should have. The appropriate unit for the provided `Context` is used (pixels or terminal cells).
fn map_view_window_height_width(side_windows_width: u16, context: &impl Context) -> (u16, u16) {
	let (pixel_height, pixel_width) = context.size_in_smallest_units();
	let (_font_height, font_width) = context.font_sprite_size();
	let (graphics_height, graphics_width) = context.graphics_sprite_size();

	let height = pixel_height / graphics_height;
	let width = (pixel_width - side_windows_width * font_width) / graphics_width;

	(height, width)
}

fn get_top_left_centered_at_the_player_zone_id(
	map_view_window: &Window,
	map: &Map,
	player_zone_pos: &ZonePosition,
) -> ZoneId {
	let to_the_left_amount = min(map_view_window.width(), map.overview().width()) / 2;

	let up_amount = min(map_view_window.height(), map.overview().height()) / 2;

	let mut top_left = player_zone_pos.zone_id;

	for _ in 0..to_the_left_amount {
		top_left = MapOverviewScrollGrid(map.overview())
			.apply_direction(&top_left, Direction::Left)
			.unwrap();
	}

	for _ in 0..up_amount {
		top_left = MapOverviewScrollGrid(map.overview())
			.apply_direction(&top_left, Direction::Up)
			.unwrap();
	}

	top_left
}
