use direction_simple::Direction;

use nrustes::prelude::ScrollGrid;

use dastan::map::{MapOverview, ZoneId, ZoneOverview};

/// I need to create a new type since `MapOverview` and `ScrollGrid` come from
/// different crates.
pub struct MapOverviewScrollGrid<'a>(pub &'a MapOverview);

impl<'a> ScrollGrid for MapOverviewScrollGrid<'a> {
	type Position = ZoneId;
	type CellItem = ZoneOverview;

	fn rows(&self) -> u16 {
		self.0.height()
	}

	fn columns(&self) -> u16 {
		self.0.width()
	}

	fn apply_direction(
		&self,
		position: &Self::Position,
		direction: direction_simple::Direction,
	) -> Option<Self::Position> {
		let y = {
			if direction.is_up() {
				if position.0 > 0 {
					position.0 - 1
				} else {
					self.rows() - 1
				}
			} else if direction.is_down() {
				if position.0 + 1 >= self.rows() {
					0
				} else {
					position.0 + 1
				}
			} else {
				position.0
			}
		};

		let x = {
			if direction.is_left() {
				if position.1 > 0 {
					position.1 - 1
				} else {
					self.columns() - 1
				}
			} else if direction.is_right() {
				if position.1 + 1 >= self.columns() {
					0
				} else {
					position.1 + 1
				}
			} else {
				position.1
			}
		};

		Some((y, x))
	}

	fn position_to_screen_coordinates(
		&self,
		zone_id: &Self::Position,
		top_left: &Self::Position,
	) -> Option<(u16, u16)> {
		let mut y = 0;

		let mut cursor = *top_left;

		while cursor.0 != zone_id.0 {
			cursor = self.apply_direction(&cursor, Direction::Down).unwrap();

			y += 1;
		}

		let mut x = 0;

		while cursor.1 != zone_id.1 {
			cursor = self.apply_direction(&cursor, Direction::Right).unwrap();

			x += 1
		}

		Some((y, x))
	}

	fn cell_item(&self, zone_id: &Self::Position) -> &Self::CellItem {
		self.0.get(zone_id)
	}
}
