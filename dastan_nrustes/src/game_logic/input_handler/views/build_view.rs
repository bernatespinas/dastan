use nrustes::prelude::*;

use dastan::command::{self, Command, ValidateCommand};
use dastan::ecs::prelude::*;
use dastan::ecs_components::{Profession, RecipeBook};
use dastan::game::Game;
use dastan::map::{Map, Tile};

use crate::game_logic::input_handler::target::TargetCommand;
use crate::game_logic::input_handler::CommandProducer;
use crate::game_logic::map_camera_view::MapCameraView;

#[derive(Clone, Copy)]
pub enum Action {
	BuildBlueprint,
	ChangeProfession,
	Scroll,
}

#[derive(Draw, DoubleViewHolder)]
pub struct BuildView<'a> {
	#[draw]
	view: DoubleViewState<Scrollable<String>, Window, Action>,

	profession: Profession,
	professions: Vec<Profession>,
	recipe_book: &'a RecipeBook,

	game: &'a Game,
	map_camera_view: &'a MapCameraView,
	creature: &'a Creature,
}

impl<'a> DoubleView for BuildView<'a> {
	type Output = Box<dyn Command>;

	fn initialize(&mut self) {
		self.scrollable_mut()
			.bind_movement_shortcuts(Action::Scroll);

		self.scrollable_mut().bind_shortcut(
			Key::Char('p'),
			Shortcut::new(Action::ChangeProfession)
				.with_label("Profession")
				.permanent(),
		);

		self.update();
	}

	fn handle_action(
		&mut self,
		action: &Self::Action,
		context: &mut impl Context,
	) -> Option<Result<Self::Output, String>> {
		match action {
			Action::BuildBlueprint => {
				// todo!()
				let mut command = command::BuildBlueprint {
					creature_id: *self.creature.id(),
					profession: self.profession,
					index: self.scrollable().container().hovered_index().unwrap(),
					// ingredients: ingredients.clone(),
					zone_pos: None,
				};

				match command.validate_ingredients(self.game.map()) {
					Ok(()) => {}
					Err(message) => return Some(Err(message)),
				};

				let mut target_command: TargetCommand<()> = TargetCommand::new(
					self.game,
					self.map_camera_view,
					*self.game.map().entity_zone_pos(self.creature.id()).unwrap(),
					info_tile_build,
					context,
				)
				.adjacent();

				if let Some((_, zone_pos)) =
					target_command.input(self.game, self.game.map(), self.creature, context)
				{
					command.zone_pos = Some(zone_pos);

					Some(command.validate_command(self.game))
				} else {
					None
				}
			}
			Action::ChangeProfession => {
				let mut context_menu = ContextMenu::new(
					"Professions",
					self.professions.iter().map(|p| p.to_string()).collect(),
					self.scrollable().window(),
					context,
				);

				match context_menu.show(self, context) {
					None => {}
					Some(pos) => {
						self.profession = self.professions[pos];

						let names: Vec<String> = self
							.recipe_book
							.blueprints_by_profession(self.profession)
							.iter()
							.map(|b| b.output.to_string())
							.collect();

						self.scrollable_mut().container_mut().change_items(names);

						self.view
							.scrollable_mut()
							.container_mut()
							.change_title(format!("Recipes - {}", self.profession));
					}
				}

				None
			}
			Action::Scroll => {
				self.update();

				None
			}
		}
	}
}

impl<'a> BuildView<'a> {
	fn update(&mut self) {
		self.window_mut().clear_shortcuts();
		self.window_mut().delete_content();

		if self.scrollable().container().hovered().is_some() {
			self.window_mut().print_accent((3, 1), "TODO");

			self.window_mut().bind_shortcut(
				Key::Char('e'),
				Shortcut::new(Action::BuildBlueprint).with_label("Build"), // .permanent(),
			);
		}

		self.window_mut().schedule_draw();
	}
}

impl<'a> CommandProducer for BuildView<'a> {
	fn produce(
		game: &Game,
		creature: &Creature,
		map_camera_view: &MapCameraView,
		context: &mut impl Context,
	) -> Option<Box<dyn Command>> {
		let recipe_book = game.map().entity_component::<RecipeBook>(creature.id())?;

		let window: Window = WindowBuilder::right("Properties", context).build();

		let professions: Vec<Profession> = Profession::list_construction();
		let profession = Profession::Carpentry;
		// let mut mode = Mode::BuildFurniture;

		let names: Vec<String> = recipe_book
			.blueprints_by_profession(profession)
			.iter()
			.map(|r| r.output.to_string())
			.collect();

		let scrollable: Scrollable<String> = WindowBuilder::left("Build", context)
			.scrollable_builder()
			.with_items(names)
			.build();

		BuildView {
			view: DoubleViewState::new(scrollable, window),

			profession,
			professions,
			recipe_book,

			game,
			map_camera_view,
			creature,
		}
		.input_loop(context)
	}
}

fn info_tile_build(
	_map: &Map,
	tile: &Tile,
	_controlled_creature: &Creature,
	details: &mut Shortcuts<Window, ()>,
) {
	let is_empty_floor = !tile.has_creature()
		&& !tile.has_prop()
		&& !tile.has_item()
		&& !tile.terrain_type.blocks_passage();

	details.window_mut().print_accent((2, 1), "info_tile_build");

	if is_empty_floor {
		details.bind_shortcut(Key::Select, Shortcut::new(()).with_label("Build"));
	}
}
