use std::collections::HashMap;

use dastan::event::Event;
use nrustes::prelude::*;

use dastan::command::Command;
use dastan::ecs::prelude::Creature;
use dastan::game::Game;

use crate::game_logic::flags::Flags;
use crate::game_logic::map_camera_view::MapCameraView;

use super::super::TurnAction;
use super::move_or_attack::move_or_attack;

// TODO Can I use `impl Context` instead of `CTX`?
pub struct PlayerInputHandler<CTX: Context> {
	views: HashMap<Key, fn(&Game, &Creature, &MapCameraView, &mut CTX) -> Option<Box<dyn Command>>>,
	menus: HashMap<Key, fn(&Game, &mut Flags, &mut CTX) -> Option<Box<dyn Command>>>,
}

impl<CTX: Context> Default for PlayerInputHandler<CTX> {
	fn default() -> Self {
		PlayerInputHandler {
			views: HashMap::with_capacity(10),
			menus: HashMap::with_capacity(1),
		}
	}
}

impl<CTX: Context> PlayerInputHandler<CTX> {
	pub fn with_view(
		mut self,
		key: Key,
		view: fn(&Game, &Creature, &MapCameraView, &mut CTX) -> Option<Box<dyn Command>>,
	) -> Self {
		self.views.insert(key, view);

		self
	}

	pub fn with_menu(
		mut self,
		key: Key,
		menu: fn(&Game, &mut Flags, &mut CTX) -> Option<Box<dyn Command>>,
	) -> Self {
		self.menus.insert(key, menu);

		self
	}

	pub fn handle_input(
		&self,
		key: Key,
		game: &Game,
		player: &Creature,
		map_camera_view: &MapCameraView,
		flags: &mut Flags,
		context: &mut CTX,
	) -> (Option<Box<dyn Command>>, TurnAction) {
		if let Key::Movement(direction) = key {
			match move_or_attack(game, direction) {
				Some(command) => {
					return (Some(command), TurnAction::TakeTurn);
				}
				None => return (None, TurnAction::Nothing),
			}
		}

		if let Some(menu) = self.menus.get(&key) {
			let command = menu(game, flags, context);

			return (command, TurnAction::Redraw);
		}

		if let Some(view) = self.views.get(&key) {
			match view(game, player, map_camera_view, context) {
				Some(command) => {
					return (Some(command), TurnAction::TakeTurnAndRedraw);
				}
				None => return (None, TurnAction::Redraw),
			};
		}

		(None, TurnAction::Nothing)
	}

	pub fn handle_command(&self, command: Box<dyn Command>, game: &mut Game) -> Event {
		match command.player_validate(game) {
			Ok(()) => command.execute(game),
			Err(message) => {
				todo!("Create `ValidatedCommand` or `UnvalidatedCommand` or whatever to differentiate `Command`s that have been validated from those that have not. Anyways, the error message is: {message}.")
			}
		}
	}
}
