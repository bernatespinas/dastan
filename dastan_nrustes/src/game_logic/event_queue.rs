use dastan::event::Event;

/// Stores the `Event`s which have taken place in a turn.
#[derive(Default)]
pub struct EventQueue {
	events: Vec<Event>,
}

impl EventQueue {
	pub fn add(&mut self, event: Event) {
		self.events.push(event);
	}

	/// Returns a consuming iterator with all the stored `Event`s.
	pub fn iter(&mut self) -> impl Iterator<Item = Event> {
		std::mem::take(&mut self.events).into_iter()
	}
}
