use nrustes::prelude::*;

use dastan::{
	ecs::prelude::{Creature, EntityId, Identify},
	game::{Game, PlayerMapChange},
	map::{Map, MapZonePos, Tile},
};

use crate::game_logic::map_camera_view::MapCameraView;
use crate::game_logic::panels::PanelManager;
use crate::game_logic::{flags::Flags, input_handler::TargetCommand};

use super::{change_map, load_map};

pub fn handle_player_movement_if_necessary(
	game: &mut Game,
	map_camera_view: &mut MapCameraView,
	panel_manager: &mut PanelManager<Window>,
	flags: &mut Flags,
	context: &mut impl Context,
) -> Result<(), String> {
	prepare_map_change_if_necessary(game, map_camera_view, panel_manager, flags, context)?;

	if flags.player_zone_pos_has_changed {
		if context.can_render_graphics() {
			map_camera_view.try_to_center_position(game.player_zone_pos(), game.map());

			flags.redraw = true;
		} else if map_camera_view
			.pan_camera_to_keep_position_visible(game.player_zone_pos(), game.map())
		{
			flags.redraw = true;
		}

		if flags.player_zone_id_has_changed {
			let player_zone_id = game.player_zone_pos().zone_id;

			game.map_mut().update_loaded_zones(&player_zone_id)?;

			flags.update_zone_feature(&game);
			flags.ambiance_might_need_to_be_updated = true;

			flags.player_zone_id_has_changed = false;

			if flags.player_map_has_changed {
				// Right now, I don't have any logic which has to run here. I'll leave this here in case I have any in the future.
				flags.player_map_has_changed = false;
			}
		}

		flags.player_zone_pos_has_changed = false;
	}

	Ok(())
}

/// Tries to obtain and return the `MapZonePos` which describes where is the `Entity` with the given ID.
fn get_entity_map_zone_pos_from_its_map(
	game: &mut Game,
	entity_id: &EntityId,
) -> Result<MapZonePos, String> {
	let map_id = *game.get_entity_map(entity_id).unwrap();
	let game_name = game.name().to_string();

	// I need to load the `Map` the `Entity` is in to be able to obtain its `ZonePosition`.
	if !game.map_manager.map_is_loaded(&map_id) {
		game.map_manager.load_map(&game_name, &map_id)?;
	}

	let zone_pos = *game
		.map_manager
		.map(&map_id)
		.entity_zone_pos(entity_id)
		.unwrap();

	Ok(MapZonePos { map_id, zone_pos })
}

fn prepare_map_change_if_necessary(
	game: &mut Game,
	map_camera_view: &mut MapCameraView,
	panel_manager: &mut PanelManager<Window>,
	flags: &mut Flags,
	context: &mut impl Context,
) -> Result<(), String> {
	let Some(player_map_change) = game.player_map_change.take() else {
		return Ok(());
	};

	log::debug!("Moving the player to another Map: {player_map_change:?}.");

	flags.player_map_has_changed = true;
	flags.player_zone_pos_has_changed = true;
	flags.player_zone_id_has_changed = true;

	match player_map_change {
		PlayerMapChange::TeleportToPosition(map_zone_pos) => {
			log::debug!("Trying to get player from {:?}", game.player_zone_pos());
			let mut player = game.player_take();
			player.components.target = None;

			game.set_entity_map(player.id(), map_zone_pos.map_id);

			let player_zone_pos = map_zone_pos.zone_pos;

			change_map(
				game,
				&map_zone_pos,
				map_camera_view,
				&mut panel_manager.custom_panel,
				context,
			)?;

			game.map_mut().put(&player_zone_pos, player);
		}
		PlayerMapChange::TeleportToEntity(entity_id) => {
			let mut player = game.player_take();
			player.components.target = None;

			let map_zone_pos = get_entity_map_zone_pos_from_its_map(game, &entity_id)?;

			game.set_entity_map(player.id(), map_zone_pos.map_id);

			let player_zone_pos = map_zone_pos.zone_pos;

			change_map(
				game,
				&map_zone_pos,
				map_camera_view,
				&mut panel_manager.custom_panel,
				context,
			)?;

			game.map_mut().put(&player_zone_pos, player);
		}
		PlayerMapChange::ControlCreature(entity_id) => {
			let map_zone_pos = get_entity_map_zone_pos_from_its_map(game, &entity_id)?;

			game.previous_player_ids.push_front(game.player_id);

			change_map(
				game,
				&map_zone_pos,
				map_camera_view,
				&mut panel_manager.custom_panel,
				context,
			)?;

			game.player_id = entity_id;
		}
		PlayerMapChange::StopControllingEntity => {
			let last_player_id = game.previous_player_ids.pop_front().unwrap();

			let last_map_id = game.get_entity_map(&last_player_id).copied().unwrap();

			let last_zone_pos = game
				.map_manager
				.map(&last_map_id)
				.entity_zone_pos(&last_player_id)
				.copied()
				.unwrap();

			change_map(
				game,
				&MapZonePos {
					map_id: last_map_id,
					zone_pos: last_zone_pos,
				},
				map_camera_view,
				&mut panel_manager.custom_panel,
				context,
			)?;

			game.player_id = last_player_id;
		}
		PlayerMapChange::PickAdjacentPosition(entity_id) => {
			let map_zone_pos = get_entity_map_zone_pos_from_its_map(game, &entity_id)?;

			let mut target_command: TargetCommand<()> = TargetCommand::new(
				game,
				map_camera_view,
				map_zone_pos.zone_pos,
				teleport_tile_info_fn,
				context,
			)
			.adjacent();

			load_map(game, &map_zone_pos, target_command.custom_panel(), context)?;

			if let Some((_, teleport_zone_pos)) = target_command.input(
				game,
				game.map_manager.map(&map_zone_pos.map_id),
				game.player(),
				context,
			) {
				let mut player = game.player_take();
				player.components.target = None;

				game.set_entity_map(player.id(), map_zone_pos.map_id);

				change_map(
					game,
					&MapZonePos {
						zone_pos: teleport_zone_pos,
						..map_zone_pos
					},
					map_camera_view,
					&mut panel_manager.custom_panel,
					context,
				)?;

				game.map_mut().put(&teleport_zone_pos, player);
			}
		}
	};

	Ok(())
}

fn teleport_tile_info_fn(
	_map: &Map,
	tile: &Tile,
	_controlled_creature: &Creature,
	details: &mut Shortcuts<Window, ()>,
) {
	details
		.window_mut()
		.print_accent((2, 1), tile.terrain_type.to_string());

	if !tile.blocks_passage() {
		details.bind_shortcut(Key::Select, Shortcut::new(()).with_label("Teleport"));
	}
}
