use nrustes::prelude::*;

use dastan::game::Game;
use dastan::generator::GeneratorBundle;
use dastan::map::MapZonePos;

use crate::{audio::AudioSystem, dastan_config::DastanConfig};

mod map_camera_view;
use map_camera_view::MapCameraView;

mod panels;
use panels::PanelManager;

mod input_handler;
use input_handler::{InputHandler, TurnAction};

mod flags;
use flags::Flags;

mod draw;
use draw::{draw_map_and_panels, update_color_palette_and_graphics_if_necessary};

mod event_queue;
use event_queue::EventQueue;

mod process_events;
use process_events::process_events;

mod handle_player_input;
use handle_player_input::handle_player_input;

mod move_player;
use move_player::handle_player_movement_if_necessary;

mod player_target;
use player_target::invalidate_player_target_if_necessary;

mod save;
use save::save_if_necessary;

mod sound;
use sound::update_ambiance_sound_if_necessary;

fn handle_first_time_ever_loaded_game(game: &mut Game) {
	if game.new_game {
		game.new_game = false;
	}
}

pub fn main_loop(mut game: Game, context: &mut impl Context) -> Result<(), String> {
	let dastan_config = DastanConfig::load("dastan_config.ron")?;

	game.gen_bundle = GeneratorBundle::load();

	let mut map_camera_view =
		MapCameraView::centered_at(game.player_zone_pos(), game.map(), context);

	handle_first_time_ever_loaded_game(&mut game);

	update_color_palette_and_graphics_if_necessary(&game, &mut map_camera_view, context);

	let mut panel_manager = PanelManager::new(context);

	let mut audio_system = get_audio_system(&dastan_config);

	// let pathfinding = Pathfinding::new(game.map());

	let mut flags = Flags::for_game_that_has_just_been_loaded(&game);

	let input_handler = InputHandler::default();

	let mut event_queue = EventQueue::default();

	loop {
		invalidate_player_target_if_necessary(
			&mut game,
			&mut map_camera_view,
			&mut panel_manager,
			context,
		);

		draw_map_and_panels(
			&game,
			&mut panel_manager,
			&mut map_camera_view,
			&mut flags,
			context,
		);

		handle_player_input(
			&mut game,
			&input_handler,
			&map_camera_view,
			&mut event_queue,
			&mut flags,
			context,
		);

		if flags.take_turn {
			tick_minute(&mut game, &mut map_camera_view, &mut flags, context);
		}

		process_events(
			&mut game,
			&mut event_queue,
			&mut map_camera_view,
			&mut flags,
		);

		handle_player_movement_if_necessary(
			&mut game,
			&mut map_camera_view,
			&mut panel_manager,
			&mut flags,
			context,
		)?;

		update_ambiance_sound_if_necessary(&mut flags, &mut audio_system);

		save_if_necessary(
			&mut game,
			&mut panel_manager,
			&mut map_camera_view,
			&mut flags,
			context,
		)?;

		if flags.exit {
			audio_system.stop()?;

			return Ok(());
		}
	}
}

fn tick_minute(
	game: &mut Game,
	map_camera_view: &mut MapCameraView,
	flags: &mut Flags,
	context: &mut impl Context,
) {
	if game.map_mut().time.tick_minute() {
		let previous_color_palette = map_camera_view.color_palette();

		update_color_palette_and_graphics_if_necessary(game, map_camera_view, context);

		let current_color_palette = map_camera_view.color_palette();

		if previous_color_palette != current_color_palette {
			flags.redraw = true;
		}
	}
}

fn change_map(
	game: &mut Game,
	map_zone_pos: &MapZonePos,
	map_camera_view: &mut MapCameraView,
	popup_container: &mut impl Container,
	context: &mut impl Context,
) -> Result<(), String> {
	let changing_map = || -> Result<(), String> {
		game.change_map(map_zone_pos)?;

		map_camera_view.try_to_center_position(&map_zone_pos.zone_pos, game.map());

		Ok(())
	};

	ProgressPopup::centered_in_container("Loading map", popup_container, context).process(
		changing_map,
		popup_container,
		context,
	)
}

fn load_map<C: Container>(
	game: &mut Game,
	map_zone_pos: &MapZonePos,
	container: &mut C,
	context: &mut impl Context,
) -> Result<(), String> {
	let loading_map = || -> Result<(), String> {
		let game_name = game.name().to_string();

		game.map_manager.load_map_and_required_zones(
			&game_name,
			&map_zone_pos.map_id,
			&map_zone_pos.zone_pos.zone_id,
		)
	};

	ProgressPopup::centered_in_container("Loading map", container, context).process(
		loading_map,
		container,
		context,
	)
}

fn get_audio_system(dastan_config: &DastanConfig) -> AudioSystem {
	if dastan_config.enable_sound {
		AudioSystem::rodio(dastan_config.resources_dir_path.join("sounds"))
	} else {
		AudioSystem::no_audio()
	}
}
