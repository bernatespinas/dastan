use nrustes::prelude::*;

use dastan::{ecs::prelude::Identify, ecs_components::TargetComponent, game::Game};

use super::{map_camera_view::MapCameraView, panels::PanelManager};

pub fn invalidate_player_target_if_necessary(
	game: &mut Game,
	map_camera_view: &mut MapCameraView,
	panel_manager: &mut PanelManager<Window>,
	context: &mut impl Context,
) {
	if let Some(target_id) = &game.player_target_id() {
		log::debug!("target_id exists: {target_id}");

		// Check if the player's target exists. If it doesn't, clear it.
		if let Some(target_zone_pos) = game.map().entity_zone_pos(target_id) {
			log::debug!("Targeted entity is being tracked.");

			// If the player's target exists, clear it if it's no longer visible.
			if !map_camera_view.is_visible(target_zone_pos, game.map()) {
				log::debug!("Targeted entity is not visible - removing target_id.");

				clear_player_target(game);
				panel_manager.clear_target(context);
			}
		} else {
			log::debug!("Targeted entity is not being tracked - removing target_id.");

			clear_player_target(game);
			panel_manager.clear_target(context);
		}
	}
}

fn clear_player_target(game: &mut Game) {
	let player_id = *game.player().id();

	game.map_mut()
		.entity_component_remove::<TargetComponent>(&player_id);
}
