use nrustes::prelude::*;

use dastan::game::Game;

use crate::game_logic::flags::Flags;
use crate::game_logic::map_camera_view::MapCameraView;
use crate::game_logic::panels::PanelManager;

use super::draw_map_and_panels;

pub fn save<C: Container>(
	game: &Game,
	popup_container: &mut C,
	context: &mut impl Context,
) -> Result<(), String> {
	let saving_game = || -> Result<(), String> { game.save_universe() };

	ProgressPopup::centered_in_container("Saving game", popup_container, context).process(
		saving_game,
		popup_container,
		context,
	)
}

pub fn save_if_necessary(
	game: &mut Game,
	panel_manager: &mut PanelManager<Window>,
	map_camera_view: &mut MapCameraView,
	flags: &mut Flags,
	context: &mut impl Context,
) -> Result<(), String> {
	if flags.save {
		flags.redraw = true;

		draw_map_and_panels(game, panel_manager, map_camera_view, flags, context);

		save(game, &mut panel_manager.custom_panel, context)?;

		flags.save = false;
	}

	Ok(())
}
