use nrustes::prelude::*;

mod health_panel;
pub use health_panel::HealthPanel;

mod game_info_panel;
pub use game_info_panel::GameInfoPanel;

mod panel_manager;
pub use panel_manager::PanelManager;

const SMALL_PANEL_HEIGHT: u16 = 5;
const BIG_PANEL_HEIGHT: u16 = 10;

fn create_panel(
	y: u16,
	x: u16,
	width: u16,
	title: &str,
	primary_color: RGB8,
	accent_color: RGB8,
	context: &impl Context,
) -> Window {
	WindowBuilder::new(y, x, SMALL_PANEL_HEIGHT, width, context)
		.with_centered_title(title)
		.with_colors(primary_color, accent_color)
		.build()
}
