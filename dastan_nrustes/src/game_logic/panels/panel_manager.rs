use nrustes::{
	colors::{DARK_GREEN, LIGHT_GREEN},
	prelude::*,
};

use dastan::{color::Color, ecs::prelude::Creature, game::Game};

use crate::game_logic::map_camera_view::MapCameraView;
use crate::to_rgb8::ToRGB8;

use super::{create_panel, GameInfoPanel, HealthPanel, BIG_PANEL_HEIGHT};

pub struct PanelManager<C: Container> {
	player_health: HealthPanel,
	game_info: GameInfoPanel,

	pub target_health: HealthPanel,
	empty_panel: Window,

	pub custom_panel: C,
}

impl<C> PanelManager<C>
where
	C: Container,
	WindowBuilder: BuildContainer<C>,
{
	pub fn new(context: &impl Context) -> Self {
		// let panel_width = context_width/4 - 2 + 1;
		let panel_width = context.width() / 4;

		PanelManager {
			player_health: HealthPanel::new(
				context.height() - 10,
				0,
				panel_width,
				"Health",
				context,
			),

			game_info: GameInfoPanel::new(context.height() - 5, 0, panel_width, context),

			target_health: HealthPanel::new(
				context.height() - 10,
				context.width() - panel_width,
				panel_width,
				"Target health",
				context,
			),

			empty_panel: create_panel(
				context.height() - 5,
				context.width() - panel_width,
				panel_width,
				"",
				Color::DBlue.to_rgb8(),
				Color::LBlue.to_rgb8(),
				context,
			),

			custom_panel: WindowBuilder::new(
				context.height() - BIG_PANEL_HEIGHT,
				panel_width,
				BIG_PANEL_HEIGHT,
				panel_width * 2,
				context,
			)
			.with_colors(DARK_GREEN, LIGHT_GREEN)
			.build(),
		}
	}

	pub fn redraw_small_panels(
		&mut self,
		game: &Game,
		creature: &Creature,
		map_camera_view: &MapCameraView,
		context: &mut impl Context,
	) {
		self.player_health.draw_decorations(context);
		self.game_info.draw_decorations(context);

		self.target_health.draw_decorations(context);
		self.empty_panel.draw_decorations(context);

		self.redraw_small_panels_content(game, creature, map_camera_view, context);
	}

	fn redraw_small_panels_content(
		&self,
		game: &Game,
		creature: &Creature,
		map_camera_view: &MapCameraView,
		context: &mut impl Context,
	) {
		self.player_health.erase_content(context);

		self.player_health.draw_health(creature, context);

		self.game_info.erase_content(context);
		self.game_info
			.draw_game_info(game, map_camera_view, context);

		self.target_health.erase_content(context);
		if let Some(target) = game.player_target() {
			self.target_health.draw_health(target, context);
		}
	}

	pub fn redraw_full(
		&mut self,
		game: &Game,
		creature: &Creature,
		map_camera_view: &MapCameraView,
		context: &mut impl Context,
	) {
		self.redraw_small_panels(game, creature, map_camera_view, context);

		self.custom_panel.draw(context);
	}

	pub fn redraw_content(
		&mut self,
		game: &Game,
		creature: &Creature,
		map_camera_view: &MapCameraView,
		context: &mut impl Context,
	) {
		self.redraw_small_panels_content(game, creature, map_camera_view, context);

		self.custom_panel.draw_content(context);
	}

	pub fn clear_target(&self, context: &mut impl Context) {
		self.target_health.erase_content(context);
	}
}
