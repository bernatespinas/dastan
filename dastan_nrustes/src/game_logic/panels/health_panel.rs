use nrustes::colors::{BLACK, DARK_RED, LIGHT_RED};
use nrustes::prelude::*;

use dastan::ecs::prelude::Creature;

use super::create_panel;

#[derive(Draw)]
pub struct HealthPanel {
	#[draw]
	window: Window,
}

impl HealthPanel {
	pub fn new(y: u16, x: u16, width: u16, title: &str, context: &impl Context) -> HealthPanel {
		HealthPanel {
			window: create_panel(y, x, width, title, DARK_RED, LIGHT_RED, context),
		}
	}

	pub fn draw_health(&self, creature: &Creature, context: &mut impl Context) {
		let health = format!(
			"{}/{}",
			creature.current_vitality(),
			creature.max_vitality()
		);

		context.render_text_container(&self.window, 1, 1, &health, LIGHT_RED, BLACK);
	}
}

impl Container for HealthPanel {
	fn window(&self) -> &Window {
		&self.window
	}

	fn window_mut(&mut self) -> &mut Window {
		&mut self.window
	}

	fn update(&mut self) {}
}
