use nrustes::colors::BLACK;
use nrustes::prelude::*;

use dastan::color::Color;
use dastan::game::Game;

use crate::game_logic::map_camera_view::MapCameraView;
use crate::to_rgb8::ToRGB8;

use super::create_panel;

#[derive(Draw)]
pub struct GameInfoPanel {
	#[draw]
	window: Window,
}

impl GameInfoPanel {
	pub fn new(y: u16, x: u16, width: u16, context: &impl Context) -> GameInfoPanel {
		GameInfoPanel {
			window: create_panel(
				y,
				x,
				width,
				"Game info",
				Color::DBlue.to_rgb8(),
				Color::LBlue.to_rgb8(),
				context,
			),
		}
	}

	pub fn draw_game_info(
		&self,
		game: &Game,
		map_camera_view: &MapCameraView,
		context: &mut impl Context,
	) {
		context.render_text_container(
			&self.window,
			1,
			1,
			&game.map().time.hour.to_string(),
			Color::LBlue.to_rgb8(),
			BLACK,
		);

		context.render_text_container(
			&self.window,
			2,
			1,
			&format!(
				"{:?}, {}",
				game.player_zone_pos().zone_id,
				game.player_zone_pos().pos
			),
			Color::LBlue.to_rgb8(),
			BLACK,
		);

		context.render_text_container(
			&self.window,
			3,
			1,
			&format!("{}", game.map().zone_pos_to_pos(map_camera_view.top_left())),
			Color::LBlue.to_rgb8(),
			BLACK,
		);
	}
}

impl Container for GameInfoPanel {
	fn window(&self) -> &Window {
		&self.window
	}

	fn window_mut(&mut self) -> &mut Window {
		&mut self.window
	}

	fn update(&mut self) {}
}
