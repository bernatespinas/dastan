use dastan::game::Game;

use super::{event_queue::EventQueue, flags::Flags, map_camera_view::MapCameraView};

pub fn process_events(
	game: &mut Game,
	event_queue: &mut EventQueue,
	map_camera_view: &mut MapCameraView,
	flags: &mut Flags,
) {
	for event in event_queue.iter() {
		if let Some(message) = event.message {
			game.message_log.add(message);

			flags.has_new_messages_to_display = true;
		}

		map_camera_view.add_tiles_pending_redraw(event.updated_tiles);
	}
}
