use nrustes::prelude::*;

use dastan::color::Color;
use dastan::ecs::prelude::{EntityId, Identify};
use dastan::event::{Message, Severity};
use dastan::game::Game;

use crate::game_logic::map_camera_view::{color_palette, MapCameraView};
use crate::game_logic::panels::PanelManager;
use crate::to_rgb8::ToRGB8;

use super::flags::Flags;

pub fn draw_map_and_panels(
	game: &Game,
	panel_manager: &mut PanelManager<Window>,
	map_camera_view: &mut MapCameraView,
	flags: &mut Flags,
	context: &mut impl Context,
) {
	if flags.has_new_messages_to_display {
		draw_messages(&mut panel_manager.custom_panel, game);

		flags.has_new_messages_to_display = false;
	}

	if flags.redraw || context.needs_to_redraw_buffer() {
		fully_draw_map_and_panels(game, panel_manager, map_camera_view, context);

		flags.redraw = false;
	} else {
		draw_modified_tiles_and_panels_content(game, panel_manager, map_camera_view, context);
	}
}

fn fully_draw_map_and_panels(
	game: &Game,
	panel_manager: &mut PanelManager<Window>,
	map_camera_view: &mut MapCameraView,
	context: &mut impl Context,
) {
	if context.needs_to_redraw_buffer() {
		context.clear();
	}

	map_camera_view.draw_map(game.map(), &game.gen_bundle.terrain_gen, context);

	// Fully draw all panels (decorations + clear content + content).
	panel_manager.redraw_full(game, game.player(), map_camera_view, context);

	// Flush all the input events that accumulated while the map was being drawn. TODO This doesn't work with termion at least.
	context.flush_events();
}

fn draw_modified_tiles_and_panels_content(
	game: &Game,
	panel_manager: &mut PanelManager<Window>,
	map_camera_view: &mut MapCameraView,
	context: &mut impl Context,
) {
	let map = game.map();

	for zone_pos in map_camera_view.tiles_pending_redraw() {
		if map_camera_view.is_visible(&zone_pos, map) {
			map_camera_view.draw_tile(map, &zone_pos, &game.gen_bundle.terrain_gen, context);
		}
	}

	panel_manager.redraw_content(game, game.player(), map_camera_view, context);
}

fn draw_messages(window: &mut Window, game: &Game) {
	let messages_to_show = (window.height() as usize - 2) / 2;

	let player_id = game.player().id();

	for (i, message) in game
		.message_log
		.messages
		.iter()
		.rev()
		.take(messages_to_show)
		.rev()
		.enumerate()
	{
		//               y index
		// |-               0
		// |                1
		// | Message _0_    2 = 2 * _0_ + 2
		// |                3
		// | Message _1_    4 = 2 * _1_ + 2
		// |                5
		// | Message _2_    6 = 2 * _2_ + 2
		// |-               7
		let y: u16 = 2 * i as u16 + 2;

		let color = message_color(message, player_id);

		window.print_fg((y, 1), &message.text, color.to_rgb8());
	}
}

/// Returns the `Color` in which a `Message` should be printed.
fn message_color(message: &Message, player_id: &EntityId) -> Color {
	match message.severity_for_receiver {
		Severity::Good => {
			// TODO Use `contains` (or its equivalent) once it's available.
			// if message.receiver.is_none() || message.receiver.contains(player_id) {
			// 	Color::LGreen
			// } else {
			// 	Color::White
			// }
			match &message.receiver {
				None => Color::LGreen,
				Some(entity_id) => {
					if entity_id == player_id {
						Color::LGreen
					} else {
						Color::White
					}
				}
			}
		}
		Severity::Neutral => Color::White,
		Severity::Bad => {
			// TODO Use `contains` (or its equivalent) once it's available.
			// if message.receiver.is_none() || message.receiver.contains(player_id) {
			// 	Color::Red
			// } else {
			// 	Color::White
			// }
			match &message.receiver {
				None => Color::Red,
				Some(entity_id) => {
					if entity_id == player_id {
						Color::Red
					} else {
						Color::White
					}
				}
			}
		}
	}
}

pub fn update_color_palette_and_graphics_if_necessary(
	game: &Game,
	map_camera_view: &mut MapCameraView,
	context: &mut impl Context,
) {
	let color_palette = color_palette::choose_palette(&game.map().time);

	map_camera_view.set_color_palette(color_palette);

	if context.can_render_graphics() {
		let part_of_day = game.map().time.part_of_day();
		let texture_name = format!("{part_of_day:?}").to_lowercase();
		log::debug!("Setting graphics texture to {texture_name}.");

		context.set_current_graphics_texture(texture_name);
	}
}
