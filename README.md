# Welcome!

This is Dastan, a work-in-progress "roguelike" or world simulator ___.

**Current status:**

- Generated worlds and gameplay in general are not entertaining at all.
- Almost every feature is not finished.

Most notable features at the moment:

- You can save and load games!
- Looping maps divided into zones (chunks) that load and unload as the player moves across/through them.
- A crude version of space, spaceships and planets.

Thanks for having a look! :D

...

# Supported platforms

I've only tested the game in/with Fedora, so I expect any Linux distribution to be okay.

He probado el juego sólo en Fedora, así que espero que cualquier otra distribución Linux pueda ejecutarlo. Algunas no tendrán disponibles los paquetes necesarios o sus versiones, pero seguro que las más grandes sí. 

No he hecho nada especial para asegurarme de que se ejecute en Windows, pero tampoco lo he probado, así que...

# Requirements

## Dependencies

If you want to compile the game yourself, you'll need to install the Rust ¿toolchain?. TODO rustup

If you want to use the SDL2 backend, install SDL2!

## Alacritty

gnome-terminal, konsole and the like are too slow and/or don't support monospace fonts that well.

I recommend using [Kitty]() or [Alacritty]() instead. Although a bit harder to configure, the game looks and runs way better ¿in? them.

TODO Share basic config?

## Elieroz3 font

If you want to play the game using ```termion``` or ```crossterm```, you'll want to use the font I created ¿e?specifically for it: [Elieroz3.ttf](about:blank).

Configure your terminal emulator program to use it!

If you want to use ```SDL2``` instead you don't need it, because then the game will use a bitmap image.

